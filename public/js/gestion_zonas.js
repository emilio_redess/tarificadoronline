    $(document).ready(function(){

        $( "#selectZonas" ).change(function() {

            var id = $('#selectZonas').val();
            var nombreZona = $("#selectZonas option:selected").text();


            if (id == "")
                $('#nombreZona').val("");
            else
                $('#nombreZona').val(nombreZona);
            $.ajax({
                type:'POST',
                data:{id: id},
                url: JS_BASE_URL + "/admin/zonas/ajax_datosZona/",
                success: function(result){
                   var json_obj = $.parseJSON(result);//parse JSON

                    for (var i in json_obj) 
                    {
                        if (json_obj[i].incluida == 1)
                        {                            
                            $('#provincia_'+json_obj[i].id).prop('checked', true);
                        }
                        else{
                            $('#provincia_'+json_obj[i].id).prop('checked', false);
                        }
                    }
                }
            });
        });
    });