    $(document).ready(function(){

        $( ".ajax_select" ).change(function() {

            var idCia = $('#selectCias').val();
            var codigoRamo = $('#selectRamo').val();
            var soloVacios = $('#soloVacios').val();
            // Vaciamos el select de productos para llenarlo con los productos que corresponda
            $( ".selectProducto" ).each(function( index ) {
                //$('option', '.selectProducto').not(':eq(0)').remove();
                $('option', this).not(':eq(0)').remove();
            });

            if (idCia == ""){
                $( ".selectProducto" ).prop( "disabled", true );  
            }
            else{   
                $( ".selectProducto" ).prop( "disabled", false );    // Activamos el select del producto que estaba previamente en estado disabled  
                $.ajax({
                    type:'POST',
                    data:{idCia: idCia,
                          codigoRamo: codigoRamo},
                    url: JS_BASE_URL + "/admin/productos_general/ajax_getProducto/" + soloVacios,
                    success: function(result){
                       var json_obj = $.parseJSON(result);//parse JSON

                        for (var i in json_obj) 
                        {                                                  
                            $('.selectProducto').append($('<option>', {
                                value: json_obj[i].id,
                                text: json_obj[i].nombre_producto
                            }));

                        }
                    }
                });

            }

        });
    });