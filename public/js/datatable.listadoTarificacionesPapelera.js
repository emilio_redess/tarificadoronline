$(function() {
	//wait till the page is fully loaded before loading table
	//dataTableSearch() is optional.  It is a jQuery plugin that looks for input fields in the thead to bind to the table searching
	$("#tarificaciones").dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
    },
        
		processing: true,
        serverSide: true,
        "order": [[ 1, "desc" ]],
        ajax: {
            "url": JS_BASE_URL + "/Tarificador/dataTable/" + ramo,
            "type": "POST"
        },
        columns: [
            { data: "t.codigo_tarificacion" },
            
            { data: "t.fecha_borrado" },
        	{ data: "$.deletedByNameFull" },
        	{ data: "r.name" },
        	{ data: "$.contactNameFull" },
        	{ data: "c.nombre" },
            { data: "t.numMensajes"}  // esta ultima columna se le pone cualquier valor, pues despues se le va a cambiar el valor y se pondran los botones de acciones.

        ],

        "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var cadena = '<div class="btn-group">';
                    var tipoBotonMensajes = "btn-success";

                    if (row.t.numMensajes != "0")
                        tipoBotonMensajes = "btn-warning";

                    cadena = cadena + '<a href="' + JS_BASE_URL + '/tarificador/ver_detalle_tarificacion/' + row.t.id + '" target="_blank" class="btn btn-primary" data-balloon="Ver Detalles" data-balloon-pos="left" data-container="body"><span class="glyphicon glyphicon-file"></span></a>';
                    cadena = cadena + '<button type="button" class="btn-restore btn btn-success" data-toggle="modal" data-target="#ModalRestore" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-balloon="Restaurar" data-balloon-pos="up" data-container="body"><span class="glyphicon glyphicon-share-alt"></span></button>';                
                    cadena = cadena + '<button type="button" class="btn-delete btn btn-danger" data-toggle="modal" data-target="#ModalDelete" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-balloon="Eliminar" data-balloon-pos="right" data-container="body"><span class="glyphicon glyphicon-trash"></span></button>';                
                    cadena = cadena + '</div>';
                // var cadena = row.t.codigo_tarificacion;
                    return cadena;
                },
                "targets": 6
            },
            { "visible": true,  "targets": [ 0 ] }
        ]

	}).dataTableSearch(500);

});


///////////////////////////////////////////////////////////////   VACIAR PAPELERA   ////////////////////////////////////////////////////////
$(document).on("click", ".btn-delete-all", function () {

    var textoModal = "¿Deseas eliminar estos elementos de forma permanente?";

    $( "div.modal-delete-body" ).html(textoModal);

});

$(document).on("click", ".btn-delete-all-accept", function () {

    $('#ModalDeleteAll').modal('hide');
    var url = JS_BASE_URL + "/Tarificaciones/vaciarPapelera/";
     window.location = url;
    });
///////////////////////////////////////////////////////////////   FIN VACIAR PAPELERA   ////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////   BORRAR 1 TARIFICACION    ////////////////////////////////////////////////////////
$(document).on("click", ".btn-delete", function () {
    var tarificacionId = $(this).data('id');
    $("#tarificacionId").val( tarificacionId );

    var tarificacionRef = $(this).data('ref');
    var textoModal = "La tarificación con la referencia " + tarificacionRef + " sera borrada permanentemente.";

    $( "div.modal-delete-body" ).html(textoModal);

});

        $(document).on("click", ".btn-delete-accept", function () {

            $('#ModalDelete').modal('hide');
            var tarificacionId = $('#tarificacionId').val();
            var url = JS_BASE_URL + "/Tarificaciones/borrarTarificacion/" + tarificacionId;
            window.location = url;
            

        });
///////////////////////////////////////////////////////////   FIN BORRAR 1 TARIFICACION   ///////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////   RESTAURAR TARIFICACION    ////////////////////////////////////////////////////////
        $(document).on("click", ".btn-restore", function () {
            var tarificacionId = $(this).data('id');
            $("#tarificacionId").val( tarificacionId );

            var tarificacionRef = $(this).data('ref');
            var textoModal = "Se va a restaurar la tarificación con la referencia " + tarificacionRef;

            $( "div.modal-restore-body" ).html(textoModal);

        });

        $(document).on("click", ".btn-restore-accept", function () {

            $('#ModalRestore').modal('hide');
            var tarificacionId = $('#tarificacionId').val();
            var url = JS_BASE_URL + "/Tarificaciones/restaurarTarificacion/" + tarificacionId;
            window.location = url;
            

        });
///////////////////////////////////////////////////////////   FIN RESTAURAR TARIFICACION   ///////////////////////////////////////////////////////