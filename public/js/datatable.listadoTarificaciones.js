$(function() {
	//wait till the page is fully loaded before loading table
	//dataTableSearch() is optional.  It is a jQuery plugin that looks for input fields in the thead to bind to the table searching

    var cadenaSearch = search_tarificacion;

	var listado = $("#tarificaciones").dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
    },
         "search": {
            "search": cadenaSearch
          },

		processing: true,
        serverSide: true,
        "order": [[ 2, "desc" ]],
        ajax: {
            "url": JS_BASE_URL + "/Tarificador/dataTable/" + ramo,
            "type": "POST"
        },
        columns: [
        	{ data: "t.codigo_tarificacion" },
        	{ data: "r.name" },
        	{ data: "t.fecha_creacion" },
        	{ data: "c.nombre" },
        	{ data: "$.contactNameTruncated" },
        	{ data: "t.telefono" },
            { data: "$.contactEmailTruncated" },        	
            { data: "t.numMensajes"},  // esta ultima columna se le pone cualquier valor, pues despues se le va a cambiar el valor y se pondran los botones de acciones.
            { data: "t.documentacionEnviada" },
            { data: "t.contratada" }

        ],

        "columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    var cadena = '<div class="btn-group">';
                    var tipoBotonMensajes = "btn-success";
                    var tipoBotonDocu = "btn-warning";
                    var tipoBotonContratada = "btn-warning";
                    var glyphDocu = "glyphicon-remove-circle";
                    var glyphContratada = "glyphicon-thumbs-down";

                    if (row.t.numMensajes != "0")
                        tipoBotonMensajes = "btn-warning";

                    if (row.t.documentacionEnviada == "1")
                    {
                        tipoBotonDocu = "btn-success";
                        glyphDocu = "glyphicon-ok-circle";
                    }

                    if (row.t.contratada == "1")
                    {
                        tipoBotonContratada = "btn-success";
                        glyphContratada = "glyphicon-thumbs-up";
                    }                    

                    cadena = cadena + '<a href="' + JS_BASE_URL + '/tarificador/ver_detalle_tarificacion/' + row.t.id + '" target="_blank" class="btn btn-primary" data-balloon="Ver Detalles" data-balloon-pos="left" data-container="body"><span class="glyphicon glyphicon-file"></span></a>';
                    cadena = cadena + '<button type="button" class="btn-delete btn btn-danger" data-toggle="modal" data-target="#ModalDelete" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-balloon="Enviar a la papelera" data-balloon-pos="up" data-container="body"><span class="glyphicon glyphicon-trash"></span></button>';                    
                    cadena = cadena + '<button type="button" id="commentarios' + row.t.id + '" class="btn-commentaries btn '+ tipoBotonMensajes +'" data-toggle="modal" data-target="#ModalCommentaries" data-nmen="' + row.t.numMensajes + '" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-balloon="Ver anotaciones" data-balloon-pos="up" data-container="body"><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;<span id="commentaries-badge' + row.t.id + '" class="badge">' + row.t.numMensajes + '</span></button>';
                    cadena = cadena + '<button type="button" id="docu' + row.t.id + '" class="btn-docu btn '+ tipoBotonDocu +'" data-toggle="modal" data-target="#ModalDocu" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-estado="' + row.t.documentacionEnviada + '" data-balloon="Documentación enviada" data-balloon-pos="up" data-container="body"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;<span id="glyphdocu' + row.t.id + '" class="glyphicon ' + glyphDocu + '"></span></button>';
                    cadena = cadena + '<button type="button" id="contratada' + row.t.id + '" class="btn-contratada btn '+ tipoBotonContratada +'" data-toggle="modal" data-target="#ModalContratada" data-id="' + row.t.id + '" data-ref="' + row.t.codigo_tarificacion + '" data-estado="' + row.t.contratada + '" data-balloon="Estado contratación" data-balloon-pos="right" data-container="body"><span id="glyphcontratada' + row.t.id + '" class="glyphicon ' + glyphContratada + '"></span></button>';
                    cadena = cadena + '</div>';

                    return cadena;
                },
                "targets": 7
            },
           // {"asSorting": [ "desc"], "aTargets": [ 3 ] },
            { "visible": true,  "targets": [ 0 ] },
            { "visible": false,  "targets": [ 8 ] },
            { "visible": false,  "targets": [ 9 ] },
            {
                targets : 2,
                    render : function(this_date){
                        //Here you should call the date format function:
                        return datejs_format_function(this_date);
                }
            }
        ]

	}).dataTableSearch(500);

});

///////////////////////////////////////////////////////////////   CONTRATACION    ////////////////////////////////////////////////////////
$(document).on("click", ".btn-contratada", function () {
    var tarificacionId = $(this).data('id');
    $("#contratadaTarificacionId").val( tarificacionId );
    $("#contratadaEstado").val( $(this).data('estado') );

    var tarificacionRef = $(this).data('ref');
    if ($(this).data('estado') == "0")
        var textoModal = "Se va a marcar la tarificación con la referencia " + tarificacionRef + " como contratada.";
    else
        var textoModal = "Se va a desmarcar la tarificación con la referencia " + tarificacionRef + " como contratada";


    $( "div.modal-contratada-body" ).html(textoModal);

});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////// CONTRATACION Ajax Action //////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-contratada-accept", function () {

            var idTarif = $('#contratadaTarificacionId').val();
            var contratadaEstado = $('#contratadaEstado').val();

       
            $(".modal-add-contratada-wait-icon").html('<span style="color:red;">Enviando datos. Un momento por favor...</span>'); 

                $.ajax({
                    url     : JS_BASE_URL + "/Tarificador/ajax_ContratarTarificacion",
                    type: 'post',
                    data: {
                        idTarif: idTarif,
                        contratadaEstado: contratadaEstado
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);//parse JSON
                        //Ajax request success
                        if (data.status == 'ok') {
                            $('#ModalContratada').modal('hide'); 

                            $(".modal-add-contratada-wait-icon").html('');                        

                            if (contratadaEstado == 1){    // se ha puesto el flag de la sql contratada a 0
                                $('#contratada'+idTarif).data('estado','0');
                                $('#contratada' + idTarif).removeClass("btn-success");
                                $('#contratada' + idTarif).addClass("btn-warning");
                                $('#glyphcontratada' + idTarif).removeClass("glyphicon-thumbs-up");
                                $('#glyphcontratada' + idTarif).addClass("glyphicon-thumbs-down");                                   
                            }else{ // se ha puesto el flag de la sql documentacionEnviada a 1
                                $('#contratada'+idTarif).data('estado','1');
                                $('#contratada' + idTarif).removeClass("btn-warning");
                                $('#contratada' + idTarif).addClass("btn-success"); 
                                $('#glyphcontratada' + idTarif).removeClass("glyphicon-thumbs-down");
                                $('#glyphcontratada' + idTarif).addClass("glyphicon-thumbs-up");                                                               
                            }

                            // status ok actions
                        } else {
                            // status error actions
                            $('#ModalContratada').modal('hide');
                            $('#ModalError').modal('show');
                        }
                    },
                    error: function (xhr) {
                        //Ajax request error
                    }
                })
            
        });



///////////////////////////////////////////////////////////////   ENVIO DE DOCUMENTACION    ////////////////////////////////////////////////////////
$(document).on("click", ".btn-docu", function () {
    var tarificacionId = $(this).data('id');
    $("#docuTarificacionId").val( tarificacionId );
    $("#docuEstado").val( $(this).data('estado') );

    var tarificacionRef = $(this).data('ref');
    if ($(this).data('estado') == "0")
        var textoModal = "Se va a marcar la tarificación con la referencia " + tarificacionRef + " como que se ha enviado la documentación al usuario.";
    else
        var textoModal = "Se va a desmarcar la tarificación con la referencia " + tarificacionRef + " como que se ha enviado la documentación al usuario.";


    $( "div.modal-docu-body" ).html(textoModal);

});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////// ENVIO DE DOCUMENTACION Ajax Action //////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-docu-accept", function () {

            var idTarif = $('#docuTarificacionId').val();
            var docuEstado = $('#docuEstado').val();

       
            $(".modal-add-docu-wait-icon").html('<span style="color:red;">Enviando datos. Un momento por favor...</span>'); 

                $.ajax({
                    url     : JS_BASE_URL + "/Tarificador/ajax_EnviarDocumentacion",
                    type: 'post',
                    data: {
                        idTarif: idTarif,
                        docuEstado: docuEstado
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);//parse JSON
                        //Ajax request success
                        if (data.status == 'ok') {
                            $('#ModalDocu').modal('hide'); 

                            $(".modal-add-docu-wait-icon").html('');                        

                            if (docuEstado == 1){    // se ha puesto el flag de la sql documentacionEnviada a 0
                                $('#docu'+idTarif).data('estado','0');
                                $('#docu' + idTarif).removeClass("btn-success");
                                $('#docu' + idTarif).addClass("btn-warning");
                                $('#glyphdocu' + idTarif).removeClass("glyphicon-ok-circle");
                                $('#glyphdocu' + idTarif).addClass("glyphicon-remove-circle");                                   
                            }else{ // se ha puesto el flag de la sql documentacionEnviada a 1
                                $('#docu'+idTarif).data('estado','1');
                                $('#docu' + idTarif).removeClass("btn-warning");
                                $('#docu' + idTarif).addClass("btn-success"); 
                                $('#glyphdocu' + idTarif).removeClass("glyphicon-remove-circle");
                                $('#glyphdocu' + idTarif).addClass("glyphicon-ok-circle");                                                               
                            }

                            // status ok actions
                        } else {
                            // status error actions
                            $('#ModalDocu').modal('hide');
                            $('#ModalError').modal('show');
                        }
                    },
                    error: function (xhr) {
                        //Ajax request error
                    }
                })
            
        });

///////////////////////////////////////////////////////////////   ENVIAR A PAPELERA 1 TARIFICACION    ////////////////////////////////////////////////////////
$(document).on("click", ".btn-delete", function () {
    var tarificacionId = $(this).data('id');
    $("#tarificacionId").val( tarificacionId );

    var tarificacionRef = $(this).data('ref');
    var textoModal = "Se va a enviar la tarificación con la referencia " + tarificacionRef + " a la papelera de reciclaje.";

    $( "div.modal-delete-body" ).html(textoModal);

});

$(document).on("click", ".btn-delete-accept", function () {

    $('#ModalDelete').modal('hide');
    var tarificacionId = $('#tarificacionId').val();
    var ramo = $('#ramo').val();
    var url = JS_BASE_URL + "/Tarificaciones/enviarTarificacionPapelera/" + ramo + "/" + tarificacionId;
    window.location = url;
            

});
///////////////////////////////////////////////////////////   FIN DESACTIVAR 1 TARIFICACION   ///////////////////////////////////////////////////////

        //////////////////////// jquery UI Datepicker   ////////////////////////////////////////////////////////////////
        $(function() {

            $( "#reminderDate" ).datepicker();
            $( "#reminderDate" ).datepicker( "option", "showAnim", "slideDown" );
            $( "#reminderDate" ).datepicker( "option", "minDate", "0");
            $( "#reminderDate" ).datepicker( "option", "maxDate", "+30");
            $("#reminderDate").datepicker( "option", $.datepicker.regional[ "es" ] );

            $( "#edit-reminderDate" ).datepicker();
            $( "#edit-reminderDate" ).datepicker( "option", "showAnim", "slideDown" );
            $( "#edit-reminderDate" ).datepicker( "option", "minDate", "0");
            $( "#edit-reminderDate" ).datepicker( "option", "maxDate", "+30");
            $("#edit-reminderDate").datepicker( "option", $.datepicker.regional[ "es" ] );
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////// Commentaries Listing   ///////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-commentaries", function () {
            var idTarif = $(this).data('id');
            var pricingRef = $(this).data('ref');
            var num_mensajes = $(this).data('nmen');
    
            var btnExport = '';
            if (num_mensajes > 0)
                btnExport = '<a href= "' + JS_BASE_URL + '/ws-api/tarificaciones/listadoAnotaciones/tid/' + idTarif + '/format/csv"><button type="button" class="btn btn-export-commentary btn-success btn-sm commentaries-btn" data-toggle="modal" data-tid=""' + idTarif + '"><span class="glyphicon glyphicon-export"></span></button></a>';
            var modalCommentariesTitle = "Anotaciones de la tarificación " + pricingRef + " " + btnExport;
            $("#modalCommentariesTitle").html(modalCommentariesTitle);
            $("#commentaries-table-tbody").html('<tr><td colspan="4">CARGANDO DATOS...</td></tr>');

            $.ajax({             
                url     : JS_BASE_URL + "/Tarificador/ajax_ListarComentarios",
                type    : 'post',
                data   : {idTarif: idTarif},
                success : function(result) {
                    //Ajax request success
                    var data = $.parseJSON(result);//parse JSON
                    if(data.status=='ok'){
                        var html ="";
                       
                        $("#commentaries-table-tbody").empty();

                        if(data.items.length > 0){

                            $.each(data.items, function(key,val){

                                var color_texto = 'style="color:black;"';
                                if (data.items[key]['fecha_limite'] == 1)
                                    color_texto = 'style="color:red"';

                                var btnEdit = '';
                                var btnDel = '';
                                
                                if (data.items[key]['isOwner'] == true) {
                                    btnEdit = '<button type="button" class="btn btn-edit-commentary btn-info btn-sm commentaries-btn" data-toggle="modal" data-target="#ModalEditCommentary"' + 'data-id="' + data.items[key]['id'] + '" data-reminderDate="' + data.items[key]['reminderDateFormatted'] + '" data-text="' + data.items[key]['text'] + '" id="edit-commentaries-btn-' + data.items[key]['id'] + '"><span class="glyphicon glyphicon-edit"></span></button>';
                                    btnDel = '<button type="button" class="btn btn-delete-commentary btn-danger btn-sm commentaries-btn" data-toggle="modal" data-target="#ModalDeleteCommentary"' + 'data-id="' + data.items[key]['id'] + '" id="del-commentaries-btn-' + data.items[key]['id'] + '"><span class="glyphicon glyphicon-trash"></span></button>';
                                    //btnExport = '<button type="button" class="btn btn-export-commentary btn-success btn-sm commentaries-btn" data-toggle="modal" data-tid=""' + idTarif + '"><span class="glyphicon glyphicon-export"></span></button>';
                                    
                                    
                                }

                                html = html +
                                '<tr id="rowCommentary' + data.items[key]['id'] + '" ' + color_texto + '>' +
                                '<td>' + data.items[key]['createdAtFormatted'] + '</td>' +
                                '<td>' + data.items[key]['reminderDateFormatted'] + '</td>' +
                                '<td>' + data.items[key]['username'] + '</td>' +
                                '<td>' + data.items[key]['text'] + '</td>' +
                                '<td>' +
                                btnEdit +
                                btnDel +                               
                                '</td>' +
                                '</tr>';
                            });

                        }else{
                            html = '<tr id="commentaries-table-noresults"><td colspan="5" class="text-center"><h4>' +
                            '<span class="label label-info">No existen anotaciones para esta tarificación</span></h4></td></tr>';
                        }
                        $('#commentaries-table-tbody').empty();
                        $('#commentaries-table-tbody').append(html);



                        // Se pegan los botones de "cancel" y "add" con la id de la tarificacion en data-id

                        var htmlButtons = '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>' +
                                '<button type="button" id="btn-add-commentary" data-toggle="modal" data-target="#ModalAddCommentary" class="btn btn-add-commentary btn-primary" data-id="' + idTarif + '">Añadir</button>';

                        $('.commentaries-footer').empty();
                        $('.commentaries-footer').append(htmlButtons);

                        // status ok actions
                    }else{
                        // status error actions
                        alert(data.msg)
                    }
                },
                error   : function(xhr){
                    //Ajax request error
                }
            })
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////      



//////////////// Vaciar el modal de add comentarios al abrirlo  /////////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-add-commentary", function () {
            $(".modal-add-commentary-wait-icon").html('');
            var idTarif = $(this).data('id');
            $("#addCommentaryPricingId").val( idTarif );
            $('#reminderDate').val('');
            $('#commentaryText').val('');
            if ($( ".form-group-commentary-text" ).hasClass( "has-error" ))
                $( ".form-group-commentary-text" ).removeClass( "has-error" );
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

////////// Añadir comentario Ajax Action //////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-add-commentary-accept", function () {

            var commentaryDate = $('#reminderDate').val();
            var commentaryText = $('#commentaryText').val();
            var idTarif = $('#addCommentaryPricingId').val();

            if (commentaryText == ""){
                $( ".form-group-commentary-text" ).addClass( "has-error" );
            }else{
               $(".modal-add-commentary-wait-icon").html('<span style="color:red;">Enviando datos. Un momento por favor...</span>'); 

                $.ajax({
                    url     : JS_BASE_URL + "/Tarificador/ajax_AddComentario",
                    type: 'post',
                    data: {
                        idTarif: idTarif,
                        commentaryDate: commentaryDate,
                        commentaryText: commentaryText
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);//parse JSON
                        //Ajax request success
                        if (data.status == 'ok') {
                            $('#ModalAddCommentary').modal('hide');
                            $('#ModalCommentaries').modal('hide');

                            var cant = $('#commentaries-badge'+idTarif).html();
                            cant = parseInt(cant) + 1;
                            $('#commentaries-badge' + idTarif).html(cant);

                            if (cant == 1){
                                $('#commentarios' + idTarif).removeClass("btn-success");
                                $('#commentarios' + idTarif).addClass("btn-warning");
                            }

                            // status ok actions
                        } else {
                            // status error actions
                            $('#ModalAddCommentary').modal('hide');
                            $('#ModalCommentaries').modal('hide');
                            $('#ModalError').modal('show');
                        }
                    },
                    error: function (xhr) {
                        //Ajax request error
                    }
                })
            }
        });
       
         ///////////////// Delete comentario   //////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-delete-commentary", function () {
            var commentaryId = $(this).data('id');
            $("#commentaryId").val( commentaryId );
            $( "div.modal-deleteCommentary-body" ).html("Se va a eliminar la anotación correspondiente");
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////// Delete comentario Ajax Action ///////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-deleteCommentary-accept", function () {
            $('#ModalDeleteCommentary').modal('hide');
            var commentaryId = $('#commentaryId').val();
            $.ajax({
                url : JS_BASE_URL + "/Tarificador/ajax_deleteComentario",
                type: 'post',
                data: {commentaryId: commentaryId},
                success: function (result) {
                    var data = $.parseJSON(result);//parse JSON
                    if (data.status == 'ok') {

                        $('#rowCommentary' + commentaryId)
                                .children('td')
                                .animate({padding: 0})
                                .wrapInner('<div />')
                                .children()
                                .slideUp(function () {
                                    $('#rowCommentary' + commentaryId).remove();
                                });

                        var cant = $('#commentaries-badge'+data.idTarif).html();
                        cant = parseInt(cant) - 1;
                        $('#commentaries-badge'+data.idTarif).html(cant);
                        if (cant == 0){
                            $('#commentarios' + data.idTarif).removeClass("btn-warning");
                            $('#commentarios' + data.idTarif).addClass("btn-success");

                            var  html = '<tr id="commentaries-table-noresults"><td colspan="4" class="text-center"><h4>' +
                                    '<span class="label label-info">No existen anotaciones para esta tarificación</span></h4></td></tr>';
                            $('#commentaries-table-tbody').empty();
                            $('#commentaries-table-tbody').append(html);
                        }
                    } else {
                        $('#ModalAddCommentary').modal('hide');
                        $('#ModalCommentaries').modal('hide');
                        $('#ModalError').modal('show');
                    }
                },
                error: function (xhr) {
                    //Ajax request error
                }
            })
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ///////////////// Edit Commentary   ////////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-edit-commentary", function () {
            $(".modal-edit-commentary-wait-icon").html('');
            var commentaryId = $(this).data('id');
            var reminderDate = $(this).data('reminderdate');
            var text = $(this).data('text');
            $("#editCommentaryId").val( commentaryId );
            $('#edit-reminderDate').val(reminderDate);
            $('#edit-commentaryText').val(text);
            if ($( ".form-group-commentary-text" ).hasClass( "has-error" ))
                $( ".form-group-commentary-text" ).removeClass( "has-error" );

        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////// Edit commentary Ajax Action //////////////////////////////////////////////////////////////////////////
        $(document).on("click", ".btn-edit-commentary-accept", function () {

            var commentaryDate = $('#edit-reminderDate').val();
            var commentaryText = $('#edit-commentaryText').val();
            var commentaryId = $('#editCommentaryId').val();

            if (commentaryText == ""){
                $( ".form-group-commentary-text" ).addClass( "has-error" );
            }else {
                $(".modal-edit-commentary-wait-icon").html('<span style="color:red;">Enviando datos. Un momento por favor...</span>');

                $.ajax({
                    url: JS_BASE_URL + "/Tarificador/ajax_editComentario",
                    type: 'post',
                    data: {
                        commentaryId: commentaryId,
                        commentaryDate: commentaryDate,
                        commentaryText: commentaryText
                    },
                    success: function (result) {
                        var data = $.parseJSON(result);//parse JSON
                        //Ajax request success
                        if (data.status == 'ok') {
                            $('#ModalEditCommentary').modal('hide');
                            $('#ModalCommentaries').modal('hide');
                            // status ok actions
                        } else {
                            // status error actions
                            $('#ModalEditCommentary').modal('hide');
                            $('#ModalCommentaries').modal('hide');
                            $('#ModalError').modal('show');
                        }
                    },
                    error: function (xhr) {
                        //Ajax request error
                    }
                })
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
$(document).ready(function() {




});