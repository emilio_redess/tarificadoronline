    
// ESTE ARCHIVO NO LO ESTOY USANDO
    $(document).ready(function(){

        $( "#selectCias" ).change(function() {

            var id = $('#selectCias').val();
            //var nombreCia = $("#selectCias option:selected").text();

            // Vaciamos el select de productos para llenarlo con los productos que corresponda
            $('option', '#selectProducto').not(':eq(0)').remove();
            if (id == "")
                $( "#selectProducto" ).prop( "disabled", true );    
            else            
                $( "#selectProducto" ).prop( "disabled", false );    // Activamos el select del producto que estaba previamente en estado disabled        

            $.ajax({
                type:'POST',
                data:{id: id},
                url: JS_BASE_URL + "/admin/salud/ajax_getProducto/",
                success: function(result){
                   var json_obj = $.parseJSON(result);//parse JSON

                    for (var i in json_obj) 
                    {

                        $('#selectProducto').append($('<option>', {
                            value: json_obj[i].id,
                            text: json_obj[i].nombre_producto
                        }));

                    }
                }
            });
        });
    });