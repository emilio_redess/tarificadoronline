    $(document).ready(function(){

        $( ".ajax_select" ).change(function() {

            var idCia = $('#selectCias').val();
            var idCorreduria = $('#selectCorredurias').val();
            var estadoActivo = $('#estadoActivo').val();

            // estadoActivo nos sirve para decirle a la consulta si queremos sacar los productos activos, no activos, o ambos.
            // estadoActivo = 0  -> solo productos no activos
            // estadoActivo = 1  -> solo productos activos            
            // estadoActivo = 2  -> todos los productos, tanto activos como no activos
            if (estadoActivo == null)
                estadoActivo = "1";
            // Vaciamos el select de productos para llenarlo con los productos que corresponda
            $( ".selectProducto" ).each(function( index ) {
                //$('option', '.selectProducto').not(':eq(0)').remove();
                $('option', this).not(':eq(0)').remove();
            });

            if ((idCia == "") ||  (idCorreduria == "")){
                $( ".selectProducto" ).prop( "disabled", true );  
            }
            else{   
                $( ".selectProducto" ).prop( "disabled", false );    // Activamos el select del producto que estaba previamente en estado disabled  


                $.ajax({
                    type:'POST',
                    data:{idCia: idCia,
                          idCorreduria: idCorreduria},
                    url: JS_BASE_URL + "/admin/productos_decesos/ajax_getProductoCorreduria/" + estadoActivo,
                    success: function(result){
                       var json_obj = $.parseJSON(result);//parse JSON

                        for (var i in json_obj) 
                        {                                                  
                            $('.selectProducto').append($('<option>', {
                                value: json_obj[i].id,
                                text: json_obj[i].nombre_producto
                            }));
                        }
                    }
                });
            }
        });



        $( ".selectProducto" ).change(function() {

             var idProducto = $('#selectProducto').val();

             if (idProducto == "") {
                $( "#checkActivar" ).prop( "disabled", true );
                $(".mk-trc").attr("data-color", "deep-grey");
                $(".mk-trc").data("color", "deep-grey");
                $('#checkActivar').prop('checked', false);


             }
             else{
                $( "#checkActivar" ).prop( "disabled", false );

                $(".mk-trc").attr("data-color", "deep-orange");
                $(".mk-trc").data("color", "deep-orange");



             }
             



        });

    });