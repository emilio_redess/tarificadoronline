function datejs_format_function(dateString) {
    var date = new Date(dateString);
    
   // Como en javascript los meses van de 0-11, le sumamos 1 para que muestre el valor esperado.
    var mes = date.getMonth( ) + 1;
    return date.getDate()+"/"+mes+"/"+date.getFullYear() + "  " + date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes();
}