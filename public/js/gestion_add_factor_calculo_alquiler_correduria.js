    $(document).ready(function(){

        $( ".selectProducto" ).change(function() {

            var idCia = $('#selectCias').val();
            var idProducto = $('#selectProducto').val();
            var idCorreduria = $('#selectCorreduria').val();
    

            $( ".input_fc" ).val("");
            $( "idProd" ).val("");

            if ((idCia == "") ||  (idProducto == "")){
                $( ".input_fc" ).prop( "disabled", true );  
            }
            else{   
                $( ".input_fc" ).prop( "disabled", false );    // Activamos el select del producto que estaba previamente en estado disabled  


                $.ajax({
                    type:'POST',
                    data:{idCia: idCia,
                        idCorreduria: idCorreduria,
                          idProducto: idProducto},
                    url: JS_BASE_URL + "/admin/productos_alquiler/ajax_getFactorCalculoCorreduria",
                    success: function(result){
                       var json_obj = $.parseJSON(result);//parse JSON
                       $("#fc_actual").val(json_obj.fc);
                       $("#idProd").val(json_obj.idProd);

                       
                    }
                });
            }
        });
    });