<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Controlador encargado de recibir peticion de tarificaciones, guardar los datos de dicha tarificacion en BBDD y devolver las primas calculadas.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Emilio Valls
 */
class Tarificador_seguro_alquiler extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('tarificaciones_model');
        $this->load->model('admin/funciones_comunes_model');


        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['alquiler_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['alquiler_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function coberturas_get()
    {
        $idProductoCorreduria = $this->get('id');

        $datosCoberturasTemp = $this->funciones_comunes_model->getCoberturasAlquiler($idProductoCorreduria);

        if (is_null($datosCoberturasTemp))
            $datosCoberturas = NULL;
        else
            $datosCoberturas = $datosCoberturasTemp->data;


        $datos_tarif = array(
            'coberturas' => $datosCoberturas
        );


        $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code    

    }    

  

    public function alquiler_get()
    {
        $id = $this->get('id');

        // If the id parameter doesn't exist:
        if ($id === NULL)
        {

        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $tarificacion = $this->tarificaciones_model->getDatosTarificacion($id);

        if (!empty($tarificacion))
        {
            foreach ($tarificacion->result_array() as $row)
            {

                // para todos los productos devueltos en la variable $row['data'], hay que averiguar los datos de: nombreCia, logo y enviarlo, y devolverlo
                // sustituyendo a la key 'primas' de la variable $datos_tarif.
                // La idea es devolver un array multidimensional:
                // Ejemplo:
                /*
                array(
                    "15" => array(
                        "nomCia" => "Generali",
                        "logo" => "generali.png",
                        "prima" => "450"
                        ),
                    "16" => array(
                        "nomCia" => "mapfre",
                        "logo" => "mapfre.png",
                        "prima" => "200"
                        ),                    
                    )
                */

                $primas = array();
                $arrayPrimas = json_decode($row['data'],true);
                foreach ($arrayPrimas["primas"] as $k => $v) {
                    $datosProducto = $this->funciones_comunes_model->getDatosProducto($v['id']);

                    $datosCoberturasTemp = $this->funciones_comunes_model->getCoberturasAlquiler($k);

                    if (is_null($datosCoberturasTemp))
                        $datosCoberturas = NULL;
                    else
                        $datosCoberturas = $datosCoberturasTemp->data;



                    $primas[$k] = array(
                        "nombreProducto" => $datosProducto->nomProducto,
                        "idproducto" => $k,
                        "idproducto_general" => $v["id"],
                        "logoProducto" => $datosProducto->logoProducto,
                        "nombreCia" => $datosProducto->nomCia,
                        //"formaPagoProducto" => $datosProducto->formaPagoNombre,
                        //"prima" => $v["prima"],
                        "prima_6m" => $v["prima_6m"],
                        "prima_9m" => $v["prima_9m"],
                        "prima_12m" => $v["prima_12m"],
                        "prima_18m" => $v["prima_18m"],
                        "coberturas" => $datosCoberturas
                        );
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Obtenemos el origen de la tarificacion (cliente web o usuario en el backend)
                if (is_null($row['creada_por']))
                    $origen_nombre = "Web";
                else{
                    $origen = $this->ion_auth->user($row['creada_por'])->row();
                    $origen_nombre = $origen->first_name . " " . $origen->last_name;
                }                

                $datos_tarif = array(
                    'ramoId' => $row['ramo_id'],
                    'correduriaId' => $row['correduria_id'],
                    'codtar' => $row['codigo_tarificacion'],
                    'nombreApellidos' => $row['nombre_completo'],
                    'telefono' => $row['telefono'],
                    'email' => $row['email'],
                    'primas' => $primas,
                    'fechaTarificacion' => $row['fecha_creacion'],
                    'creadaPorId' => $row['creada_por'],
                    'creadaPor' => $origen_nombre,
                    'valorAlquiler' => $row['alquiler_valorAlquiler'],
                    'Tipo de local' => $arrayPrimas["Tipo local"],
                );
            }            


            $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'No se ha encontrado la tarificacion'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }        


    }

    public function alquiler_post()
    {
        $user_id = $this->post('user_id');
        $correduria = $this->post('correduria');
        $nombreApellidos = $this->post('nombreApellidos');        
        $email = $this->post('email');
        $telefono = $this->post('telefono');
        $valorAlquiler = $this->post('valorAlquiler');
        $tipoLocal = $this->post('tipoLocal');
        if ((is_null($tipoLocal)) or ($tipoLocal === ""))
            $tipoLocal = "NODEF";

        // Averigua la id de la correduria
        //$correduria_id = $this->tarificaciones_model->get_correduria_id($correduria);


        /////////////////////////////////// Lógica para los colaboradores ///////////////////////////////////////
        // Se van a obtener las id de la correduria que envia la peticion y de su parent en caso de que lo tenga. Si no tiene, entonces ambas id tendran el mismo valor correspodiente a la propia correduria que envia la peticion.
        // De esta forma, al obtener los productos asociados a la correduria, se hace respecto a su parent_id, y al guardar la tarificacion, se hace sobre su propia id.
        $correduria_obj = $this->tarificaciones_model->get_datos_correduria($correduria);
        $correduria_id = $correduria_obj->id;
        if (is_null($correduria_obj->parent_id))
        {
            $parent_correduria_id = $correduria_obj->id;
        }
        else
        {
            $parent_correduria_obj = $this->tarificaciones_model->get_parent_correduria($correduria_obj->parent_id);
            $parent_correduria_id = $parent_correduria_obj->id;

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        

        // Saco los productos de alquiler y su factor de calculo asociados a la correduria
        $productos = $this->funciones_comunes_model->getProductosAlquilerCorreduria($parent_correduria_id,$tipoLocal);

        // Obtengo todos los descuentos/impuestos para cada producto devuelto de la llamada anterior. 
        $impuestos_recargos = array();
        foreach ($productos->result_array() as $row)
        {
            $aux = $this->funciones_comunes_model->getImpuestosRecargosAlquiler($row['id'],$tipoLocal);
            $impuestos_recargos[$row['id']] = $aux;
        }



        // Calculo el codigo de referencia usando una funcion que genera strings random
        $referencia = random_string(CODIGO_TARIFICACION_TIPO, CODIGO_TARIFICACION_LONGITUD);

        //calcular primas. Estaran en formato JSON
        $primas_arr = $this->calculo_primas->calcular_primas_alquiler($productos,$valorAlquiler,$impuestos_recargos,$tipoLocal);

        if ($primas_arr["success"] === TRUE){


            $return_arr = $this->tarificaciones_model->nueva_tarificacion_alquiler($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$valorAlquiler,$primas_arr["result"],$referencia);  

            



            if ($return_arr["success"] === TRUE){
                $message = [
                    'id' => $return_arr["id"], // Automatically generated by the model
                    'success' => $return_arr["success"],
                    //'queryString' => $return_arr["queryString"],
                    //'prima' => $primas,
                    'message' => 'Tarificación creada'
                ];                

                $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
            }
            else{
                $message = [
                   
                    'success' => $return_arr["success"],
                    //'queryString' => $return_arr["queryString"],
                    //'prima' => $primas,
                    'message' => $return_arr["queryString"]
                ];                
                $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // CREATED (201) being the HTTP response code
            }

        }else{

            $message = [
                'id' => $return_arr["id"], // Automatically generated by the model
                'success' => $return_arr["success"],
                //'queryString' => $return_arr["queryString"],
                //'prima' => $primas,
                'message' => 'Error al intentar calcular las primas'
            ];


            $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // Error en el servidor al intentar calcular primas
        }
    }
}
