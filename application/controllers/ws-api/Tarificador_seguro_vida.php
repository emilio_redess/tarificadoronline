<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Controlador encargado de recibir peticion de tarificaciones, guardar los datos de dicha tarificacion en BBDD y devolver las primas calculadas.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Emilio Valls
 */
class Tarificador_seguro_vida extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('tarificaciones_model');
        $this->load->model('admin/funciones_comunes_model');


        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['vida_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['vida_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function coberturas_get()
    {
/*
        $idProductoCorreduria = $this->get('id');

        $datosCoberturasTemp = $this->funciones_comunes_model->getCoberturasDecesos($idProductoCorreduria);

        if (is_null($datosCoberturasTemp))
            $datosCoberturas = NULL;
        else
            $datosCoberturas = $datosCoberturasTemp->data;


        $datos_tarif = array(
            'coberturas' => $datosCoberturas
        );


        $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code    
*/
    }  

    public function documentos_get()
    {
        /*
        $idProductoCorreduria = $this->get('id');

        $datosDocumentosTemp = $this->funciones_comunes_model->getDocumentosDecesos($idProductoCorreduria);

        if (is_null($datosDocumentosTemp))
        {
            $datosDocumentos = NULL;
        }
        else
        {
            $datosDocumentos = $datosDocumentosTemp->data;

        }  

        $datos_tarif = array(
            'documentos' => $datosDocumentos

        );


        $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code    
*/
    }       

  

    public function vida_get()
    {
        $id = $this->get('id');

        // If the id parameter doesn't exist:
        if ($id === NULL)
        {

        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $tarificacion = $this->tarificaciones_model->getDatosTarificacion($id);

        if (!empty($tarificacion))
        {
            foreach ($tarificacion->result_array() as $row)
            {

                // para todos los productos devueltos en la variable $row['data'], hay que averiguar los datos de: nombreCia, logo y enviarlo, y devolverlo
                // sustituyendo a la key 'primas' de la variable $datos_tarif.
                // La idea es devolver un array multidimensional:
                // Ejemplo:
                /*
                array(
                    "15" => array(
                        "nomCia" => "Generali",
                        "logo" => "generali.png",
                        "prima" => "450"
                        ),
                    "16" => array(
                        "nomCia" => "mapfre",
                        "logo" => "mapfre.png",
                        "prima" => "200"
                        ),                    
                    )
                */

                $primas = array();
                $arrayPrimas = json_decode($row['data'],true);
                foreach ($arrayPrimas as $k => $v) {
                    $datosProducto = $this->funciones_comunes_model->getDatosProducto($v['id']);

                    $datosCoberturasTemp = $this->funciones_comunes_model->getCoberturasVida($k);

                    if (is_null($datosCoberturasTemp))
                        $datosCoberturas = NULL;
                    else
                        $datosCoberturas = $datosCoberturasTemp->data;



                    $primas[$k] = array(
                        "nombreProducto" => $datosProducto->nomProducto,
                        "idproducto" => $k,
                        "idproducto_general" => $v["id"],
                        "logoProducto" => $datosProducto->logoProducto,
                        "nombreCia" => $datosProducto->nomCia,
                        "formaPagoProducto" => $datosProducto->formaPagoNombre,
                        "prima_f" => $v["prima_f"],                        
                        "prima_i" => $v["prima_i"],                        
                        "prima_fi" => $v["prima_fi"],                        
                        "prima_doble" => $v["prima_doble_capital"],                        
                        "prima_triple" => $v["prima_triple_capital"],                        
                        "coberturas" => $datosCoberturas
                        );
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Obtenemos el origen de la tarificacion (cliente web o usuario en el backend)
                if (is_null($row['creada_por']))
                    $origen_nombre = "Web";
                else{
                    $origen = $this->ion_auth->user($row['creada_por'])->row();
                    $origen_nombre = $origen->first_name . " " . $origen->last_name;
                }                

                $datos_tarif = array(
                    'ramoId' => $row['ramo_id'],
                    'correduriaId' => $row['correduria_id'],
                    'codtar' => $row['codigo_tarificacion'],
                    'nombreApellidos' => $row['nombre_completo'],
                    'telefono' => $row['telefono'],
                    'email' => $row['email'],
                    'primas' => $primas,
                    'fechaTarificacion' => $row['fecha_creacion'],
                    'creadaPorId' => $row['creada_por'],
                    'creadaPor' => $origen_nombre,
                    'capital' => $row['vida_capital'],
                    'edad' => $row['vida_edad']
                   
                );
            }            


            $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code. 
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'No se ha encontrado la tarificacion'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }        


    }

    public function vida_post()
    {
        $user_id = $this->post('user_id');
        $correduria = $this->post('correduria');
        $nombreApellidos = $this->post('nombreApellidos');        
        $email = $this->post('email');
        $telefono = $this->post('telefono');
        $capital = $this->post('capital');
        $edad = $this->post('edad');

        /////////////////////////////////// Lógica para los colaboradores ///////////////////////////////////////
        // Se van a obtener las id de la correduria que envia la peticion y de su parent en caso de que lo tenga. Si no tiene, entonces ambas id tendran el mismo valor correspondiente a la propia correduria que envia la peticion.
        // De esta forma, al obtener los productos asociados a la correduria, se hace respecto a su parent_id, y al guardar la tarificacion, se hace sobre su propia id.
        $correduria_obj = $this->tarificaciones_model->get_datos_correduria($correduria);
        $correduria_id = $correduria_obj->id;
        if (is_null($correduria_obj->parent_id))
        {
            $parent_correduria_id = $correduria_obj->id;
        }
        else
        {
            $parent_correduria_obj = $this->tarificaciones_model->get_parent_correduria($correduria_obj->parent_id);
            $parent_correduria_id = $parent_correduria_obj->id;

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // Saco los productos de vida asociados a la correduria
        $productos = $this->funciones_comunes_model->getProductosVidaCorreduria($parent_correduria_id);


        // Obtengo todos los descuentos/impuestos para cada producto devuelto de la llamada anterior. 
        $impuestos_recargos = array();
        foreach ($productos->result_array() as $row)
        {
            $aux = $this->funciones_comunes_model->getImpuestosRecargosVida($row['id_producto_correduria'],$capital);
            $impuestos_recargos[$row['id']] = $aux;
        }
//log_message('debug', "textomensaje");

        // Calculo el codigo de referencia usando una funcion que genera strings random
        $referencia = random_string(CODIGO_TARIFICACION_TIPO, CODIGO_TARIFICACION_LONGITUD);

        //calcular primas. Estaran en formato JSON
        $primas_arr = $this->calculo_primas->calcular_primas_vida($productos,$capital,$edad,$impuestos_recargos);

        if ($primas_arr["success"] === TRUE){


            $return_arr = $this->tarificaciones_model->nueva_tarificacion_vida($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$capital,$edad,$primas_arr["result"],$referencia);  

            
            


            if ($return_arr["success"] === TRUE){
                $message = [
                    'id' => $return_arr["id"], // Automatically generated by the model
                    'success' => $return_arr["success"],
                    //'queryString' => $return_arr["queryString"],
                    //'prima' => $primas,
                    'message' => 'Tarificación creada'
                ];                

                $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
            }
            else{
                $message = [
                   
                    'success' => $return_arr["success"],
                    //'queryString' => $return_arr["queryString"],
                    //'prima' => $primas,
                    'message' => $return_arr["queryString"]
                ];                
                $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // CREATED (201) being the HTTP response code
            }

        }else{

            $message = [
                'id' => $return_arr["id"], // Automatically generated by the model
                'success' => $return_arr["success"],
                //'queryString' => $return_arr["queryString"],
                //'prima' => $primas,
                'message' => 'Error al intentar calcular las primas'
            ];


            $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // Error en el servidor al intentar calcular primas
        }
    }
}
