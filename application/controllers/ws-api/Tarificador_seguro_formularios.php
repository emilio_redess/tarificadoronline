<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Controlador encargado de recibir peticion de tarificaciones, guardar los datos de dicha tarificacion en BBDD.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Emilio Valls
 */
class Tarificador_seguro_formularios extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('tarificaciones_model');
        $this->load->model('admin/funciones_comunes_model');


        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['formulario_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['formulario_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function formulario_get()
    {
        $id = $this->get('id');

        // If the id parameter doesn't exist:
        if ($id === NULL)
        {

        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $tarificacion = $this->tarificaciones_model->getDatosTarificacion($id);

        if (!empty($tarificacion))
        {
            foreach ($tarificacion->result_array() as $row)
            {

    
    
                $primas = array();
                $arrayPrimas = json_decode($row['data'],true);
                foreach ($arrayPrimas as $k => $v) {
                   

                    $primas[$k] = $v;
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Obtenemos el origen de la tarificacion (cliente web o usuario en el backend)
                if (is_null($row['creada_por']))
                    $origen_nombre = "Web";
                else{
                    $origen = $this->ion_auth->user($row['creada_por'])->row();
                    $origen_nombre = $origen->first_name . " " . $origen->last_name;
                }                

                $datos_tarif = array(
                    'ramoId' => $row['ramo_id'],
                    'correduriaId' => $row['correduria_id'],
                    'codtar' => $row['codigo_tarificacion'],
                    'nombreApellidos' => $row['nombre_completo'],
                    'telefono' => $row['telefono'],
                    'email' => $row['email'],
                    'primas' => $primas,
                    'fechaTarificacion' => $row['fecha_creacion'],
                    'creadaPorId' => $row['creada_por'],
                    'creadaPor' => $origen_nombre,
                );
            }            


            $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'No se ha encontrado la tarificacion'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }        


    }

    public function formulario_post()
    {
        $user_id = $this->post('user_id');
        $correduria = $this->post('correduria');
        $nombreApellidos = $this->post('nombreApellidos');        
        $email = $this->post('email');
        $telefono = $this->post('telefono');
        $tipo_seguro = $this->post('tipo_seguro');



        // Se coje el array recibido, y le quitamos las variables comunes (nombre y apellidos,correduria,email,telefono,tipo seguro) para dejar
        // solamente las posibles variables cuyo nombre no sepamos cual es.
        $datosRecibidosArr = $this->post();

        unset($datosRecibidosArr['correduria']);
        unset($datosRecibidosArr['nombreApellidos']);
        unset($datosRecibidosArr['email']);
        unset($datosRecibidosArr['telefono']);
        unset($datosRecibidosArr['tipo_seguro']);

        // Sustituimos el caracter '_' por un espacio en todos los elementos indice del array para la vista de detalle de la tarificacion
        foreach ($datosRecibidosArr as $k => $v) { 
            $aux = str_replace("_", " ", $k);
            $datosRecibidosArr[$aux] = $v;
            unset($datosRecibidosArr[$k]);
        }

        $datosRecibidosArr['Tipo de seguro'] = $tipo_seguro;

        $cadenaJson = json_encode($datosRecibidosArr);

     

        // Averigua la id de la correduria

        /////////////////////////////////// Lógica para los colaboradores ///////////////////////////////////////
        $correduria_obj = $this->tarificaciones_model->get_datos_correduria($correduria);
        $correduria_id = $correduria_obj->id;
       
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////          


        // Calculo el codigo de referencia usando una funcion que genera strings random
        $referencia = random_string(CODIGO_TARIFICACION_TIPO, CODIGO_TARIFICACION_LONGITUD);


        $return_arr = $this->tarificaciones_model->nueva_tarificacion_formulario($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$referencia,$cadenaJson);  


        if ($return_arr["success"] === TRUE){
            $message = [
                'id' => $return_arr["id"], // Automatically generated by the model
                'success' => $return_arr["success"],
                'message' => 'Tarificación creada'
            ];                

            $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }else{
            $message = [
                   
                'success' => $return_arr["success"],
                'message' => $return_arr["queryString"]
            ];                
            $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // CREATED (201) being the HTTP response code
        } 
    }
}
