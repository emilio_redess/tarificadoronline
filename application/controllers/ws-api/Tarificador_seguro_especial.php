<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Controlador encargado de recibir peticion de tarificaciones, guardar los datos de dicha tarificacion en BBDD.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Emilio Valls
 */
class Tarificador_seguro_especial extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('tarificaciones_model');
        $this->load->model('admin/funciones_comunes_model');


        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['especial_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['especial_post']['limit'] = 100; // 100 requests per hour per user/key
    }

    public function especial_get()
    {
        $id = $this->get('id');

        // If the id parameter doesn't exist:
        if ($id === NULL)
        {

        }

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        $tarificacion = $this->tarificaciones_model->getDatosTarificacion($id);

        if (!empty($tarificacion))
        {
            foreach ($tarificacion->result_array() as $row)
            {
                $primas = array();
                $ramo_subcategoria_nombre = "";

                if (!is_null($row['data']))
                {
                    $arrayPrimas = json_decode($row['data'],true);

                    $ramo_subcategoria_id = $arrayPrimas["ramo_especial"];
                    $ramo_subcategoria_obj = $this->funciones_comunes_model->getSubramo($ramo_subcategoria_id);
                    $ramo_subcategoria_nombre = $ramo_subcategoria_obj->nombre;

                    foreach ($arrayPrimas["primas"] as $k => $v) {
                        $datosProducto = $this->funciones_comunes_model->getDatosProducto($k);
                       
                        $primas[$k] = array(
                            "nombreProducto" => $datosProducto->nomProducto,
                            "logoProducto" => $datosProducto->logoProducto,
                            "nombreCia" => $datosProducto->nomCia,
                            "formaPagoProducto" => $datosProducto->formaPagoNombre,
                            "prima" => $v
                            );
                    }

                    $params = array ();
                    foreach ($arrayPrimas["params"] as $k => $v) {
                        $params[$k] = $v;
                    }

                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                // Obtenemos el origen de la tarificacion (cliente web o usuario en el backend)
                if (is_null($row['creada_por']))
                    $origen_nombre = "Web";
                else{
                    $origen = $this->ion_auth->user($row['creada_por'])->row();
                    $origen_nombre = $origen->first_name . " " . $origen->last_name;
                }                

                $datos_tarif = array(
                    'ramo_subcategoria' => $ramo_subcategoria_nombre,
                    'ramoId' => $row['ramo_id'],
                    'correduriaId' => $row['correduria_id'],
                    'codtar' => $row['codigo_tarificacion'],
                    'nombreApellidos' => $row['nombre_completo'],
                    'telefono' => $row['telefono'],
                    'email' => $row['email'],
                    'primas' => $primas,
                    'params' => $params,
                    'fechaTarificacion' => $row['fecha_creacion'],
                    'creadaPorId' => $row['creada_por'],
                    'creadaPor' => $origen_nombre,
                );
            }            


            $this->set_response($datos_tarif, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'No se ha encontrado la tarificacion'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }        


    }

    public function especial_post()
    {
        $user_id = $this->post('user_id');
        $correduria = $this->post('correduria');
        $nombreApellidos = $this->post('nombreApellidos');        
        $email = $this->post('email');
        $telefono = $this->post('telefono');
        $id_ramo_especial = $this->post('id_ramo_especial');

     

        // Averigua la id de la correduria
        //$correduria_id = $this->tarificaciones_model->get_correduria_id($correduria);

        /////////////////////////////////// Lógica para los colaboradores ///////////////////////////////////////
        // Se van a obtener las id de la correduria que envia la peticion y de su parent en caso de que lo tenga. Si no tiene, entonces ambas id tendran el mismo valor correspodiente a la propia correduria que envia la peticion.
        // De esta forma, al obtener los productos asociados a la correduria, se hace respecto a su parent_id, y al guardar la tarificacion, se hace sobre su propia id.
        $correduria_obj = $this->tarificaciones_model->get_datos_correduria($correduria);
        $correduria_id = $correduria_obj->id;
        if (is_null($correduria_obj->parent_id))
        {
            $parent_correduria_id = $correduria_obj->id;
        }
        else
        {
            $parent_correduria_obj = $this->tarificaciones_model->get_parent_correduria($correduria_obj->parent_id);
            $parent_correduria_id = $parent_correduria_obj->id;

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////        

        // Saco los productos especiales y su forma de pago
        $productos = $this->funciones_comunes_model->getProductosEspecialCorreduria($parent_correduria_id,$id_ramo_especial);        


        // Calculo el codigo de referencia usando una funcion que genera strings random
        $referencia = random_string(CODIGO_TARIFICACION_TIPO, CODIGO_TARIFICACION_LONGITUD);



        //calcular primas. Estaran en formato JSON


        switch ($id_ramo_especial) {
            case "1": // deportes riesgo CANARISK
                $duracion = $this->post('duracion');
                $fechaIni = $this->post('fechaIni');
                $fechaFin = $this->post('fechaFin');
                $residente = $this->post('residente');
                $residenteNombre = $this->post('residenteNombre');
                $zonaCobertura = $this->post('zonaCobertura');
                $zonaCoberturaNombre = $this->post('zonaCoberturaNombre');
                $numAseg = $this->post('numAseg');
                $primas_arr = $this->calculo_primas->calcular_primas_especial_deportesRiesgo($productos,$id_ramo_especial,$fechaIni,$fechaFin,$duracion,$residente,$residenteNombre,$zonaCobertura,$zonaCoberturaNombre,$numAseg);
                break;

            case "2": // seguro de transportes solana (seguroweb)

                $limite_viaje = $this->post('limite_viaje');
                $garantias = $this->post('garantias');
                $averia_frio = $this->post('averia_frio');

                $primas_arr = $this->calculo_primas->calcular_primas_especial_transporte($productos,$id_ramo_especial,$limite_viaje,$garantias,$averia_frio);
                break;     

            case "3": // seguro de R.C. transportes solana (seguroweb)

                $suma = $this->post('suma');
                //$facturacion = $this->post('facturacion');
                $adr = $this->post('adr');
                $ambito = $this->post('ambito');

                $primas_arr = $this->calculo_primas->calcular_primas_especial_rctransporte($productos,$id_ramo_especial,$suma,$adr,$ambito);
                break;                              
        }


        if ($primas_arr["success"] === TRUE){

            $return_arr = $this->tarificaciones_model->nueva_tarificacion_especial($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$primas_arr["result"],$referencia);  

            if ($return_arr["success"] === TRUE){
                $message = [
                    'id' => $return_arr["id"], // Automatically generated by the model
                    'success' => $return_arr["success"],
                    'message' => 'Tarificación creada'
                ];                

                $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
            }else{
                $message = [
                       
                    'success' => $return_arr["success"],
                    'message' => $return_arr["queryString"]
                ];                
                $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // CREATED (201) being the HTTP response code
            } 
        }else{

            $message = [
                'id' => $return_arr["id"], // Automatically generated by the model
                'success' => $return_arr["success"],
                //'queryString' => $return_arr["queryString"],
                //'prima' => $primas,
                'message' => 'Error al intentar calcular las primas'
            ];


            $this->set_response($message, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // Error en el servidor al intentar calcular primas
        }
    }
}
