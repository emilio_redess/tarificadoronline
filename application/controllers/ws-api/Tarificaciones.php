<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Controlador encargado de recibir peticion de tarificaciones
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Emilio Valls
 */
class Tarificaciones extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('tarificaciones_model');
        $this->load->model('admin/funciones_comunes_model');


        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['listado_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['listadoAnotaciones_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['listadoTodasAnotaciones_get']['limit'] = 500; // 500 requests per hour per user/key
    }

    public function listado_get()
    {
        $correduria_id = $this->get('cid');
        $ramo_id = $this->get('rid');
        $fecha_ini = $this->get('inid');
        $filter = $this->get('filter');
        
        $query = $this->tarificaciones_model->getListadoTarificaciones($correduria_id,$ramo_id,$fecha_ini,$filter);

        if (is_null($query))
            $datos = NULL;
        else
            $datos = $query->result_array();


 


        $this->set_response($datos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code    

    }   

    public function listadoTodasAnotaciones_get()
    {
        $correduria_id = $this->get('cid');
        $ramo_id = $this->get('rid');
        $fecha_ini = $this->get('inid');
        $ordenar = $this->get('sort');

        $query = $this->tarificaciones_model->getListadoTodasAnotaciones($correduria_id,$ramo_id,$fecha_ini,$ordenar);

        if (is_null($query))
            $datos = NULL;
        else
            $datos = $query->result_array();


 


        $this->set_response($datos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code          

    }

    public function listadoAnotaciones_get()
    {
        $tarif_id = $this->get('tid');

        $query = $this->tarificaciones_model->getListadoAnotaciones($tarif_id);

        if (is_null($query))
            $datos = NULL;
        else
            $datos = $query->result_array();


 


        $this->set_response($datos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response cod


    } 
}
