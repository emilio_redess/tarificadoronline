<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tarificaciones extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('tarificaciones_model');

                if (!$this->ion_auth->logged_in())
                {
                        redirect('auth/login', 'refresh');
                }
        }

        public function index()
        {
        	/*
                $data['tarificaciones'] = $this->tarificador_model->get_tarificaciones();
                $data['title'] = 'Listado de tarificaciones';

		        $this->load->view('templates/header', $data);
		        $this->load->view('tarificador/index', $data);
		        $this->load->view('templates/footer');
		        */ 
        }

        public function vaciarPapelera()
        {
                $resul = $this->tarificaciones_model->emptyTrash();

                if ($resul){
                        $user = $this->ion_auth->user()->row();
                        $data['nombreApellidos'] =  $user->first_name . " " . $user->last_name;
                        $data['title'] = 'Listado de tarificaciones de la papelera de reciclaje';
                        $data['ramo'] = 0;
                        $data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapelera.js');
                        $this->load->view('templates/header');
                        $this->load->view('templates/header_tarificador');
                        $this->load->view('tarificador/papelera', $data);
                        $this->load->view('templates/footer');
                }
                else{
                        $errorMsg = 'Ha habido un error al intentar eliminar todas las tarificaciones de la papelera.<br>Se recomienda volver a intentarlo, 
                        y en caso de que vuelva a fallar, ponte en contacto con Redess a traves del siguiente <a href="mailto:stecnico@redess.es?Subject=Error%20tarificador" target="_top">enlace de correo electrónico</a>
                        especificando el error mostrado en esta página.<br><br>' . anchor('tarificador/listado/', 'Volver al tarificador');

                        show_error($errorMsg,500,'Se ha producido un error');
                }

        }


        public function borrarTarificacion($tarificacionId)
        {
                $resul = $this->tarificaciones_model->deleteSingle($tarificacionId);

                if ($resul){
                        $user = $this->ion_auth->user()->row();
                        $data['nombreApellidos'] =  $user->first_name . " " . $user->last_name;
                        $data['title'] = 'Listado de tarificaciones de la papelera de reciclaje';
                        $data['ramo'] = 0;
                        $data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapelera.js');
                        $this->load->view('templates/header');
                        $this->load->view('templates/header_tarificador');
                        $this->load->view('tarificador/papelera', $data);
                        $this->load->view('templates/footer');
                }
                else{
                        $errorMsg = 'Ha habido un error al intentar eliminar la tarificación con la referencia ' . $tarificacionId . ' de la base de datos.<br>Se recomienda volver a intentarlo, 
                        y en caso de que vuelva a fallar, ponte en contacto con Redess a traves del siguiente <a href="mailto:stecnico@redess.es?Subject=Error%20tarificador" target="_top">enlace de correo electrónico</a>
                        especificando el error mostrado en esta página.<br><br>' . anchor('tarificador/listado/', 'Volver al tarificador');

                        show_error($errorMsg,500,'Se ha producido un error');                        
                }
        }

        public function restaurarTarificacion($tarificacionId)
        {
                $resul = $this->tarificaciones_model->restoreSingle($tarificacionId);

                if ($resul){
                        $user = $this->ion_auth->user()->row();
                        $data['nombreApellidos'] =  $user->first_name . " " . $user->last_name;
                        $data['title'] = 'Listado de tarificaciones de la papelera de reciclaje';
                        $data['ramo'] = 0;
                        $data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapelera.js');
                        $this->load->view('templates/header');
                        $this->load->view('templates/header_tarificador');
                        $this->load->view('tarificador/papelera', $data);
                        $this->load->view('templates/footer');
                }
                else{
                        $errorMsg = 'Ha habido un error al intentar restaurar la tarificación con la referencia ' . $tarificacionId . ' de la base de datos.<br>Se recomienda volver a intentarlo, 
                        y en caso de que vuelva a fallar, ponte en contacto con Redess a traves del siguiente <a href="mailto:stecnico@redess.es?Subject=Error%20tarificador" target="_top">enlace de correo electrónico</a>
                        especificando el error mostrado en esta página.<br><br>' . anchor('tarificador/listado/', 'Volver al tarificador');

                        show_error($errorMsg,500,'Se ha producido un error');                        
                }
                

        }


        /* $ramo_return se usa para saber a que vista del listado de tarificaciones hay que volver una vez se ha enviado una tarif. a la papelera */
        public function enviarTarificacionPapelera($ramo_return,$tarificacionId)
        {
                $resul = $this->tarificaciones_model->sendToTrash($tarificacionId);

                if ($resul){
                        $user = $this->ion_auth->user()->row();
                        $data['nombreApellidos'] =  $user->first_name . " " . $user->last_name;
                       // $data['title'] = 'Listado de tarificaciones de todos los ramos';
                        $data['title'] = constant("TITLE_LISTADO_TARIFICACIONES_" . $ramo_return);
                        $data['ramo'] = $ramo_return;
                        $data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');
                        $this->load->view('templates/header');
                        $this->load->view('templates/header_tarificador');
                        $this->load->view('tarificador/index', $data);
                        $this->load->view('templates/footer');
                }
                else{
                        $errorMsg = 'Ha habido un error al intentar enviar a la papelera la tarificación con la referencia ' . $tarificacionId . '.<br>Se recomienda volver a intentarlo, 
                        y en caso de que vuelva a fallar, ponte en contacto con Redess a traves del siguiente <a href="mailto:stecnico@redess.es?Subject=Error%20tarificador" target="_top">enlace de correo electrónico</a>
                        especificando el error mostrado en esta página.<br><br>' . anchor('tarificador/listado/', 'Volver al tarificador');

                        show_error($errorMsg,500,'Se ha producido un error');                        
                }
        }

}