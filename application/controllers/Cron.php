<?php


class Cron extends CI_Controller {


    public function __construct() {
			parent::__construct();
			$this->load->model('cron_jobs_model');
    }


	public function index(){}

	public function enviarAvisos() { 

/*
		// En linea de comandos: cd /ruta/index.php : php index.php cron enviarAvisos
		// Si la instruccion cron es CLI, la llamada seria algo asi: php /ruta/index.php cron enviarAvisos
		if(!$this->input->is_cli_request())
		{
			echo "Cron may only be accessed from the command line";
			return;
		}
*/
		$resul = $this->cron_jobs_model->get_anotaciones_diarias();

		$items = array();
		$item = array();
		foreach ($resul->result() as $row)
		{
			$item["id"] = $row->id;
			$item["nombre"] = $row->first_name;
			$item["apellidos"] = $row->last_name;
			$item["email"] = $row->email;

			// obtenemos el numero de anotaciones pendientes del usuario
			$resul2 = $this->cron_jobs_model->getNumeroMensajesPendientes($row->id);
			$item["cantidad"] = $resul2->cantidad;

			// Ahora se obtienen los codigos de referencia de las tarificaciones que tengan anotaciones pendientes
			$resul3 = $this->cron_jobs_model->getCodigosReferenciaPendientes($row->id);
			$codigosRef = array();
			foreach ($resul3->result() as $row2)
			{
				array_push($codigosRef, $row2->codigo_tarificacion);
			}
			$item["codigosRef"] = $codigosRef;


			$items[$row->id] = $item;
		}

		echo "Empezando el envio de correos..." . PHP_EOL;

		foreach ($items as $item)
		{
			$mensajeCodigos = "";
			foreach ($item["codigosRef"] as $item2)
			{
				$mensajeCodigos.= "<br>Codigo de Referencia de la tarificación: " . $item2;
			}

			$this->email->set_newline("\r\n");
			$this->email->to($item["email"]);
			$this->email->from(EMAIL_FROM);
			$this->email->subject("Recordatorios pendientes para hoy");
			$this->email->message($item["nombre"] . " " . $item["apellidos"] . " :" .  " Tienes " . $item["cantidad"] . " recordatorios listos para revisar hoy en " . URL_TARIFICADOR . $mensajeCodigos);
			echo "Enviando a:" . $item["email"] . PHP_EOL;
			$this->email->send();
			echo "Enviado a:" . $item["email"] . " correctamente" . PHP_EOL;
		}
	}
}
