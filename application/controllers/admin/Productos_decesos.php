<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_decesos extends CI_Controller {

	private $codigoRamo = "DCS";

    public function __construct() {
		parent::__construct();
		//$this->load->model('admin/salud_model');
		$this->load->model('admin/zonas_model');
		$this->load->model('admin/decesos_model');
		$this->load->model('admin/funciones_comunes_model');

		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	

    public function crear_producto_paso1() {

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectFormaPago', 'Forma de pago del producto', 'required');
		$this->form_validation->set_rules('selectOrigentasas', 'Origen de las tasas del producto', 'required');
		$this->form_validation->set_rules('nombreProducto', 'Nombre del producto', 'required');
		$this->form_validation->set_rules('nombreLogo', 'Nombre del logo del producto', 'required');

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 1 - ' . TITLE_CREAR_PRODUCTO_DECESOS_PASO_1;

			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar la forma de pago ///////////////
			$formaPago = $this->funciones_comunes_model->getFormaPago();
			$data['formaPago'] = array();
			foreach ($formaPago->result_array() as $row)
			{
				$datos_formaPago = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['formaPago'],$datos_formaPago);
			}
			/////////////////////////////////////////////////////////			

			// Consulta para sacar el origen de las tasas ///////////////
			$origenTasas = $this->funciones_comunes_model->getOrigenTasas();
			$data['origenTasas'] = array();
			foreach ($origenTasas->result_array() as $row)
			{
				$datos_origenTasas = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['origenTasas'],$datos_origenTasas);
			}
			/////////////////////////////////////////////////////////											
			$data['url_omitir'] = "admin/productos_decesos/crear_producto_paso2";

	        $this->load->view('templates/header');
	        $this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso1',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idCia = $this->input->post('selectCias');
			$idFormaPago = $this->input->post('selectFormaPago');
			$idOrigenTasas = $this->input->post('selectOrigentasas');
			$nombreProducto = $this->input->post('nombreProducto');
			$nombreLogo = $this->input->post('nombreLogo');

			$return_arr = $this->funciones_comunes_model->crearProducto($idCia,$this->codigoRamo,$idFormaPago,$idOrigenTasas,$nombreProducto,$nombreLogo);

			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Producto guardado correctamente en la base de datos con la id: " . $return_arr["id"];
				$data['porcentaje_completado'] = "15";
				$data['url_next'] = "admin/productos_decesos/crear_producto_paso2";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_decesos/crear_producto_paso1";
				$data['url_back_text'] = "Repetir el paso 1";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_decesos/crear_producto_paso1";
				$data['url_destino_text'] = "Reintentar el paso 1";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
		}
    }

	public function crear_producto_paso2() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Nombre del producto', 'required');

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 2 - ' . TITLE_CREAR_PRODUCTO_ASIGNAR_CORREDURIA;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}

			/////////////////////////////////////////////////////////
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';	

			$data['url_omitir'] = "admin/productos_decesos/crear_producto_paso3";
			$data['url_paso_anterior'] = "admin/productos_decesos/crear_producto_paso1";

	        $this->load->view('templates/header');
	        $this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso2',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idCorreduria = $this->input->post('selectCorredurias');
			$idCia = $this->input->post('selectCias');
			$idProducto = $this->input->post('selectProducto');



			$return_arr = $this->funciones_comunes_model->asignarProductoCorreduria($idCorreduria,$idCia,$this->codigoRamo,$idProducto,NULL,NULL,NULL);	


			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "El producto se ha asignado correctamente a la correduría con id: " . $idCorreduria;
				$data['porcentaje_completado'] = "30";
				$data['url_next'] = "admin/productos_decesos/crear_producto_paso3";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_decesos/crear_producto_paso2";
				$data['url_back_text'] = "Repetir el paso 2";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_decesos/crear_producto_paso2";
				$data['url_destino_text'] = "Reintentar el paso 2";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
		}
	}


	public function crear_producto_paso3() {

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto select', 'required');

		$config = $this->tarificador_tasas->configurar_file_upload();
		$this->load->library('upload', $config);		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 3 - ' . TITLE_CREAR_PRODUCTO_ADD_TASAS;
	    	$data['soloVacios'] = "0";
	    	$data['codigoRamo'] = $this->codigoRamo;	

			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
				
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');

			$data['url_omitir'] = "admin/productos_decesos/crear_producto_paso4";
			$data['url_paso_anterior'] = "admin/productos_decesos/crear_producto_paso2";			

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso3',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');



			if (!$this->upload->do_upload())
			{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar subir el archivo.";
				$data['error1'] = $this->upload->display_errors();
				$data['url_destino'] = "admin/productos_decesos/crear_producto_paso3";
				$data['url_destino_text'] = "Reintentar";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
			else
			{
				// Se ha recibido el archivo csv que debe contener las edades y sus correspondientes tasas.
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$file_name = $upload_data['file_name'];

				// Construimos la cadena para la insercion en la BBDD
				$return_arr = $this->tarificador_tasas->generar_tasas_decesos($idProducto,$file_name);

				if ($return_arr["success"]) {
					// Por ultimo se ejecuta la consulta generada
					$result = $this->funciones_comunes_model->insertarTasas($idProducto,$return_arr["queryString"],$return_arr["queryString_borrar_tasas_antiguas"]);
				}else{
					$result = FALSE;
				}

				if ($result["success"]) {
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Se han guardado las tasas en la base de datos correctamente.";
					$data['porcentaje_completado'] = "45";
					$data['url_next'] = "admin/productos_decesos/crear_producto_paso4";
					$data['url_next_text'] = "Continuar";
					$data['url_back'] = "admin/productos_decesos/crear_producto_paso3";
					$data['url_back_text'] = "Repetir el paso 3";
					$data['datos1_arr'] = $result["queryString"];

					$this->load->view('templates/header');	
					$this->load->view('templates/header_tarificador');
					$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
					$this->load->view('templates/footer');
				}
				else{
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Error al intentar guardar los datos.";
					$data['error1'] = $result["queryString"];
					$data['url_destino'] = "admin/productos_decesos/crear_producto_paso3";
					$data['url_destino_text'] = "Reintentar";

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');	
					$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
					$this->load->view('templates/footer');
				}
			}
		}			
	}


	public function crear_producto_paso4() { /******************** nuevo para capitales *********************************/

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto select', 'required');

		$config = $this->tarificador_tasas->configurar_file_upload();
		$this->load->library('upload', $config);		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 4 - ' . TITLE_CREAR_PRODUCTO_ADD_CAPITALES_PROVINCIA;
	    	$data['soloVacios'] = "0";
	    	$data['codigoRamo'] = $this->codigoRamo;	

			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
				
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');

			$data['url_omitir'] = "admin/productos_decesos/crear_producto_paso5";
			$data['url_paso_anterior'] = "admin/productos_decesos/crear_producto_paso3";			

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso4',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');



			if (!$this->upload->do_upload())
			{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar subir el archivo.";
				$data['error1'] = $this->upload->display_errors();
				$data['url_destino'] = "admin/productos_decesos/crear_producto_paso4";
				$data['url_destino_text'] = "Reintentar";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
			else
			{
				// Se ha recibido el archivo csv que debe contener las edades y sus correspondientes tasas.
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$file_name = $upload_data['file_name'];

				// Construimos la cadena para la insercion en la BBDD
				$return_arr = $this->tarificador_tasas->generar_capitales_decesos($idProducto,$file_name);

				if ($return_arr["success"]) {
					// Por ultimo se ejecuta la consulta generada
					$result = $this->decesos_model->insertarCapitales($idProducto,$return_arr["queryString"],$return_arr["query_borrar_capitales_antiguos"]);
				}else{
					$result = FALSE;
				}

				if ($result["success"]) {
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Se han guardado las tasas en la base de datos correctamente.";
					$data['porcentaje_completado'] = "60";
					$data['url_next'] = "admin/productos_decesos/crear_producto_paso5";
					$data['url_next_text'] = "Continuar";
					$data['url_back'] = "admin/productos_decesos/crear_producto_paso4";
					$data['url_back_text'] = "Repetir el paso 4";
					$data['datos1_arr'] = $result["queryString"];

					$this->load->view('templates/header');	
					$this->load->view('templates/header_tarificador');
					$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
					$this->load->view('templates/footer');
				}
				else{
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Error al intentar guardar los datos.";
					$data['error1'] = $result["queryString"];
					$data['url_destino'] = "admin/productos_decesos/crear_producto_paso4";
					$data['url_destino_text'] = "Reintentar";

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');	
					$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
					$this->load->view('templates/footer');
				}
			}
		}			
	}	

	public function crear_producto_paso5() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Nombre del producto', 'required');
		$this->form_validation->set_rules('selectOperacion', 'Nombre de la operación', 'required');
		$this->form_validation->set_rules('selectFormaOperacion', 'Nombre de la forma de operación', 'required');
		$this->form_validation->set_rules('selectFormaPago', 'Nombre de la forma de pago', 'required');
		$this->form_validation->set_rules('selectRangoPersonas', 'Nombre del rango de personas', 'required');
		$this->form_validation->set_rules('valor', 'valor numérico de la operación', 'required');


		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Paso 5 - ' . TITLE_CREAR_PRODUCTO_ADD_IMPUESTOS_RECARGOS;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			//////////////////////////////////////////////////////////////////////////////
			// Consulta para sacar los tipos de operaciones (descuentos o recargos) ///////////////
			$op = $this->funciones_comunes_model->getDescuentosRecargos();
			$data['operacion'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_operacion = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['operacion'],$datos_operacion);
			}
			/////////////////////////////////////////////////////////		
			// Consulta para sacar las formas de pago ///////////////
			$fp = $this->funciones_comunes_model->getFormaPago();
			$data['formaPago'] = array();
			foreach ($fp->result_array() as $row)
			{
				$datos_formaPago = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['formaPago'],$datos_formaPago);
			}
			/////////////////////////////////////////////////////////	
			// Consulta para sacar la forma en la que se aplicará la operación (porcentaje o cantidad fija) ///////////////
			$op = $this->funciones_comunes_model->getPorcentajeCantidad();
			$data['forma_operacion'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_forma_operacion = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['forma_operacion'],$datos_forma_operacion);
			}
			/////////////////////////////////////////////////////////		
			// Consulta para sacar los rangos de numero de personas ///////////////
			$op = $this->funciones_comunes_model->getRangoNumeroPersonas();
			$data['rangos_personas'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_rangos = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['rangos_personas'],$datos_rangos);
			}
			/////////////////////////////////////////////////////////										
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto_decesos_correduria.js');			
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';				


			$data['url_omitir'] = "admin/productos_decesos/crear_producto_paso6";
			$data['url_paso_anterior'] = "admin/productos_decesos/crear_producto_paso4";

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso5',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');
			$idOperacion = $this->input->post('selectOperacion');
			$idFormaOperacion= $this->input->post('selectFormaOperacion');
			$idFormaPago = $this->input->post('selectFormaPago');
			$idRangoPersonas = $this->input->post('selectRangoPersonas');
			$edadMinima = $this->input->post('selectEdadMinima');
			$edadMaxima = $this->input->post('selectEdadMaxima');
			$valor = $this->input->post('valor');

			$return_arr = $this->decesos_model->crearImpuestoRecargo($idProducto,$idOperacion,$idFormaOperacion,$idFormaPago,$idRangoPersonas,$edadMinima,$edadMaxima,$valor);				

			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Se ha añadido correctamente la operación con id: " . $return_arr["id"];
				$data['porcentaje_completado'] = "85";
				$data['url_next'] = "admin/productos_decesos/crear_producto_paso6";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_decesos/crear_producto_paso5";
				$data['url_back_text'] = "Repetir el paso 5";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_decesos/crear_producto_paso5";
				$data['url_destino_text'] = "Reintentar el paso 5";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}

		}		
	}

	public function crear_producto_paso6() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Nombre del producto', 'required');

		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Paso 6 - ' . TITLE_CREAR_PRODUCTO_ACTIVACION_PRODUCTO;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto_decesos_correduria.js');			
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';				


			$data['url_omitir'] = "admin/admin_general/en_construccion";
			$data['url_paso_anterior'] = "admin/productos_decesos/crear_producto_paso5";

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/decesos/crear_producto_paso6',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');
			$accion = $this->input->post('checkActivar');


			// Si se ha puesto el checkbox en "on", se activa el producto. Sino, no se hace nada.
			if ($accion == "on"){
				$return_arr = $this->decesos_model->activacionProducto($idProducto,'1');
				$data['mensaje'] = "Se ha activado correctamente el producto con la id: " . $idProducto;
				$data['datos1_arr'] = $return_arr["queryString"];
			}
			else{
				$data['mensaje'] = "No se ha activado el producto con la id: " . $idProducto;
			}

			$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
			
			$data['porcentaje_completado'] = "100";
			$data['url_next'] = "tarificador";
			$data['url_next_text'] = "Finalizar";
			$data['url_back'] = "admin/productos_decesos/crear_producto_paso6";
			$data['url_back_text'] = "Repetir el paso 6";			

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
			$this->load->view('templates/footer');



		}		
	}	

	public function ver_detalles_producto_de_correduria(){

	}	
	public function ver_detalles_producto(){

    

	}


    public function modificar_producto() {

    }



	public function borrar_producto() {

    }






    public function modificar_tasas(){

    }



	public function borrar_tasas(){

    }    




    public function ajax_getProductoCorreduria($estado_activo){
		$idCorreduria = $this->input->post('idCorreduria');
		$idCia = $this->input->post('idCia');

		$resul = $this->decesos_model->getProductosCorreduria($idCorreduria,$idCia,$estado_activo);

		$data['productosCia'] = array();
		foreach ($resul->result_array() as $row)
		{
			$datos_producto = array(
				'id' => $row['id'],
				'nombre_producto' => $row['nombre_producto'],
			);
			array_push($data['productosCia'],$datos_producto);
		}	

		echo json_encode($data['productosCia']);
    }
}
