<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_general extends CI_Controller {

    public function __construct() {
		parent::__construct();


		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	
}