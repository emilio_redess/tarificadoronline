<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_alquiler extends CI_Controller {

	private $codigoRamo = "ALQ";

    public function __construct() {
		parent::__construct();
		$this->load->model('admin/alquiler_model');
		$this->load->model('admin/funciones_comunes_model');
		$this->load->model('tarificaciones_model');

		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	

    

	public function ver_detalles_producto(){

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto', 'required');		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = TITLE_DETALLES_PRODUCTO;
	    	$data['soloVacios'] = "0";


			// Consulta para sacar todas las compañias en las que la correduria tenga algun producto para este ramo ///////////////
	    	

	    	if ($this->ion_auth->is_admin())
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosAlquilerAdmin($this->codigoRamo);
	    	else{
	    		$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosAlquiler($correduriaId,$this->codigoRamo);
	    	}

	    	$data['companyias'] = array();
	    	foreach ($cias as $row)
			{
				$datos_companyia = array(
					'id' => $row->id,
					'nombre' => $row->nombre,
				);
				array_push($data['companyias'],$datos_companyia);
			}

			/////////////////////////////////////////////////////////

			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');
			$data['codigoRamo'] = $this->codigoRamo;

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/alquiler/detalles_producto_formulario',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');

			$datosProducto = $this->funciones_comunes_model->getDatosProducto($idProducto);

			//Ahora hay que sacar los datos especificos del producto que se aplican a cada correduria,
			// en este caso de alquiler, seria el factor de calculo.
			if ($this->ion_auth->is_admin()) {
				$datosEspecificos = $this->funciones_comunes_model->getDatosEspecificosProductoAlquilerAdmin($datosProducto->prod_id);

				$data['factorCalculo'] = array();
				foreach ($datosEspecificos as $row)
				{
					if ($row->activo === '1')
						$activo = "Sí";
					else
						$activo = "No";

					$datos_fc = array(
						'nombre' => $row->nombre,
						'valor' => $row->valor,
						'activo' => $activo
					);
					array_push($data['factorCalculo'],$datos_fc);				        
				      
				}


			}else{
				$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
				$datosEspecificos = $this->funciones_comunes_model->getDatosEspecificosProductoAlquiler($correduriaId, $datosProducto->prod_id);

				if (!is_null($datosEspecificos)){
					$data['factorCalculo'] = $datosEspecificos->factorCalculo;

					if ($datosEspecificos->activo === '1')
						$activo = "Sí";
					else
						$activo = "No";				
					$data['activo'] = $activo;
				}else{

					$data['factorCalculo'] = 'Valor no definido';
					$data['activo'] = '-';
				}
			}

			//////////////////////////////////////////////////////////////////////////////////////////////			


			$data['title'] = TITLE_DETALLES_PRODUCTO;
			//$data['mensaje'] = "Estas viendo el producto con id: " . $idProducto;
			$data['url_next_text'] = "Ver otro producto";
			$data['url_back'] = "admin/productos_alquiler/ver_detalles_producto";

			$data['datosProducto'] = $datosProducto;

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/alquiler/detalles_producto_datos',$data);
			$this->load->view('templates/footer');			
		}

	}


    public function modificar_factor_calculo() {
    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto', 'required');		
		$this->form_validation->set_rules('fc_nuevo', 'Nuevo valor del factor de cálculo', 'required');		
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = TITLE_DETALLES_PRODUCTO;
	    	$data['soloVacios'] = "0";


			// Obtenemos el listado de corredurias que tienen al menos algun producto de alquiler para el 
			// caso de que sea un admin el que está accediendo, poder seleccionar a que 
			// correduria asignar dicha tarificacion
			$resul = $this->tarificaciones_model->getCorreduriasAlquiler();
			$data['correduriasArray'] = $resul;




			// Consulta para sacar todas las compañias en las que la correduria tenga algun producto para este ramo ///////////////
	    	if ($this->ion_auth->is_admin())
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosAlquilerAdmin($this->codigoRamo);
	    	else{
	    		$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosAlquiler($correduriaId,$this->codigoRamo);
	    	}

	    	$data['companyias'] = array();
	    	foreach ($cias as $row)
			{
				$datos_companyia = array(
					'id' => $row->id,
					'nombre' => $row->nombre,
				);
				array_push($data['companyias'],$datos_companyia);
			}

			/////////////////////////////////////////////////////////

			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js','public/js/gestion_add_factor_calculo_alquiler_correduria.js');
			$data['codigoRamo'] = $this->codigoRamo;

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/alquiler/modificar_factor_calculo_formulario',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProd = $this->input->post('idProd');
			$fc = $this->input->post('fc_nuevo');
			$fc = $this->funciones_utiles->formatear_decimales($fc);

			if (!is_null($fc)){  // tenemos el nuevo valor del factor de calculo, vamos a guardarlo en la BBDD



				$resul = $this->alquiler_model->actualizar_factor_calculo($fc,$idProd);


				// mostrar vista ok
				$data['mensaje'] = "Los datos han sido actualizados correctamente.";

		

			}else{
				$data['mensaje'] = "Ha habido un problema al intentar guardar los datos. Asegúrate de introducir un numero y de no usar separador de miles.";
			}

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/alquiler/resultado',$data);
			$this->load->view('templates/footer');					


		}    	

    }



	public function borrar_producto() {

    }




    public function ajax_getFactorCalculoCorreduria(){
		$idCia = $this->input->post('idCia');
		$idProducto = $this->input->post('idProducto');

		if ($this->ion_auth->is_admin()) {
			$correduriaId = $this->input->post('idCorreduria');
		}else{

			$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
		}

		//$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
		$datosEspecificos = $this->funciones_comunes_model->getDatosEspecificosProductoAlquiler($correduriaId, $idProducto);

		if (!is_null($datosEspecificos)){
			$fc = $datosEspecificos->factorCalculo;
			$idProd = $datosEspecificos->idProd;
	
		}else{

			$fc = 'Valor no definido';
			$idProd = $datosEspecificos->idProd;
		}	

		$resul = array();
		$resul["fc"] = $fc;
		$resul["idProd"] = $idProd;


		echo json_encode($resul);
    }
}
