<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Zonas extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('admin/zonas_model');
		$this->load->model('admin/funciones_comunes_model');

		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	


/**************   CRUD para la gestion de zonas de provincias ***************************/
    public function crear_zona(){

    	// Form validation
		$this->form_validation->set_rules('nombreZona', 'Nombre de zona', 'required');

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = "Crear zona";

	    	// Consulta para obtener todas los nombres de las provincias
			$resul = $this->funciones_comunes_model->getProvincias();

			$data['provincias'] = array();
			foreach ($resul->result_array() as $row)
			{
				$datos_provincia = array(
					'id' => $row['id'],
					'provincia' => $row['provincia'],
				);
				array_push($data['provincias'],$datos_provincia);
			}

	        $this->load->view('templates/header');
	        $this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/crear_zona',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$nombreZona = $this->input->post('nombreZona');
			$this->zonas_model->crearZona($nombreZona);

			$data['nombreZona'] = $nombreZona;

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/crear_zona_success',$data);
			$this->load->view('templates/footer');
		}
	}

	public function modificar_zona(){

		// Form validation
		$this->form_validation->set_rules('selectZonas', 'Zonas select', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$data['title'] = "Modificar zona";


			/***************************  obtenemos las zonas *********************************************/
			$resul = $this->zonas_model->getZonas();

			$data['zonas'] = array();
			foreach ($resul->result_array() as $row)
			{
				$datos_zona = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['zonas'],$datos_zona);
			}
			/**********************************************************************************************/



			/***************************  obtenemos las provincias ****************************************/
			$resul = $this->funciones_comunes_model->getProvincias();

			$data['provincias'] = array();
			foreach ($resul->result_array() as $row)
			{
				$datos_provincia = array(
					'id' => $row['id'],
					'provincia' => $row['provincia'],
				);
				array_push($data['provincias'],$datos_provincia);
			}
			/**********************************************************************************************/

			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_zonas.js');
			// Cargamos la vista que contiene el formulario
			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/modificar_zona',$data);
			$this->load->view('templates/footer');
		}
		else // El formulario ha sido enviado correctamente. Recogemos datos y hacemos algo con eso.
		{
			//Datos de la zona seleccionada
			$zonaSeleccionadaId = $this->input->post('selectZonas');
			$data['zonaSeleccionadaId'] = $zonaSeleccionadaId;


			$nombreZona = $this->input->post('nombreZona');
			$this->zonas_model->setNombreZona($zonaSeleccionadaId,$nombreZona);
			$data['nombreZona'] = $nombreZona;


			//Datos de los checkboxes con las provincias para la zona seleccionada
			$arrayProvinciasZona = array();
			for ($i = 1; $i <= 52; $i++) {
			    $aux = $this->input->post("provincia_" . $i);
			    if ($aux == NULL)			
			    	$arrayProvinciasZona[$i]='0';			    
			    else			    
			    	$arrayProvinciasZona[$i]='1';			    
			}

			//Actualizamos los datos de la zona seleccionada en la BBDD
			$this->zonas_model->modificarZona($zonaSeleccionadaId,$arrayProvinciasZona);

			//Monstramos vista de resultados
			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/modificar_zona_success',$data);
			$this->load->view('templates/footer');			
		}
	}


    public function borrar_zona(){

		// Form validation
		$this->form_validation->set_rules('selectZonas', 'Zonas select', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			// Consulta para sacar todas las zonas definidas en la BBDD
			$resul = $this->zonas_model->getZonas();

			$data['zonas'] = array();
			foreach ($resul->result_array() as $row)
			{
				$datos_zona = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['zonas'],$datos_zona);
			}

			$data['title'] = "Borrar zona";

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/borrar_zona',$data);
			$this->load->view('templates/footer');
		}
		else // El formulario ha sido enviado correctamente. Recogemos datos y hacemos algo con eso.
		{
			$seleccionado = $this->input->post('selectZonas');
			$nombreZona = $this->zonas_model->getNombreZona($seleccionado);


			$resul = $this->zonas_model->borrarZona($seleccionado);
			if ($resul == TRUE)
				$data['mensaje'] = $nombreZona . " ha sido borrada correctamente.";
			else
				$data['mensaje'] = "Ha habido algún error intentando borrar " . $nombreZona;

			

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/zonas/borrar_zona_success',$data);
			$this->load->view('templates/footer');
		}
	
	}

    public function ajax_datosZona(){
		$id = $this->input->post('id');

		$resul = $this->zonas_model->getDatosZona($id);

		$data['datosZona'] = array();
		foreach ($resul->result_array() as $row)
		{
			$datos_zona = array(
				'id' => $row['id'],
				'id_provincia' => $row['id_provincia'],
				'provincia' => $row['provincia'],
				'incluida' => $row['incluida'],
			);
			array_push($data['datosZona'],$datos_zona);
		}	

		echo json_encode($data['datosZona']);
    }	
/****************************************************************************************/

}
