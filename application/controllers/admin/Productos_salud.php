<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_salud extends CI_Controller {

	private $codigoRamo = "SAL";

    public function __construct() {
		parent::__construct();
		//$this->load->model('admin/salud_model');
		$this->load->model('admin/zonas_model');
		$this->load->model('admin/salud_model');
		$this->load->model('admin/funciones_comunes_model');

		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	

    public function crear_producto_paso1() {

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectFormaPago', 'Forma de pago del producto', 'required');
		$this->form_validation->set_rules('selectOrigentasas', 'Origen de las tasas del producto', 'required');
		//$this->form_validation->set_rules('selectZonas', 'Zona select', 'required');
		$this->form_validation->set_rules('nombreProducto', 'Nombre del producto', 'required');
		$this->form_validation->set_rules('nombreLogo', 'Nombre del logo del producto', 'required');

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 1 - ' . TITLE_CREAR_PRODUCTO_SALUD_PASO_1;

			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar la forma de pago ///////////////
			$formaPago = $this->funciones_comunes_model->getFormaPago();
			$data['formaPago'] = array();
			foreach ($formaPago->result_array() as $row)
			{
				$datos_formaPago = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['formaPago'],$datos_formaPago);
			}
			/////////////////////////////////////////////////////////			

			// Consulta para sacar el origen de las tasas ///////////////
			$origenTasas = $this->funciones_comunes_model->getOrigenTasas();
			$data['origenTasas'] = array();
			foreach ($origenTasas->result_array() as $row)
			{
				$datos_origenTasas = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['origenTasas'],$datos_origenTasas);
			}
			/////////////////////////////////////////////////////////											
			$data['url_omitir'] = "admin/productos_salud/crear_producto_paso2";

	        $this->load->view('templates/header');
	        $this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/crear_producto_paso1',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idCia = $this->input->post('selectCias');
			$idFormaPago = $this->input->post('selectFormaPago');
			$idOrigenTasas = $this->input->post('selectOrigentasas');
			//$idZona = $this->input->post('selectZonas');
			$nombreProducto = $this->input->post('nombreProducto');
			$nombreLogo = $this->input->post('nombreLogo');

			$return_arr = $this->funciones_comunes_model->crearProducto($idCia,$this->codigoRamo,$idFormaPago,$idOrigenTasas,$nombreProducto,$nombreLogo);

			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Producto guardado correctamente en la base de datos con la id: " . $return_arr["id"];
				$data['porcentaje_completado'] = "20";
				$data['url_next'] = "admin/productos_salud/crear_producto_paso2";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_salud/crear_producto_paso1";
				$data['url_back_text'] = "Repetir el paso 1";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_salud/crear_producto_paso1";
				$data['url_destino_text'] = "Reintentar el paso 1";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
		}
    }

	public function crear_producto_paso2() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('nombreProducto', 'Nombre del producto', 'required');
		$this->form_validation->set_rules('selectProductoCM', 'Nombre del producto para Cuadro Médico', 'required');
		$this->form_validation->set_rules('selectProductoRE', 'Nombre del producto para Reembolso', 'required');

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 2 - ' . TITLE_CREAR_PRODUCTO_ASIGNAR_CORREDURIA;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}


			/***************************  obtenemos los tipos de contratante ****************************************/
			$resul = $this->funciones_comunes_model->getContratantes();

			$data['contratantes'] = array();
			foreach ($resul->result_array() as $row)
			{
				$datos_contratante = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['contratantes'],$datos_contratante);
			}
			/**********************************************************************************************/


			/////////////////////////////////////////////////////////
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';	

			$data['url_omitir'] = "admin/productos_salud/crear_producto_paso3";
			$data['url_paso_anterior'] = "admin/productos_salud/crear_producto_paso1";

	        $this->load->view('templates/header');
	        $this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/crear_producto_paso2',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idCorreduria = $this->input->post('selectCorredurias');
			$idCia = $this->input->post('selectCias');
			$nombreProducto = $this->input->post('nombreProducto');
			$idProductoCM = $this->input->post('selectProductoCM');
			$idProductoRE = $this->input->post('selectProductoRE');


			$return_arr = $this->funciones_comunes_model->asignarProductoCorreduria($idCorreduria,$idCia,$this->codigoRamo,NULL,$nombreProducto,$idProductoCM,$idProductoRE);	



			//Datos de los checkboxes con los tipos de contratantes que se van a asignar al producto
			//Ahora que el producto se ha asignado a la correduria, con la id devuelta por la query, vamos a asignarle a dicho producto los contratantes seleccionados de los checkboxes
			if ($return_arr["success"]) {
				$numeroContratantes = $this->funciones_comunes_model->getNumberContratantes();

				for ($i = 1; $i <= $numeroContratantes; $i++) {
				    $aux = $this->input->post("contratante_" . $i);
				    if (!is_null($aux))			
			    		$this->funciones_comunes_model->insertContratante($return_arr["id"],$i);
				}
			}


			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "El producto se ha asignado correctamente a la correduría con id: " . $idCorreduria;
				$data['porcentaje_completado'] = "40";
				$data['url_next'] = "admin/productos_salud/crear_producto_paso3";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_salud/crear_producto_paso2";
				$data['url_back_text'] = "Repetir el paso 2";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_salud/crear_producto_paso2";
				$data['url_destino_text'] = "Reintentar el paso 2";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
		}
	}


	public function crear_producto_paso3() {

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectZonas', 'Zona select', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto select', 'required');

		$config = $this->tarificador_tasas->configurar_file_upload();
		$this->load->library('upload', $config);		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = 'Paso 3 - ' . TITLE_CREAR_PRODUCTO_ADD_TASAS;
	    	$data['soloVacios'] = "0";
	    	$data['codigoRamo'] = $this->codigoRamo;	

			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////	
			// Obtenemos las zonas de provincias //////////////////////
			$zonas = $this->zonas_model->getZonas(TRUE);
			$data['zonas'] = array();
			foreach ($zonas->result_array() as $row)
			{
				$datos_zona = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['zonas'],$datos_zona);
			}	
			/////////////////////////////////////////////////////////				
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');

			$data['url_omitir'] = "admin/productos_salud/crear_producto_paso4";
			$data['url_paso_anterior'] = "admin/productos_salud/crear_producto_paso2";			

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/crear_producto_paso3',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');
			$idZona = $this->input->post('selectZonas');


			if (!$this->upload->do_upload())
			{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar subir el archivo.";
				$data['error1'] = $this->upload->display_errors();
				$data['url_destino'] = "admin/productos_salud/crear_producto_paso3";
				$data['url_destino_text'] = "Reintentar";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}
			else
			{
				// Se ha recibido el archivo csv que debe contener las edades y sus correspondientes tasas.
				$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
				$file_name = $upload_data['file_name'];

				// Construimos la cadena para la insercion en la BBDD
				$return_arr = $this->tarificador_tasas->generar_tasas_salud($idProducto,$file_name,$idZona);

				if ($return_arr["success"]) {
					// Por ultimo se ejecuta la consulta generada
					$result = $this->funciones_comunes_model->insertarTasas($idProducto,$return_arr["queryString"],$return_arr["queryString_borrar_tasas_antiguas"]);
				}else{
					$result = FALSE;
				}

				if ($result["success"]) {
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Se han guardado las tasas en la base de datos correctamente.";
					$data['porcentaje_completado'] = "60";
					$data['url_next'] = "admin/productos_salud/crear_producto_paso4";
					$data['url_next_text'] = "Continuar";
					$data['url_back'] = "admin/productos_salud/crear_producto_paso3";
					$data['url_back_text'] = "Repetir el paso 3";
					$data['datos1_arr'] = $result["queryString"];

					$this->load->view('templates/header');	
					$this->load->view('templates/header_tarificador');
					$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
					$this->load->view('templates/footer');
				}
				else{
					$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
					$data['mensaje'] = "Error al intentar guardar los datos.";
					$data['error1'] = $result["queryString"];
					$data['url_destino'] = "admin/productos_salud/crear_producto_paso3";
					$data['url_destino_text'] = "Reintentar";

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');	
					$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
					$this->load->view('templates/footer');
				}
			}
		}			
	}

	public function crear_producto_paso4() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Nombre del producto', 'required');
		$this->form_validation->set_rules('selectSubproducto', 'Subproducto', 'required');
		$this->form_validation->set_rules('selectOperacion', 'Nombre de la operación', 'required');
		$this->form_validation->set_rules('selectFormaOperacion', 'Nombre de la forma de operación', 'required');
		$this->form_validation->set_rules('selectFormaPago', 'Nombre de la forma de pago', 'required');
		$this->form_validation->set_rules('selectRangoPersonas', 'Nombre del rango de personas', 'required');
		$this->form_validation->set_rules('valor', 'valor numérico de la operación', 'required');


		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Paso 4 - ' . TITLE_CREAR_PRODUCTO_ADD_IMPUESTOS_RECARGOS;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			//////////////////////////////////////////////////////////////////////////////
			// Consulta para sacar los tipos de operaciones (descuentos o recargos) ///////////////
			$op = $this->funciones_comunes_model->getDescuentosRecargos();
			$data['operacion'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_operacion = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['operacion'],$datos_operacion);
			}
			/////////////////////////////////////////////////////////		
			// Consulta para sacar las formas de pago ///////////////
			$fp = $this->funciones_comunes_model->getFormaPago();
			$data['formaPago'] = array();
			foreach ($fp->result_array() as $row)
			{
				$datos_formaPago = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['formaPago'],$datos_formaPago);
			}
			/////////////////////////////////////////////////////////	
			// Consulta para sacar la forma en la que se aplicará la operación (porcentaje o cantidad fija) ///////////////
			$op = $this->funciones_comunes_model->getPorcentajeCantidad();
			$data['forma_operacion'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_forma_operacion = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['forma_operacion'],$datos_forma_operacion);
			}
			/////////////////////////////////////////////////////////		
			// Consulta para sacar los rangos de numero de personas ///////////////
			$op = $this->funciones_comunes_model->getRangoNumeroPersonas();
			$data['rangos_personas'] = array();
			foreach ($op->result_array() as $row)
			{
				$datos_rangos = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['rangos_personas'],$datos_rangos);
			}
			/////////////////////////////////////////////////////////										
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto_salud_correduria.js');			
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';				


			$data['url_omitir'] = "admin/productos_salud/crear_producto_paso5";
			$data['url_paso_anterior'] = "admin/productos_salud/crear_producto_paso3";

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/crear_producto_paso4',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');
			$idSubproducto = $this->input->post('selectSubproducto');
			$idOperacion = $this->input->post('selectOperacion');
			$idFormaOperacion= $this->input->post('selectFormaOperacion');
			$idFormaPago = $this->input->post('selectFormaPago');
			$idRangoPersonas = $this->input->post('selectRangoPersonas');
			$edadMinima = $this->input->post('selectEdadMinima');
			$edadMaxima = $this->input->post('selectEdadMaxima');
			$valor = $this->input->post('valor');

			$return_arr = $this->salud_model->crearImpuestoRecargo($idProducto,$idSubproducto,$idOperacion,$idFormaOperacion,$idFormaPago,$idRangoPersonas,$edadMinima,$edadMaxima,$valor);				

			if ($return_arr["success"]) {
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Se ha añadido correctamente la operación con id: " . $return_arr["id"];
				$data['porcentaje_completado'] = "80";
				$data['url_next'] = "admin/productos_salud/crear_producto_paso5";
				$data['url_next_text'] = "Continuar";
				$data['url_back'] = "admin/productos_salud/crear_producto_paso4";
				$data['url_back_text'] = "Repetir el paso 4";
				$data['datos1_arr'] = $return_arr["queryString"];

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
				$this->load->view('templates/footer');
			}
			else{
				$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
				$data['mensaje'] = "Error al intentar guardar los datos.";
				$data['error1'] = $return_arr["queryString"];
				$data['url_destino'] = "admin/productos_salud/crear_producto_paso4";
				$data['url_destino_text'] = "Reintentar el paso 4";

				$this->load->view('templates/header');	
				$this->load->view('templates/header_tarificador');
				$this->load->view('admin/productos/crear_producto_datos_recibidos_error',$data);
				$this->load->view('templates/footer');
			}

		}		
	}

	public function crear_producto_paso5() {

    	// Form validation
		$this->form_validation->set_rules('selectCorredurias', 'Nombre de la correduría', 'required');
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Nombre del producto', 'required');

		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Paso 5 - ' . TITLE_CREAR_PRODUCTO_ACTIVACION_PRODUCTO;

			// Consulta para sacar todas las corredurias ///////////////
			$corredurias = $this->funciones_comunes_model->getCorredurias();
			$data['corredurias'] = array();
			foreach ($corredurias->result_array() as $row)
			{
				$datos_correduria = array(
					'id' => $row['id'],
					'codigo' => $row['codigo'],
					'nombre' => $row['nombre'],
				);
				array_push($data['corredurias'],$datos_correduria);
			}
			/////////////////////////////////////////////////////////
			// Consulta para sacar todas las compañias ///////////////
			$cias = $this->funciones_comunes_model->getCompanyias();
			$data['companyias'] = array();
			foreach ($cias->result_array() as $row)
			{
				$datos_companyia = array(
					'id' => $row['id'],
					'nombre' => $row['nombre'],
				);
				array_push($data['companyias'],$datos_companyia);
			}
			/////////////////////////////////////////////////////////
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto_salud_correduria.js');			
			$data['codigoRamo'] = $this->codigoRamo;	
			$data['soloVacios'] = '0';				


			$data['url_omitir'] = "admin/admin_general/en_construccion";
			$data['url_paso_anterior'] = "admin/productos_salud/crear_producto_paso4";

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/crear_producto_paso5',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');
			$accion = $this->input->post('checkActivar');


			// Si se ha puesto el checkbox en "on", se activa el producto. Sino, no se hace nada.
			if ($accion == "on"){
				$return_arr = $this->salud_model->activacionProducto($idProducto,'1');
				$data['mensaje'] = "Se ha activado correctamente el producto con la id: " . $idProducto;
				$data['datos1_arr'] = $return_arr["queryString"];
			}
			else{
				$data['mensaje'] = "No se ha activado el producto con la id: " . $idProducto;
			}

			$data['title'] = TITLE_CREAR_PRODUCTO_RESUMEN;
			
			$data['porcentaje_completado'] = "100";
			$data['url_next'] = "tarificador";
			$data['url_next_text'] = "Finalizar";
			$data['url_back'] = "admin/productos_salud/crear_producto_paso5";
			$data['url_back_text'] = "Repetir el paso 5";			

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/productos/crear_producto_datos_recibidos_ok',$data);
			$this->load->view('templates/footer');



		}		
	}	

	public function ver_detalles_producto_de_correduria(){

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto select', 'required');	
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorredurias', 'Correduria', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = TITLE_DETALLES_PRODUCTO;

			
	    	if ($this->ion_auth->is_admin()){

				// Consulta para sacar todas las corredurias ///////////////
				$corredurias = $this->funciones_comunes_model->getCorredurias();
				$data['corredurias'] = array();
				foreach ($corredurias->result_array() as $row)
				{
					$datos_correduria = array(
						'id' => $row['id'],
						'codigo' => $row['codigo'],
						'nombre' => $row['nombre'],
					);
					array_push($data['corredurias'],$datos_correduria);
				}
				///////////////////////////////////////////////////////
				// Ahora se sacan las compañias
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosSaludAdmin($this->codigoRamo);
	    	}else{
	    		$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
	    		$data['correduria_id'] = $correduriaId;
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosSalud($correduriaId,$this->codigoRamo);
	    	}

	    	$data['companyias'] = array();
	    	foreach ($cias as $row)
			{
				$datos_companyia = array(
					'id' => $row->id,
					'nombre' => $row->nombre,
				);
				array_push($data['companyias'],$datos_companyia);
			}

			/////////////////////////////////////////////////////////			
			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto_salud_correduria.js');

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/detalles_salud_producto_correduria_formulario',$data);
			$this->load->view('templates/footer');


		}		
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');


			// Primero se obtienen los datos del producto asociado a la correduria
			$datosProducto = $this->salud_model->getDatosProducto($idProducto);
			$idCM = $datosProducto->id_producto_CM;
			$idRE = $datosProducto->id_producto_RE;



			// Ahora se sacan los datos de las zonas de cada subproducto: tanto de CM como de RE
			$arr = array();

			// CM
			if (!is_null($idCM)){
				$zonasProducto = $this->zonas_model->getZonasProducto($idCM);
				foreach ($zonasProducto->result_array() as $row)
				{
					if (!array_key_exists($row['nombre'],$arr)){
						$arr[$row['nombre']] = array();
						$provinciasZona = $this->zonas_model->getDatosZona($row['id']);

						foreach ($provinciasZona->result_array() as $row2)
						{

							if ($row2['incluida'] == '1'){
								
									array_push($arr[$row['nombre']],$row2['provincia']);
							}
						}
					}	

				}
			}



			// RE
			if (!is_null($idRE)){
				$zonasProducto = $this->zonas_model->getZonasProducto($idRE);
				
				foreach ($zonasProducto->result_array() as $row)
				{
					if (!array_key_exists($row['nombre'],$arr)){
						$arr[$row['nombre']] = array();
						$provinciasZona = $this->zonas_model->getDatosZona($row['id']);

						foreach ($provinciasZona->result_array() as $row2)
						{

							if ($row2['incluida'] == '1'){
								
									array_push($arr[$row['nombre']],$row2['provincia']);
							}
						}
					}	

				}
			}


			$data['provinciasZona'] = $arr;	


			// Ahora se sacan las tasas de ambos subproductos		

			// CM
			if (is_null($idCM)){
				$data['tasasCM'] = NULL;
			}else{
				$tasasProducto = $this->funciones_comunes_model->getTasasProducto($idCM,$this->codigoRamo);
				$data['tasasCM'] = array();
				foreach ($tasasProducto->result_array() as $row)
				{
					$datos_tasa = array(
						'edad' => $row['edad'],
						'tasa' => $row['tasa'],
						'nombreZona' => $row['nombre'],
					);
					array_push($data['tasasCM'],$datos_tasa);
				}
			}


			// RE
			if (is_null($idRE)){
				$data['tasasRE'] = NULL;
			}else{
				$tasasProducto = $this->funciones_comunes_model->getTasasProducto($idRE,$this->codigoRamo);
				$data['tasasRE'] = array();
				foreach ($tasasProducto->result_array() as $row)
				{
					$datos_tasa = array(
						'edad' => $row['edad'],
						'tasa' => $row['tasa'],
						'nombreZona' => $row['nombre'],
					);
					array_push($data['tasasRE'],$datos_tasa);
				}
			}			


			// Por ultimo, cargamos la vista con los datos obtenidos.
			$data['title'] = TITLE_DETALLES_PRODUCTO;
			$data['url_next_text'] = "Ver otro producto";
			$data['url_back'] = "admin/productos_salud/ver_detalles_producto_de_correduria";

			$data['datosProducto'] = $datosProducto;
			$data['ramo'] = "Salud";

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/detalles_salud_producto_correduria_datos',$data);
			$this->load->view('templates/footer');	




		}



	}	
	public function ver_detalles_producto(){

    	// Form validation
		$this->form_validation->set_rules('selectCias', 'Nombre de la compañía de seguros', 'required');
		$this->form_validation->set_rules('selectProducto', 'Producto select', 'required');		

		if ($this->form_validation->run() == FALSE)
		{
	    	$data['title'] = TITLE_DETALLES_PRODUCTO;
	    	$data['soloVacios'] = "0";

			// Consulta para sacar todas las compañias en las que la correduria tenga algun producto para este ramo ///////////////
	    	if ($this->ion_auth->is_admin())
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosSaludAdmin($this->codigoRamo);
	    	else{
	    		$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
	    		$cias = $this->funciones_comunes_model->getCompanyiasConProductosSalud($correduriaId,$this->codigoRamo);
	    	}

	    	$data['companyias'] = array();
	    	foreach ($cias as $row)
			{
				$datos_companyia = array(
					'id' => $row->id,
					'nombre' => $row->nombre,
				);
				array_push($data['companyias'],$datos_companyia);
			}

			/////////////////////////////////////////////////////////

			// Cargamos un archivo JS con funciones usadas en esta vista
			$data['jsArray'] = array('public/js/gestion_add_producto.js');
			$data['codigoRamo'] = $this->codigoRamo;

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/detalles_producto_formulario',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{
			$idProducto = $this->input->post('selectProducto');

			$datosProducto = $this->funciones_comunes_model->getDatosProducto($idProducto);

			// Vamos a sacar un listado de todas las zonas que tiene asociadas el producto, y todas las provincia de cada una de esas zonas.
			$zonasProducto = $this->zonas_model->getZonasProducto($idProducto);
			
			$arr = array();
			foreach ($zonasProducto->result_array() as $row)
			{
				$arr[$row['nombre']] = array();
				$provinciasZona = $this->zonas_model->getDatosZona($row['id']);


				
				foreach ($provinciasZona->result_array() as $row2)
				{

					if ($row2['incluida'] == '1'){
						array_push($arr[$row['nombre']],$row2['provincia']);
					}
				}	

			}
			$data['provinciasZona'] = $arr;


			$tasasProducto = $this->funciones_comunes_model->getTasasProducto($idProducto,$this->codigoRamo);
			$data['tasas'] = array();
			foreach ($tasasProducto->result_array() as $row)
			{
				$datos_tasa = array(
					'edad' => $row['edad'],
					'tasa' => $row['tasa'],
					'nombreZona' => $row['nombre'],
				);
				array_push($data['tasas'],$datos_tasa);
			}
			//////////////////////////////////////////////////////////////////////////////////////////////			


			$data['title'] = TITLE_DETALLES_PRODUCTO;
			$data['url_next_text'] = "Ver otro producto";
			$data['url_back'] = "admin/productos_salud/ver_detalles_producto";

			$data['datosProducto'] = $datosProducto;

			$this->load->view('templates/header');	
			$this->load->view('templates/header_tarificador');
			$this->load->view('admin/salud/detalles_producto_datos',$data);
			$this->load->view('templates/footer');			
		}

	}


    public function modificar_producto() {

    }



	public function borrar_producto() {

    }






    public function modificar_tasas(){

    }



	public function borrar_tasas(){

    }    




    public function ajax_getProductoCorreduria($estado_activo){
		$idCorreduria = $this->input->post('idCorreduria');
		$idCia = $this->input->post('idCia');

		$resul = $this->salud_model->getProductosCorreduria($idCorreduria,$idCia,$estado_activo);

		$data['productosCia'] = array();
		foreach ($resul->result_array() as $row)
		{
			$datos_producto = array(
				'id' => $row['id'],
				'nombre_producto' => $row['nombre'],
			);
			array_push($data['productosCia'],$datos_producto);
		}	

		echo json_encode($data['productosCia']);
    }
}
