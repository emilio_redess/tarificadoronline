<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_general extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model('admin/salud_model');
		$this->load->model('admin/zonas_model');
		$this->load->model('admin/funciones_comunes_model');

		if ((!$this->ion_auth->is_admin()) && (!$this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))))
		{
			redirect('auth/login', 'refresh');
		}

		// Cambiamos el delimitador de los mensajes del formulario para que usen los estilos de bootstrap
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }	

    public function ajax_getProducto($soloVacios){
		$idCia = $this->input->post('idCia');
		$codigoRamo = $this->input->post('codigoRamo');

		$resul = $this->funciones_comunes_model->getProductosCia($idCia,$codigoRamo,$soloVacios);

		$data['productosCia'] = array();
		foreach ($resul->result_array() as $row)
		{
			$datos_producto = array(
				'id' => $row['id'],
				'nombre_producto' => $row['id'] . ' ' . $row['nombre_producto'],
			);
			array_push($data['productosCia'],$datos_producto);
		}	

		echo json_encode($data['productosCia']);
    }


}