<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tarificador extends CI_Controller {

    public function __construct() {
			parent::__construct();
			$this->load->model('admin/funciones_comunes_model');
			$this->load->model('tarificaciones_model');
			if (!$this->ion_auth->logged_in())
			{
				redirect('auth/login', 'refresh');
			}
    }



	public function index() {

		$mensajes = $this->funciones_comunes_model->getMensajes("ANU");

		$data['mensajes'] = $mensajes;



		$data['title'] = 'Página de inicio';

		$this->load->view('templates/header');
		$this->load->view('templates/header_tarificador');
		$this->load->view('inicio', $data);
		$this->load->view('templates/footer');
		
	}

	public function anuncio($idMensaje) {
		$mensaje = $this->funciones_comunes_model->getMensaje($idMensaje);
		$data['mensaje'] = $mensaje;

		$this->load->view('templates/header');
		$this->load->view('templates/header_tarificador');
		$this->load->view('anuncio', $data);
		$this->load->view('templates/footer');		
	}

	public function construccion($submenu = FALSE) {

		$this->load->view('templates/header');
		if ($submenu)
			$this->load->view('templates/header_tarificador');
		$this->load->view('en_construccion');
		$this->load->view('templates/footer');

	}

	public function ramo_disabled() {
		$this->load->view('templates/header');
		$this->load->view('templates/header_tarificador');
		$this->load->view('ramo_disabled');
		$this->load->view('templates/footer');

	}



	// A esta funcion se llega desde el tarificador interno, no desde el cliente remoto mediante una llamada curl
	public function nueva_tarificacion_salud() {

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('provincia', 'provincia', 'required');
		$this->form_validation->set_rules('numeroPersonas', 'numero de personas', 'required');
		$this->form_validation->set_rules('date-1', 'Edad del asegurado', 'required');
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{

			// Obtenemos el listado de corredurias que tienen al menos algun producto de salud para el 
			// caso de que sea un admin el que está creando una tarificacion, poder seleccionar a que 
			// correduria asignar dicha tarificacion
			$resul = $this->tarificaciones_model->getCorreduriasSalud();
			$data['correduriasArray'] = $resul;


			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('usuarios/salud/nueva_tarificacion',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{	
			// Si el usuario conectado es admin, obtenemos la correduria del select del formulario, sino, 
			// la correduria será la que tenga asignada el  usuario logeado.
			if ($this->ion_auth->is_admin()){
				$cod_correduria = $this->input->post('selectCorreduria');
			}else{
				$cod_correduria = $this->ion_auth->get_user_correduria_id()->codigo;
			}

			// Id del usuario que está creando la tarificacion.
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
			
			// datos del formulario
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$prefijoProvincia = $this->input->post('provincia');
			$numAseg = $this->input->post('numeroPersonas');
			$autonomo = $this->input->post('autonomo');		

			/****************************** Llamada CURL al WS   *******************************/
			$params = array(
			   "user_id" => $user_id,
			   "correduria" => $cod_correduria,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "provinciaPrefijo" => $prefijoProvincia,
			   "saludContratante" => $autonomo,
			   "numPersonas" => $numAseg
			);	

			for ($i = 1; $i <= $numAseg; $i++) {

				$params['edad' . $i] = $this->input->post('date-'. $i);
			}



			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_salud/salud",$params);	

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);

				if ($arrayRespuesta['success'] == TRUE) { 

					// exito creando tarificacion -> ir a vista de tarificaciones
					$data['title'] = 'Listado de tarificaciones de Salud';
					$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');
					$data['ramo'] = $this->funciones_comunes_model->getRamo("Salud")->id;


					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('tarificador/index', $data);
					$this->load->view('templates/footer');

				}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:001';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';
					

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

			}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:002';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

		}
	}


	// A esta funcion se llega desde el tarificador interno, no desde el cliente remoto mediante una llamada curl
	public function nueva_tarificacion_alquiler() {

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('valorAlquiler', 'valor alquiler', 'required');
		$this->form_validation->set_rules('tipoLocal', 'tipo de local', 'required');
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{

			// Obtenemos el listado de corredurias que tienen al menos algun producto de alquiler para el 
			// caso de que sea un admin el que está creando una tarificacion, poder seleccionar a que 
			// correduria asignar dicha tarificacion
			$resul = $this->tarificaciones_model->getCorreduriasAlquiler();
			$data['correduriasArray'] = $resul;


			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('usuarios/alquiler/nueva_tarificacion',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{	
			// Si el usuario conectado es admin, obtenemos la correduria del select del formulario, sino, 
			// la correduria será la que tenga asignada el  usuario logeado.
			if ($this->ion_auth->is_admin()){
				$cod_correduria = $this->input->post('selectCorreduria');
			}else{
				$cod_correduria = $this->ion_auth->get_user_correduria_id()->codigo;
			}

			// Id del usuario que está creando la tarificacion.
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
			
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$valorAlquiler = $this->input->post('valorAlquiler');	

			$tipoLocal = $this->input->post('tipoLocal');	
			//$tipoLocal = "VIV";

			/****************************** Llamada CURL al WS   *******************************/
			$params = array(
			   "user_id" => $user_id,
			   "correduria" => $cod_correduria,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "valorAlquiler" => $valorAlquiler,
			   "tipoLocal" => $tipoLocal
			);	



			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_alquiler/alquiler",$params);	

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);

				if ($arrayRespuesta['success'] == TRUE) { 

					// exito creando tarificacion -> ir a vista de tarificaciones
					$data['title'] = 'Listado de tarificaciones de Protección de Alquiler';
					$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');
					$data['ramo'] = $this->funciones_comunes_model->getRamo("Alquiler")->id;


					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('tarificador/index', $data);
					$this->load->view('templates/footer');

				}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:001';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

			}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:002';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

		}
	}

	// A esta funcion se llega desde el tarificador interno, no desde el cliente remoto mediante una llamada curl
	public function nueva_tarificacion_vida() {

		// Form validation
		$this->form_validation->set_rules('nombreApellidos', 'nombre y apellidos', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required');
		$this->form_validation->set_rules('capital', 'capital a asegurar', 'required');
		$this->form_validation->set_rules('edad', 'edad del asegurado', 'required');
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{

			// Obtenemos el listado de corredurias que tienen al menos algun producto de alquiler para el 
			// caso de que sea un admin el que está creando una tarificacion, poder seleccionar a que 
			// correduria asignar dicha tarificacion
			$resul = $this->tarificaciones_model->getCorreduriasVida();
			$data['correduriasArray'] = $resul;


			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('usuarios/vida/nueva_tarificacion',$data);
			$this->load->view('templates/footer');
		}
		else // Formulario validado correctamente.
		{	
			// Si el usuario conectado es admin, obtenemos la correduria del select del formulario, sino, 
			// la correduria será la que tenga asignada el  usuario logeado.
			if ($this->ion_auth->is_admin()){
				$cod_correduria = $this->input->post('selectCorreduria');
			}else{
				$cod_correduria = $this->ion_auth->get_user_correduria_id()->codigo;
			}

			// Id del usuario que está creando la tarificacion.
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;
			
			$nombreApellidos = $this->input->post('nombreApellidos');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$capital = $this->input->post('capital');	
			$edad = $this->input->post('edad');	
			/****************************** Llamada CURL al WS   *******************************/
			$params = array(
			   "user_id" => $user_id,
			   "correduria" => $cod_correduria,
			   "nombreApellidos" => $nombreApellidos,
			   "email" => $email,
			   "telefono" => $telefono,
			   "capital" => $capital,
			   "edad" => $edad
			);	



			$output_arr = $this->curl_functions->httpPost(URL_CURL . "tarificador_seguro_vida/vida",$params);

			

			if ($output_arr['success'] === TRUE){
				// Decodificamos el JSON de respuesta
				$arrayRespuesta = json_decode($output_arr['output'],true);

				if ($arrayRespuesta['success'] == TRUE) { 

					// exito creando tarificacion -> ir a vista de tarificaciones
					$data['title'] = 'Listado de tarificaciones de Vida';
					$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');
					$data['ramo'] = $this->funciones_comunes_model->getRamo("Vida")->id;


					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('tarificador/index', $data);
					$this->load->view('templates/footer');

				}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:001';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';
					$data['resp'] = $arrayRespuesta;

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

			}else{
					// error creando tarificacion
					$data['heading'] = 'codigo-error:002';
					$data['message'] = 'Ha habido un error al intentar generar la tarificación. Por favor, vuelve a intentarlo.';

					$this->load->view('templates/header');
					$this->load->view('templates/header_tarificador');
					$this->load->view('errors/cli/error_general', $data);
					$this->load->view('templates/footer');					
				}

		}
	}	

	public function ver_detalle_tarificacion($tarificacionId){


		// Obtenemos la id_ramo de la tarificacion
		$tarificacion = $this->tarificaciones_model->getDatosTarificacion($tarificacionId);
		$ramo_id_tarificacion = $tarificacion->row()->ramo_id;



			// rutas de las vistas y funciones curl
			switch ($ramo_id_tarificacion) {
			    case "1":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Vida")->codigo;
			        $vista = "usuarios/vida/ver_detalle";
			        $ruta_curl = "tarificador_seguro_vida/vida/id/";
			        break;
			    case "2":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Salud")->codigo;
			        $vista = "usuarios/salud/ver_detalle";
			        $ruta_curl = "tarificador_seguro_salud/salud/id/";
			        break;
			    case "3":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Decesos")->codigo;
			        $vista = "usuarios/decesos/ver_detalle";
			        $ruta_curl = "tarificador_seguro_decesos/decesos/id/";
			        break;
			    case "4":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Hogar")->codigo;
			        $vista = "usuarios/hogar/ver_detalle";
			        $ruta_curl = "tarificador_seguro_salud/salud/id/";
			        break;
			    case "5":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Alquiler")->codigo;			  
			        $vista = "usuarios/alquiler/ver_detalle";
			        $ruta_curl = "tarificador_seguro_alquiler/alquiler/id/";
			        break;
			    case "6":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Formulario")->codigo;			  
			        $vista = "usuarios/formulario/ver_detalle";
			        $ruta_curl = "tarificador_seguro_formularios/formulario/id/";			    
			    	break;	
			    case "7":
			        $sufijo_codtar = $this->funciones_comunes_model->getRamo("Especial")->codigo;			  
			        $vista = "usuarios/especial/ver_detalle";
			        $ruta_curl = "tarificador_seguro_especial/especial/id/";			    
			    	break;				    			        			        
			    default:
			        $sufijo_codtar = "PROD";
			        $vista = "usuarios/generico/ver_detalle";
			        break;
			}




		$output_arr = $this->curl_functions->httpGet(URL_CURL . $ruta_curl . $tarificacionId);



		if ($output_arr['success'] === TRUE){
			// Decodificamos el JSON de respuesta
			$arrayRespuesta = json_decode($output_arr['output'],true);

			$correduriaId = $arrayRespuesta['correduriaId'];
			$cod_correduria = $this->tarificaciones_model->get_correduria_codigo($correduriaId);


			// Damos formato a la fecha de creacion
			$dt = new DateTime($arrayRespuesta['fechaTarificacion']);
			$fechaFormateada = $dt->format('j/m/Y H:i:s');




			// Datos comunes de todos los ramos
			$data['title'] = "Detalles de la tarificación ";
			$data['fechaTarificacion'] = $fechaFormateada;				
			$data['origen'] = $arrayRespuesta['creadaPor'];
			$data['nombreApellidos'] = $arrayRespuesta['nombreApellidos'];
			$data['email'] = $arrayRespuesta['email'];
			$data['telefono'] = $arrayRespuesta['telefono'];
			$data['primas'] = $arrayRespuesta['primas'];
			$data['codtar'] = $cod_correduria . " - " . $arrayRespuesta['codtar'] . " - " . $sufijo_codtar;


			// Datos especificos de cada ramo
			switch ($ramo_id_tarificacion) {
			    case "1":
			      $data['capital'] = $arrayRespuesta['capital'];
			      $data['edad'] = $arrayRespuesta['edad'];
			        break;
			    case "2":
			    	$data['numeroPersonas'] = $arrayRespuesta['numeroPersonas'];
			    	$data['provincia'] = $arrayRespuesta['saludProvinciaNombre'];
			    	$data['contratanteTipo'] = $arrayRespuesta['saludContratanteTipo'];
			    	$data['edades_arr'] = $arrayRespuesta['edades_arr'];			   			     
			        break;
			    case "3":
			       
			        break;
			    case "4":
			      
			        break;
			    case "5":
			       
			        $data['valorAlquiler'] = $arrayRespuesta['valorAlquiler'];
			        $data['tipoLocal'] = $arrayRespuesta['Tipo de local'];
			      
			        break;	
			    case "7":  // ramo especial
			    	$data['ramo_subcategoria'] = $arrayRespuesta['ramo_subcategoria'];
			    	$data['params'] = $arrayRespuesta['params'];




			    	break;		        			        
			    default:

			       break;
			}			
			
			

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view($vista, $data);
			$this->load->view('templates/footer');


		}else{
			$data['message'] = $output_arr['output'];
			$data['heading'] = 'error_1';

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('errors/cli/error_general',$data);
			$this->load->view('templates/footer');
		}		
	}


	// Controller para ver el listado de anotaciones de tarificaciones pendientes de revisar para hoy
	public function anotaciones() {
		$user = $this->ion_auth->user()->row();
		$userId = $user->id;
		$listadoAnotaciones = $this->funciones_comunes_model->getAnotacionesDia($userId);

		$data['listadoAnotaciones'] = $listadoAnotaciones;

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('tarificador/listadoAnotaciones',$data);
			$this->load->view('templates/footer');		


	}


	public function listado($ramo = 99,$idTarif = NULL) {
		// 99 => todos los ramos
		// 100 => tarificacion filtrada: mostrar solo una tarificacion

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			
			// Si se ha pasado el parametro $idTarif, significa que queremos hacer una busqueda en la datatable para la tarificacion especificada.
			// Se envia dicho valor a la vista, y al definir la datatable en datatable.listadoTarificaciones.js, se ejecuta la busqueda con el parametro "search"
			$data['search_tarificacion'] = $idTarif;

			$data['ramo'] = $ramo;
			$data['title'] = constant("TITLE_LISTADO_TARIFICACIONES_" . $ramo);


			if ($ramo == '0'){
				$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapelera.js');
				$this->load->view('tarificador/papelera', $data);
			}else{
				$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');
			    $this->load->view('tarificador/index', $data);			  
			}

			$this->load->view('templates/footer');
	}


	public function dataTable($ramo) {

		switch ($ramo) {
			case '0':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_papelera_admin';
				}else{
			    	$model = 'datatables_papelera';
			    }			
			    break;
			case '1':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_vida_admin';
				}else{
			    	$model = 'datatables_vida';
			    }			
			    break;
			case '2':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_salud_admin';
				}else{
			    	$model = 'datatables_salud';
			    }			
			    break;
			case '3':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_decesos_admin';
				}else{
			    	$model = 'datatables_decesos';
			    }			
			    break;
			case '4':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_hogar_admin';
				}else{
			    	$model = 'datatables_hogar';
			    }			
			    break;
			case '5':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_alquiler_admin';
				}else{
			    	$model = 'datatables_alquiler';
			    }	
			    break;
			case '6':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_formularios_admin';
				}else{
			    	$model = 'datatables_formularios';
			    }			    		
			    break;		
			case '7':
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_especiales_admin';
				}else{
			    	$model = 'datatables_especiales';
			    }			    		
			    break;				    	    
			default:
				if ($this->ion_auth->is_admin()){
					$model = 'datatables_all_admin';
				}else{
			    	$model = 'datatables_all';
			    }
			    break;
		}			

		$this -> load -> library('Datatable', array('model' => $model, 'rowIdCol' => 't.id'));
		$jsonArray = $this -> datatable -> datatableJson(array(
                'fecha_creacion' => 'datetime',
                'fecha_borrado' => 'date'
            ));

		$aux = json_encode($jsonArray);
		//log_message('debug', $aux);

		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));
	}

	public function listado_colaborador($idColaborador,$ramo = 99) {

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			
			$data['ramo'] = $ramo;
			$data['title'] = constant("TITLE_LISTADO_TARIFICACIONES_" . $ramo);

			// Esta variable de sesion se utiliza para saber dentro del modelo la id del colaborador del que se deben mostrar las tarificaciones
			$_SESSION['colaborador_seleccionado'] = $idColaborador;

			if ($ramo == '0'){
				$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapeleraColaborador.js');
				$this->load->view('tarificador/papelera', $data);
			}else{
				$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesColaborador.js');
			    $this->load->view('tarificador/index', $data);			  
			}

			$this->load->view('templates/footer');
	}	

	public function dataTableColaborador($ramo) {

		$model = 'datatables_all_colaborador';

		$this -> load -> library('Datatable', array('model' => $model, 'rowIdCol' => 't.id'));
		$jsonArray = $this -> datatable -> datatableJson(array(
                'fecha_creacion' => 'datetime',
                'fecha_borrado' => 'date'
            ));

		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));

	}

	public function contactanos() {

    	// Form validation
		$this->form_validation->set_rules('nombre', 'nombre', 'required');		
		$this->form_validation->set_rules('email', 'email', 'required');		
		$this->form_validation->set_rules('telefono', 'telefono', 'required');		
		$this->form_validation->set_rules('indicaciones', 'indicaciones', 'required');		

		if ($this->form_validation->run() == FALSE)
		{
			$data['telefono1'] = TELEFONO_CONTACTO_REDESS1;
			$data['telefono2'] = TELEFONO_CONTACTO_REDESS2;
			$data['telefono3'] = TELEFONO_CONTACTO_REDESS3;

			$this->load->view('templates/header');
			$this->load->view('contactanos',$data);
			$this->load->view('templates/footer');	

		}else{
			$nombre = $this->input->post('nombre');
			$email = $this->input->post('email');
			$telefono = $this->input->post('telefono');
			$indicaciones = $this->input->post('indicaciones');



			// Datos ok, enviar correo
			$subject = "Formulario de contacto Tarificador Redess";
			$recipient = EMAIL_RECIPIENTE;
			$cc = EMAIL_CC;
			$bcc = EMAIL_BCC;
			$from = EMAIL_FROM;
			$fromName = EMAIL_FROMNAME;
			$view = "emails/contactanos.php";

			$datosCorreo = array("telefono" => $telefono, "nombre" => $nombre, "email" => $email, "indicaciones" => $indicaciones);

			$respuesta = $this->send_emails->smtp_mail($subject,$recipient,$cc,$bcc,$from,$fromName,$view,$datosCorreo);
			///////////////////////////////////////////////////////////////////////////////////////////
			     
			if (!$respuesta) {
			    $data['mensaje'] = "Ha habido un error al intentar enviar el email, por favor, vuelve a intentarlo.";

			}else {
				$data['mensaje'] = 'Gracias por contactar con nosotros. Hemos recibido tu mensaje.';
				
			}
			$this->load->view('templates/header');
			$this->load->view('emails/respuesta',$data);
			$this->load->view('templates/footer');				
		}		
	}


	function ajax_ListarComentarios() {
		$idTarif = $this->input->post('idTarif');

		// Obtengo todos los comentarios para la tarificacion seleccionada
		$listadoComentarios = $this->funciones_comunes_model->getComentariosTarificacion($idTarif);

		$current_user = $this->ion_auth->user()->row();

		$comentarios = array();
		$item = array();
		foreach ($listadoComentarios->result() as $row)
		{
			$item['id'] = $row->id;
			$item['text'] = $row->contenido;
			$item['fecha_limite'] = 0;  // Bool que nos indica si la fecha de recordatorio de la anotacion es hoy.


			if (is_null($row->fecha_recordatorio))
				$item['reminderDateFormatted'] = "-";
			else{

				// Si la fecha recordatorio es hoy, se va a poner a 1 $item['fecha_limite']
				$date_hoy = new DateTime("now");
				$date_anotacion = new DateTime($row->fecha_recordatorio);

				if($date_anotacion->format('Y-m-d') == date('Y-m-d')) // es hoy
					$item['fecha_limite'] = 1;

				


				$item['reminderDateFormatted'] = date_format(new DateTime($row->fecha_recordatorio),"d/m/Y");
			}

			$item['createdAtFormatted'] = date_format(new DateTime($row->fecha_creacion),"d/m/Y H:i:s");
			$item['username'] = $row->first_name . " " . $row->last_name;

			if ($current_user->id == $row->owner_id)
				$item['isOwner'] = true;	
			else
				$item['isOwner'] = false;
			 

			array_push($comentarios, $item);
		}

		 $result = array(
                'items'     => $comentarios,
                'status'    => 'ok',
                'msg'       => ''
                );


		echo json_encode($result);
	}

	function ajax_AddComentario() {
		$idTarif = $this->input->post('idTarif');
		$commentaryReminderDate = $this->input->post('commentaryDate');
		if ($commentaryReminderDate == "")
			$commentaryReminderDate = NULL;
		$commentaryText = $this->input->post('commentaryText');

		$current_user = $this->ion_auth->user()->row();

		// Se añade el comentario a la tabla mensajes
		$resul = $this->funciones_comunes_model->addComentarioTarificacion($idTarif,$current_user->id,$commentaryReminderDate,$commentaryText);

		//log_message('debug', $resul);

		// Se incrementa el contador de mensajes de la tabla tarificaciones para la tarificacion correspondiente
		$resul = $this->funciones_comunes_model->incrementarMensajesTarificacion($idTarif);


		 $result = array(		 		
                'status'    => 'ok',
                'msg'       => ''
                );

		 echo json_encode($result);

	}

	function ajax_deleteComentario() {
		$idComentario = $this->input->post('commentaryId');

		$comentarioData = $this->funciones_comunes_model->getComentario($idComentario);
		$idTarif = $comentarioData->tarificacion_id;

		$resul = $this->funciones_comunes_model->deleteComentarioTarificacion($idComentario);
		if ($resul)
			$resul = $this->funciones_comunes_model->decrementarMensajesTarificacion($idTarif);




		 $result = array(
		 		'idTarif' => $idTarif,
                'status'    => 'ok',
                'msg'       => ''
                );

		 echo json_encode($result);


	}

	public function ajax_editComentario() {

		$idComentario = $this->input->post('commentaryId');
		$commentaryReminderDate = $this->input->post('commentaryDate');
		$commentaryText = $this->input->post('commentaryText');

		$resul = $this->funciones_comunes_model->editComentarioTarificacion($idComentario,$commentaryReminderDate,$commentaryText);
		//log_message('debug', $resul);


		 $result = array(		 		
                'status'    => 'ok',
                'msg'       => ''
                );

		 echo json_encode($result);		


	}


	function ajax_EnviarDocumentacion() {
		$idTarif = $this->input->post('idTarif');
		$docuEstado = $this->input->post('docuEstado');

		if ($docuEstado == "0")
			$valor = "1";
		else
			$valor = "0";

		$resul = $this->funciones_comunes_model->updateEnvioDocumentacionTarificacion($idTarif,$valor);
	
		 $result = array(
		 		'idTarif' => $idTarif,
                'status'    => 'ok',
                'msg'       => ''
                );

		 echo json_encode($result);
	}	

	function ajax_ContratarTarificacion() {
		$idTarif = $this->input->post('idTarif');
		$contratadaEstado = $this->input->post('contratadaEstado');

		if ($contratadaEstado == "0")
			$valor = "1";
		else
			$valor = "0";

		$resul = $this->funciones_comunes_model->updateContratarTarificacion($idTarif,$valor);
	
		 $result = array(
		 		'idTarif' => $idTarif,
                'status'    => 'ok',
                'msg'       => ''
                );

		 echo json_encode($result);
	}	




	// Funcion para poder exportar los listados de tarificaciones en distintos formatos (JSON, CSV, XML, HTML) mediante una peticion REST
	function exportarTarificaciones() {
		$this->form_validation->set_rules('selectRamo', 'Ramo de seguros', 'required');
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}		

		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Exportar listado de tarificaciones';

			$query = $this->funciones_comunes_model->getCorredurias(TRUE);
			$data['correduriasArray'] = $query->result();




			// Si admin --> mostrar todos los ramos
			// sino, mostrar los ramos que tenga asignados su correduria
			if ($this->ion_auth->is_admin()){
				$resul = $this->funciones_comunes_model->getRamos();
				
			}else{
				$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
				$resul = $this->funciones_comunes_model->getRamosCorreduria($correduriaId);
			}
			$data['ramosArray'] = $resul;



			$data['jsArray'] = array('public/js/exportarTarificaciones.js');		

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('tarificador/formularioExportarTarificaciones',$data);
			$this->load->view('templates/footer');						


		}
		else // Formulario validado correctamente.
		{
			if ($this->ion_auth->is_admin())
				$correduriaSelected = $this->input->post('selectCorreduria');
			else
				$correduriaSelected = $this->ion_auth->get_user_correduria_id()->correduria_id;

			$ramoSelected = $this->input->post('selectRamo');
			$selectContratada = $this->input->post('selectContratada');
			$selectDocu = $this->input->post('selectDocu');
			$fechaInicio = $this->input->post('fechaInicio');

			if ($fechaInicio != ""){
				$date = DateTime::createFromFormat('d/m/Y', $fechaInicio);
				$fechaInicioeu = $fechaInicio;
				$fechaInicio = $date->format('Y-m-d');
			}else{ 
				$fechaInicio = "orig";
				$fechaInicioeu ="Desde el origen de los tiempos";
			}

			if (($selectContratada === "2") && ($selectDocu === "2"))
				$data['filtroContratadaTexto'] = "No se ha seleccionado ningún filtro";

			if ($selectContratada === "0")
				$data['filtroContratadaTexto'] = "Mostrar solamente tarificaciones no contratadas.";
			elseif ($selectContratada === "1")
				$data['filtroContratadaTexto'] = "Mostrar solamente tarificaciones contratadas.";

			if ($selectDocu === "0")
				$data['filtroDocuTexto'] = "Mostrar solamente tarificaciones sin la documentación enviada.";
			elseif ($selectDocu === "1")
				$data['filtroDocuTexto'] = "Mostrar solamente tarificaciones con la documentación enviada.";			
			

			$data['title'] = 'Exportar listado de tarificaciones';
			$data['correduria_id'] = $correduriaSelected;
			$data['ramo_id'] = $ramoSelected;
			$data['filtroContratada'] = $selectContratada;
			$data['filtroDocu'] = $selectDocu;
			$data['fechaInicio'] = $fechaInicio;
			$data['fechaInicioeu'] = $fechaInicioeu;

			// Montamos la url con los parametros recogidos, y lo enviamos a la vista con los enlaces para generar los archivos a descargar

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('tarificador/EnlacesGeneradosExportarTarificaciones',$data);
			$this->load->view('templates/footer');				
		}		
	}


// Funcion para poder exportar los listados de tarificaciones en distintos formatos (JSON, CSV, XML, HTML) mediante una peticion REST
	function exportarAnotaciones() {
		$this->form_validation->set_rules('selectRamo', 'Ramo de seguros', 'required');
		if ($this->ion_auth->is_admin()){
			$this->form_validation->set_rules('selectCorreduria', 'Correduria', 'required');
		}		

		if ($this->form_validation->run() == FALSE)
		{

			$data['title'] = 'Exportar listado de anotaciones';

			$query = $this->funciones_comunes_model->getCorredurias(TRUE);
			$data['correduriasArray'] = $query->result();




			// Si admin --> mostrar todos los ramos
			// sino, mostrar los ramos que tenga asignados su correduria
			if ($this->ion_auth->is_admin()){
				$resul = $this->funciones_comunes_model->getRamos();
				
			}else{
				$correduriaId = $this->ion_auth->get_user_correduria_id()->correduria_id;
				$resul = $this->funciones_comunes_model->getRamosCorreduria($correduriaId);
			}
			$data['ramosArray'] = $resul;



			$data['jsArray'] = array('public/js/exportarTarificaciones.js');		

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('tarificador/formularioExportarAnotaciones',$data);
			$this->load->view('templates/footer');						


		}
		else // Formulario validado correctamente.
		{
			if ($this->ion_auth->is_admin())
				$correduriaSelected = $this->input->post('selectCorreduria');
			else
				$correduriaSelected = $this->ion_auth->get_user_correduria_id()->correduria_id;

			$ramoSelected = $this->input->post('selectRamo');
			$fechaInicio = $this->input->post('fechaInicio');
			$ordenar = $this->input->post('ordenar');

			if ($fechaInicio != ""){
				$date = DateTime::createFromFormat('d/m/Y', $fechaInicio);
				$fechaInicioeu = $fechaInicio;
				$fechaInicio = $date->format('Y-m-d');
			}else{ 
				$fechaInicio = "orig";
				$fechaInicioeu ="Desde el origen de los tiempos";
			}


			$data['title'] = 'Exportar listado de anotaciones';
			$data['correduria_id'] = $correduriaSelected;
			$data['ramo_id'] = $ramoSelected;
			$data['ordenar'] = $ordenar;
			
			
			$data['fechaInicio'] = $fechaInicio;
			$data['fechaInicioeu'] = $fechaInicioeu;

			// Montamos la url con los parametros recogidos, y lo enviamos a la vista con los enlaces para generar los archivos a descargar

			$this->load->view('templates/header');
			$this->load->view('templates/header_tarificador');
			$this->load->view('tarificador/EnlacesGeneradosExportarAnotaciones',$data);
			$this->load->view('templates/footer');				
		}		
	}	


}

