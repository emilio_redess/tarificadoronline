<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rest_server extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');

        $this->load->view('templates/header');
        $this->load->view('templates/header_tarificador');
		$this->load->view('rest_server');
		$this->load->view('templates/footer');
    }

    public function welcome_rest(){

		$this->load->helper('url');

		//$this->load->view('welcome_rest');

		$this->load->view('templates/header');
		$this->load->view('templates/header_tarificador');
		$this->load->view('welcome_rest');
		$this->load->view('templates/footer');		
	}
}
