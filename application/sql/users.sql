-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-07-2016 a las 19:39:05
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(3, '127.0.0.1', 'administrator', '$2y$08$E42dCdIe.rOz/JLezUnRcepw0tjoCjLZZU/LjV6wDZtfzXm6Rh9Bi', '', 'emilio@redess.es', '', NULL, NULL, NULL, 1268889823, 1469726829, 1, 'Emilio', 'Valls', 'REDESS', '722707776'),
(4, '::1', NULL, '$2y$08$9lPowBDecXaGmH7jlNeL6ermNeqGT8Ng2N7pB.sJUVK7GRUfHxlT.', NULL, 'raulgdm@grupotat.com', NULL, NULL, NULL, 'sghYF.kgnegTI4f/34eQf.', 1469560479, 1469719686, 1, 'Raúl', 'García de Madinabeitia', 'grupo TAT', '917100072'),
(5, '::1', NULL, '$2y$08$wpEE3aXRw4sdl82wE3kh5OmDbdNyrBCTvk69Xnr5ylvyg8cIPIRAe', NULL, 'usuario@tatalquiler.com', NULL, NULL, NULL, NULL, 1469611115, 1469719706, 1, 'Usuario 1', 'tat alquiler', 'grupo TAT', '66677788890'),
(6, '::1', NULL, '$2y$08$xmHSlMRUg16U/IXdeUBNhupGxK1iFG7KrrGG.gX5lSl0ktphXfbXu', NULL, 'usustb@stb.com', NULL, NULL, NULL, NULL, 1469708524, 1469724247, 1, 'usuarioSTB', 'alalala', 'STB', '666777111'),
(7, '::1', NULL, '$2y$08$2iUIfKhq34mLqbkNdg2rUe.tlg/ujOon/ft3MEORMLaGLsR3RRd36', NULL, 'usu1_cnk@cnk.com', NULL, NULL, NULL, NULL, 1469708730, 1469709036, 1, 'usuarioCNK', 'cnk', 'cnk', '1231212');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
