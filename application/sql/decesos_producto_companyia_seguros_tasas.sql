-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-01-2017 a las 20:28:17
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `basees_tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `decesos_producto_companyia_seguros_tasas`
--

CREATE TABLE IF NOT EXISTS `decesos_producto_companyia_seguros_tasas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `edad` tinyint(4) NOT NULL,
  `tasa` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Volcado de datos para la tabla `decesos_producto_companyia_seguros_tasas`
--

INSERT INTO `decesos_producto_companyia_seguros_tasas` (`id`, `id_producto`, `edad`, `tasa`) VALUES
(1, 75, 0, 0.004269),
(2, 75, 1, 0.004269),
(3, 75, 2, 0.004073),
(4, 75, 3, 0.003897),
(5, 75, 4, 0.003754),
(6, 75, 5, 0.003649),
(7, 75, 6, 0.003581),
(8, 75, 7, 0.003544),
(9, 75, 8, 0.003529),
(10, 75, 9, 0.003531),
(11, 75, 10, 0.003557),
(12, 75, 11, 0.003604),
(13, 75, 12, 0.003683),
(14, 75, 13, 0.003799),
(15, 75, 14, 0.003955),
(16, 75, 15, 0.004148),
(17, 75, 16, 0.004358),
(18, 75, 17, 0.004559),
(19, 75, 18, 0.004588),
(20, 75, 19, 0.004623),
(21, 75, 20, 0.004654),
(22, 75, 21, 0.00468),
(23, 75, 22, 0.004705),
(24, 75, 23, 0.004728),
(25, 75, 24, 0.004754),
(26, 75, 25, 0.004776),
(27, 75, 26, 0.004748),
(28, 75, 27, 0.004721),
(29, 75, 28, 0.00469),
(30, 75, 29, 0.004656),
(31, 75, 30, 0.004623),
(32, 75, 31, 0.00464),
(33, 75, 32, 0.004679),
(34, 75, 33, 0.004754),
(35, 75, 34, 0.00489),
(36, 75, 35, 0.00508),
(37, 75, 36, 0.005312),
(38, 75, 37, 0.005569),
(39, 75, 38, 0.00584),
(40, 75, 39, 0.006152),
(41, 75, 40, 0.006499),
(42, 75, 41, 0.006896),
(43, 75, 42, 0.007341),
(44, 75, 43, 0.007818),
(45, 75, 44, 0.008318),
(46, 75, 45, 0.00885),
(47, 75, 46, 0.00943),
(48, 75, 47, 0.010088),
(49, 75, 48, 0.010814),
(50, 75, 49, 0.011639),
(51, 75, 50, 0.012557),
(52, 75, 51, 0.013578),
(53, 75, 52, 0.014634),
(54, 75, 53, 0.015695),
(55, 75, 54, 0.016771),
(56, 75, 55, 0.017901),
(57, 75, 56, 0.019091),
(58, 75, 57, 0.02034),
(59, 75, 58, 0.021569),
(60, 75, 59, 0.022727),
(61, 75, 60, 0.023837),
(62, 75, 61, 0.024951),
(63, 75, 62, 0.026093),
(64, 75, 63, 0.027318),
(65, 75, 64, 0.028716),
(66, 75, 65, 0.030374),
(67, 75, 66, 0.033214),
(68, 75, 67, 0.0366),
(69, 75, 68, 0.040634),
(70, 75, 69, 0.045397),
(71, 75, 70, 0.050967),
(72, 75, 71, 0.057466),
(73, 75, 72, 0.065062),
(74, 75, 73, 0.076042),
(75, 75, 74, 0.089452),
(76, 75, 75, 0.112882),
(77, 75, 76, 0.12225),
(78, 75, 77, 0.13256),
(79, 75, 78, 0.14384),
(80, 75, 79, 0.15611),
(81, 75, 80, 0.16931),
(82, 75, 81, 0.18381),
(83, 75, 82, 0.19973),
(84, 75, 83, 0.21717),
(85, 75, 84, 0.23623),
(86, 75, 85, 0.25701),
(87, 75, 86, 0.27956),
(88, 75, 87, 0.30391),
(89, 75, 88, 0.33001),
(90, 75, 89, 0.3578),
(91, 75, 90, 0.38714),
(92, 75, 91, 0.417913),
(93, 75, 92, 0.449961),
(94, 75, 93, 0.483167),
(95, 75, 94, 0.517434),
(96, 75, 95, 0.552694),
(97, 75, 96, 0.588906),
(98, 75, 97, 0.626075),
(99, 75, 98, 0.664349),
(100, 75, 99, 0.704256),
(101, 75, 100, 0.747336),
(102, 75, 101, 0.798005),
(103, 75, 102, 0.844636),
(104, 75, 103, 0.892967),
(105, 75, 104, 0.943015);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `decesos_producto_companyia_seguros_tasas`
--
ALTER TABLE `decesos_producto_companyia_seguros_tasas`
  ADD CONSTRAINT `decesos_producto_companyia_seguros_tasas_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto_companyia_seguros` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
