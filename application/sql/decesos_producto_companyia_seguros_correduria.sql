-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-01-2017 a las 20:28:37
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `basees_tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `decesos_producto_companyia_seguros_correduria`
--

CREATE TABLE IF NOT EXISTS `decesos_producto_companyia_seguros_correduria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_correduria` int(11) NOT NULL,
  `id_cia` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_apcsc` (`id_correduria`),
  KEY `idx_apcsc_2` (`id_producto`),
  KEY `id_cia` (`id_cia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `decesos_producto_companyia_seguros_correduria`
--


--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `decesos_producto_companyia_seguros_correduria`
--
ALTER TABLE `decesos_producto_companyia_seguros_correduria`
  ADD CONSTRAINT `decesos_producto_companyia_seguros_correduria_ibfk_1` FOREIGN KEY (`id_correduria`) REFERENCES `correduria` (`id`),
  ADD CONSTRAINT `decesos_producto_companyia_seguros_correduria_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `producto_companyia_seguros` (`id`),
  ADD CONSTRAINT `decesos_producto_companyia_seguros_correduria_ibfk_3` FOREIGN KEY (`id_cia`) REFERENCES `companyia_seguros` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
