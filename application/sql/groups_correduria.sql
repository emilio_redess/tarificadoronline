-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-07-2016 a las 19:23:26
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups_correduria`
--

CREATE TABLE `groups_correduria` (
  `id` int(11) NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  `correduria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `groups_correduria`
--

INSERT INTO `groups_correduria` (`id`, `group_id`, `correduria_id`) VALUES
(1, 5, 9),
(2, 6, 1),
(3, 7, 2),
(4, 8, 5),
(5, 9, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `groups_correduria`
--
ALTER TABLE `groups_correduria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_gc_1` (`group_id`),
  ADD KEY `idx_gc_2` (`correduria_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `groups_correduria`
--
ALTER TABLE `groups_correduria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `groups_correduria`
--
ALTER TABLE `groups_correduria`
  ADD CONSTRAINT `groups_correduria_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `groups_correduria_ibfk_2` FOREIGN KEY (`correduria_id`) REFERENCES `correduria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
