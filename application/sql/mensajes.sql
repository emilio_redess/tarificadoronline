-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-08-2016 a las 14:50:43
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `tarificacion_id` int(11) DEFAULT NULL,
  `owner_id` int(11) UNSIGNED NOT NULL,
  `mensaje_tipo_id` int(4) DEFAULT NULL,
  `fecha_creacion` date NOT NULL,
  `hora_creacion` time DEFAULT NULL,
  `fecha_recordatorio` date DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasBeenRead` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `tarificacion_id`, `owner_id`, `mensaje_tipo_id`, `fecha_creacion`, `hora_creacion`, `fecha_recordatorio`, `descripcion`, `intro`, `contenido`, `icon`, `color`, `hasBeenRead`) VALUES
(1, NULL, 3, 3, '2016-07-06', NULL, NULL, 'Nuevo tarificador', 'Inauguramos nuestra nueva versión del tarificador de REDESS.', 'Nos complace anunciar que hoy, tras un largo desarrollo, por fin inauguramos nuestra nueva versión del tarificador REDESS.\r\n\r\nEsta nueva versión funciona sobre PHP y mySQL para la base de datos, utilizando el framework codeigniter como esqueleto de la aplicación.\r\n\r\nPara las peticiones remotas al servidor, hemos montado un servidor RESTful.\r\n\r\nA partir de ahora, que lo más complicado ya está hecho, iremos gradualmente incorporando nuevas funcionalidades según creamos necesario.', 'fa-cogs', 'red', 0),
(2, 10, 3, 2, '2016-07-27', NULL, '2016-08-10', 'mensaje 1', NULL, 'Texto del mensaje', NULL, NULL, 0),
(3, NULL, 3, 3, '2016-07-07', NULL, NULL, 'Listados de tarificaciones', 'Ya están funcionando todas las vistas de tarificaciones.', 'Ya puedes acceder a las tablas con todas las tarificaciones hechas por los usuarios desde el formulario de tu página web.\r\n\r\nDesde estos listados, haciendo click en sus botones correspondientes, puedes acceder a una vista donde se muestra con más detalle todos los datos que disponemos de dicha tarificación.', 'fa-database', 'lblue', 0),
(4, NULL, 3, 3, '2016-07-08', NULL, NULL, 'Seguro de alquiler', 'Estrenamos tarificador de seguros de alquiler.', 'Hemos implementado el tarificador de seguros de protección de Alquiler al resto de ramos con los que ya trabajábamos ( vida, salud, decesos, etc..)', 'fa-home', 'green', 0),
(5, NULL, 3, 3, '2016-07-13', NULL, NULL, 'Tarificar desde el panel', 'Ya puedes tarificar desde el panel de gestión.', 'Se ha terminado de implementar el formulario desde el que puedes tarificar igual que si lo hicieras desde tu página web ( en caso de que necesites comprobar rápidamente unos precios o para ayudar a algún usuario a rellenar los datos).\r\n\r\nPodrás acceder a este formulario desde la opción de menú "nueva tarificación", y desde ahí, eligiendo el ramo de seguros del que necesites tarificar y tengas activado con nosotros.', 'fa-plus', 'blue', 0),
(6, NULL, 3, 3, '2016-07-15', NULL, NULL, 'Funcionalidad básica', 'Ya puedes tarificar desde el panel de gestión.', 'Hemos completado la primera fase de desarrollo del tarificador en la que ya estamos dando una funcionalidad básica a todos nuestros clientes.\r\n\r\nA partir de aquí, seguiremos trabajando para continuar implementando nuevas funcionalidades, ya sea de las que tenemos en cola de tareas, o bien a través de cualquier sugerencia que nos podáis hacer siempre que veamos que pueda ser interesante y hacernos la vida más fácil tanto a nosotros como a vosotros a la hora de trabajar con el tarificador.', 'fa-bullhorn', 'orange', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1CAC12CA8864AF73` (`tarificacion_id`),
  ADD KEY `IDX_1CAC12CA7E3C61F9` (`owner_id`),
  ADD KEY `mensaje_tipo_id` (`mensaje_tipo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `mensajes_ibfk_1` FOREIGN KEY (`tarificacion_id`) REFERENCES `tarificaciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mensajes_ibfk_2` FOREIGN KEY (`mensaje_tipo_id`) REFERENCES `mensaje_tipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mensajes_ibfk_3` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
