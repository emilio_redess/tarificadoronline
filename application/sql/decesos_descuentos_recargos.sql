-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-01-2017 a las 20:28:51
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `basees_tarificador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `decesos_descuentos_recargos`
--

CREATE TABLE IF NOT EXISTS `decesos_descuentos_recargos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto_decesos` int(11) NOT NULL,
  `id_tipo_operacion` int(11) NOT NULL COMMENT 'descuento o recargo',
  `id_forma_pago` int(11) DEFAULT NULL COMMENT 'NULL = todas las formas de pago. mensual, trimestral,semestral,anual',
  `id_tipo_valor` int(11) NOT NULL COMMENT 'porcentaje o cantidad fija',
  `id_rango_numero_personas` int(11) DEFAULT NULL COMMENT '6 = cualquier rango. (1-6)',
  `edad_minima` int(11) NOT NULL,
  `edad_maxima` int(11) NOT NULL,
  `valor` float NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '0' COMMENT 'orden de aplicacion del descuento o recargo',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_drs_1` (`id_producto_decesos`),
  KEY `idx_drs_2` (`id_tipo_operacion`),
  KEY `idx_drs_3` (`id_forma_pago`),
  KEY `idx_drs_4` (`id_tipo_valor`),
  KEY `idx_drs_5` (`id_rango_numero_personas`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `decesos_descuentos_recargos`
--

INSERT INTO `decesos_descuentos_recargos` (`id`, `id_producto_decesos`, `id_tipo_operacion`, `id_forma_pago`, `id_tipo_valor`, `id_rango_numero_personas`, `edad_minima`, `edad_maxima`, `valor`, `orden`, `activo`) VALUES
(3, 1, 1, NULL, 1, 6, 0, 105, 15, 0, 1),
(4, 1, 2, NULL, 1, 6, 0, 105, 6.15, 0, 1),
(5, 1, 2, NULL, 2, 6, 0, 105, 20, 0, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `decesos_descuentos_recargos`
--
ALTER TABLE `decesos_descuentos_recargos`
  ADD CONSTRAINT `decesos_descuentos_recargos_ibfk_1` FOREIGN KEY (`id_producto_decesos`) REFERENCES `decesos_producto_companyia_seguros_correduria` (`id`),
  ADD CONSTRAINT `decesos_descuentos_recargos_ibfk_2` FOREIGN KEY (`id_tipo_operacion`) REFERENCES `descuentos_recargos` (`id`),
  ADD CONSTRAINT `decesos_descuentos_recargos_ibfk_3` FOREIGN KEY (`id_forma_pago`) REFERENCES `forma_de_pago` (`id`),
  ADD CONSTRAINT `decesos_descuentos_recargos_ibfk_4` FOREIGN KEY (`id_tipo_valor`) REFERENCES `porcentaje_cantidad` (`id`),
  ADD CONSTRAINT `decesos_descuentos_recargos_ibfk_5` FOREIGN KEY (`id_rango_numero_personas`) REFERENCES `rango_numero_personas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
