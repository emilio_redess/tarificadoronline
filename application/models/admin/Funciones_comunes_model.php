<?php
class Funciones_comunes_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function getProvincias()
	{
		$queryString = "SELECT * FROM provincia";
		$query = $this->db->query($queryString);

		return $query;
	}

	public function getProvinciaNombre($id_provincia)
	{
		$queryString = "SELECT provincia FROM provincia WHERE id_provincia = ?";
		$query = $this->db->query($queryString, array($id_provincia));

		return $query->row();
	}

	public function getProvinciaNombreFromId($id)
	{
		$queryString = "SELECT provincia FROM provincia WHERE id = ?";
		$query = $this->db->query($queryString, array($id));

		return $query->row();
	}	

	public function getContratantes()
	{
		$queryString = "SELECT * FROM contratante_tipo";
		$query = $this->db->query($queryString);

		return $query;

	}

	public function getContratanteNombre($id)
	{
		$queryString = "SELECT nombre FROM contratante_tipo WHERE id = ?";
		$query = $this->db->query($queryString, array($id));

		return $query->row();

	}	

	public function getNumberContratantes()
	{
		$queryString = "SELECT COUNT(*) as cantidad FROM contratante_tipo";
		$query = $this->db->query($queryString);

		return $query->row()->cantidad;

	}

	public function insertContratante($idProducto,$idContratante)
	{

		$queryString = "INSERT INTO salud_contratante (id_producto,id_contratante) VALUES ('" . $idProducto . "' ,'" . $idContratante . "')";
		$query = $this->db->query($queryString);
	}

	public function getProvincia_id($prefijo)
	{
		$queryString = "SELECT id FROM provincia WHERE id_provincia = ?";
		$query = $this->db->query($queryString, array($prefijo));

		$row = $query->row_array();
		$result = $row['id'];

		return $result;

	}

	public function getCorredurias($incluirColaboradores = FALSE)
	{
		$params = "";
		if (!$incluirColaboradores)
			$params =" AND ISNULL(parent_id)";

		$queryString = "SELECT * FROM correduria WHERE activo = '1'" . $params;
		$query = $this->db->query($queryString);

		return $query;

	}

	public function getCompanyias()
	{
		$queryString = "SELECT * FROM companyia_seguros";
		$query = $this->db->query($queryString);

		return $query;
	}

	public function getSubramo($id)
	{
		$queryString = "SELECT * FROM ramo_especial_subcategoria WHERE id=?";
		$query = $this->db->query($queryString,array($id));

		return $query->row();

	}


	public function getCompanyiasConProductosAlquiler($correduriaId,$codigoRamo)
	{
		$queryString = "SELECT 
							cs.id, cs.nombre
						FROM 
							companyia_seguros cs,
						    producto_companyia_seguros pcs,
						    alquiler_producto_companyia_seguros_correduria apcsc,
						    ramo_seguros rs
						WHERE
							rs.id = pcs.id_ramo AND
						    rs.codigo = ? AND
						    apcsc.id_correduria = ? AND
							apcsc.id_producto = pcs.id AND
						    pcs.id_companyia = cs.id";

		$query = $this->db->query($queryString,array($codigoRamo,$correduriaId));

		return $query->result();
	}	

	public function getCompanyiasConProductosAlquilerAdmin($codigoRamo)
	{
		$queryString = "SELECT DISTINCT 
							cs.id, cs.nombre
						FROM 
							companyia_seguros cs,
						    producto_companyia_seguros pcs,
						    alquiler_producto_companyia_seguros_correduria apcsc,
						    ramo_seguros rs
						WHERE
							rs.id = pcs.id_ramo AND
						    rs.codigo = ? AND
							apcsc.id_producto = pcs.id AND
						    pcs.id_companyia = cs.id";

		$query = $this->db->query($queryString,array($codigoRamo));

		return $query->result();
	}		

	public function getCompanyiasConProductosSalud($correduriaId,$codigoRamo)
	{
		$queryString = "SELECT DISTINCT
							cs.id, cs.nombre
						FROM 
							companyia_seguros cs,
						    producto_companyia_seguros pcs,
						    salud_producto_companyia_seguros_correduria spcsc,
						    ramo_seguros rs
						WHERE
							rs.id = pcs.id_ramo AND
						    rs.codigo = ? AND
						    spcsc.id_correduria = ? AND
							spcsc.id_producto_CM = pcs.id AND
						    pcs.id_companyia = cs.id";

		$query = $this->db->query($queryString,array($codigoRamo,$correduriaId));

		return $query->result();
	}	

	public function getCompanyiasConProductosSaludAdmin($codigoRamo)
	{
		$queryString = "SELECT DISTINCT
							cs.id, cs.nombre
						FROM 
							companyia_seguros cs,
						    producto_companyia_seguros pcs,
						    salud_producto_companyia_seguros_correduria spcsc,
						    ramo_seguros rs
						WHERE
							rs.id = pcs.id_ramo AND
						    rs.codigo = ? AND				
							(spcsc.id_producto_CM = pcs.id OR spcsc.id_producto_RE = pcs.id) AND
						    pcs.id_companyia = cs.id";

		$query = $this->db->query($queryString,array($codigoRamo));

		return $query->result();
	}

	public function getDatosEspecificosProductoAlquiler($correduriaId,$prodId){
		$queryString = "SELECT 
							afdc.id_producto as idProd, afdc.12meses as factorCalculo, apcsc.activo as activo
						FROM
							alquiler_factor_de_calculo afdc,
						    alquiler_producto_companyia_seguros_correduria apcsc
						WHERE
						    apcsc.id_producto = ? AND
						    afdc.id_producto = apcsc.id AND
						    apcsc.id_correduria = ?";

		$query = $this->db->query($queryString,array($prodId,$correduriaId));

		return $query->row();						    

	}	

	public function getDatosEspecificosProductoAlquilerAdmin($prodId){
		$queryString = "SELECT 
						c.nombre, afdc.12meses, apcsc.activo
					FROM
						alquiler_factor_de_calculo afdc,
					    alquiler_producto_companyia_seguros_correduria apcsc,
					    correduria c
					WHERE
					    apcsc.id_producto = ? AND
					    afdc.id_producto = apcsc.id AND
					    apcsc.id_correduria = c.id";

		$query = $this->db->query($queryString,array($prodId));

		return $query->result();						    

	}					


	public function getRamos()
	{
		$queryString = "SELECT * FROM ramo_seguros";
		$query = $this->db->query($queryString);

		return $query->result();
	}	

	public function getRamosCorreduria($correduriaId)
	{
		$queryString = "SELECT rs.* FROM ramo_seguros rs, ramos_correduria rc
						WHERE rc.ramo_id = rs.id AND rc.correduria_id = ?";
		$query = $this->db->query($queryString,array($correduriaId));
		return $query->result();
	}

	public function getRamo($nombre)
	{
		$queryString = "SELECT * FROM ramo_seguros WHERE name = ?";
		$query = $this->db->query($queryString,array($nombre));

		return $query->row();
	}	

	public function getFormaPago()
	{
		$queryString = "SELECT * FROM forma_de_pago";
		$query = $this->db->query($queryString);

		return $query;
	}		

	public function getOrigenTasas()
	{
		$queryString = "SELECT * FROM origen_tasas";
		$query = $this->db->query($queryString);

		return $query;
	}			

	public function getProductosCia($idCia,$codigoRamo,$soloVacios = FALSE,$codigoOrigen = 'WSLOC')
	{
		$params = "";
		if ($soloVacios)
			$params =" and pcs.tieneTasas = 0";		

		$queryString = "SELECT pcs.id, pcs.nombre_producto FROM producto_companyia_seguros pcs, ramo_seguros rs, origen_tasas ot 
						WHERE pcs.id_origen_tasas = ot.id and ot.codigo = '" . $codigoOrigen . "' and pcs.id_companyia='" . $idCia . "' and rs.codigo ='" . $codigoRamo . "' and pcs.id_ramo = rs.id" . $params;

		$query = $this->db->query($queryString);

		return $query;

	}	


	public function getProductosEspecialCorreduria($idCorreduria,$id_ramo_especial)
	{
		$queryString = "SELECT epcsc.id_producto as id, fp.codigo
						FROM
							especial_producto_companyia_seguros_correduria epcsc,
							producto_companyia_seguros pcs,
							forma_de_pago fp
						WHERE
							epcsc.id_correduria = '" . $idCorreduria . "' and 
							epcsc.activo = '1' and
							pcs.id = epcsc.id_producto and
							pcs.id_ramo_especial =  '" . $id_ramo_especial . "' and
							fp.id = pcs.id_formaPago";

		$query = $this->db->query($queryString);

		return $query;		

	}

	public function getProductosAlquilerCorreduria($idCorreduria,$tipoLocal)
	{

		$queryString = "SELECT 
							apcsc.id as id_producto_correduria, apcsc.id_producto as id, afdc.6meses, afdc.9meses, afdc.12meses, afdc.18meses
						FROM 
							alquiler_factor_de_calculo afdc, 
							alquiler_producto_companyia_seguros_correduria apcsc,
							producto_companyia_seguros pcs,
						
							alquiler_tipo_local atl
						                 
						WHERE 
							afdc.id_producto = apcsc.id and
							afdc.id_tipo_local = atl.id and
							apcsc.id_correduria = ? and 
							atl.codigo = ? and
							apcsc.activo = '1' and
							pcs.id = apcsc.id_producto";
		
/*
		$queryString = "SELECT 
		    distinct apcsc.id as id_producto_correduria, apcsc.id_producto as id
		FROM 
		    alquiler_producto_companyia_seguros_correduria apcsc		          
		WHERE 		   		 
		    apcsc.id_correduria = ? and 		  
		    apcsc.activo = '1'";	
*/			

		$query = $this->db->query($queryString,array($idCorreduria,$tipoLocal));

		return $query;
			
	}	



	public function getProductosVidaCorreduria($idCorreduria)
	{

		$queryString = "SELECT 
							dpcsc.id as id_producto_correduria, dpcsc.id_producto as id, dpcsc.consorcio_incluido, dpcsc.clea_incluido, dpcsc.isps_incluido, fp.codigo
						FROM 
						
							vida_producto_companyia_seguros_correduria dpcsc,
							producto_companyia_seguros pcs,
							forma_de_pago fp
						                 
						WHERE 
						
							dpcsc.id_correduria = '" . $idCorreduria . "' and 
							dpcsc.activo = '1' and
							pcs.id = dpcsc.id_producto and
							fp.id = pcs.id_formaPago";

		$query = $this->db->query($queryString);

		return $query;
			
	}


	public function getProductosDecesosCorreduria($idCorreduria)
	{

		$queryString = "SELECT 
							dpcsc.id as id_producto_correduria, dpcsc.id_producto as id, fp.codigo
						FROM 
						
							decesos_producto_companyia_seguros_correduria dpcsc,
							producto_companyia_seguros pcs,
							forma_de_pago fp
						                 
						WHERE 
						
							dpcsc.id_correduria = '" . $idCorreduria . "' and 
							dpcsc.activo = '1' and
							pcs.id = dpcsc.id_producto and
							fp.id = pcs.id_formaPago";

		$query = $this->db->query($queryString);

		return $query;
			
	}		

	public function getProductosSaludCorreduria($idCorreduria,$saludContratanteId)
	{

		$queryString = "SELECT distinct
							spcsc.id as id,spcsc.id_producto_CM as id_cm, spcsc.id_producto_RE as id_re, fp.codigo
						FROM 					
							salud_producto_companyia_seguros_correduria spcsc,
							producto_companyia_seguros pcs,
							forma_de_pago fp,
							provincia p,
     						provincias_zonas pz,
     						salud_contratante sc,
     						contratante_tipo ct
						                 
						WHERE 
						
							spcsc.id_correduria = ? and 
							spcsc.activo = '1' and
							(pcs.id = spcsc.id_producto_CM or
							pcs.id = spcsc.id_producto_RE) and
							fp.id = pcs.id_formaPago and
							ct.id = ? and
							sc.id_contratante = ct.id and
							sc.id_producto = spcsc.id";

		$query = $this->db->query($queryString,array($idCorreduria,$saludContratanteId));

		return $query;
			
	}	
	
	public function get_capital_provincia($id,$provinciaId) {

		$queryString = "SELECT valor_capital FROM decesos_capitales_provincia WHERE id_producto = ? AND id_provincia = ?";

		$query = $this->db->query($queryString,array($id,$provinciaId));

		return $query->row();		

	}	

	public function get_prima_salud($edad,$id_producto,$prefijoProvincia)
	{
		$queryString = "SELECT tasa 
						FROM salud_producto_companyia_seguros_tasas spcst,
							 provincia p,
							 provincias_zonas pz
						WHERE
							pz.id_provincia = p.id and
						    p.id_provincia = ? and
						    spcst.id_zona = pz.id_zona and
						    pz.incluida = '1' and
							spcst.edad = ? and 
							spcst.id_producto = ?";

		$query = $this->db->query($queryString,array($prefijoProvincia,$edad,$id_producto));

		return $query->row();		

	}

	public function get_prima_vida($edad,$capital,$id)
	{
		$queryString = "SELECT * from vida_producto_companyia_seguros_tasas WHERE id_producto = ? and edad = ? and capital_minimo < ? and capital_maximo > ?";
		$query = $this->db->query($queryString,array($id,$edad,$capital,$capital));

		return $query->row();
		
	}

	public function get_prima_decesos($edad,$id_producto_correduria)
	{
		$queryString = "SELECT tasa FROM decesos_producto_companyia_seguros_tasas WHERE id_producto = ? AND edad = ?";

		$query = $this->db->query($queryString,array($id_producto_correduria,$edad));

		return $query->row();	
	}

	public function getImpuestosRecargosAlquiler($producto_id,$tipoLocalCodigo)
	{
		$queryString = "SELECT adr.* 
						from alquiler_descuentos_recargos adr, alquiler_producto_companyia_seguros_correduria apcsc, alquiler_tipo_local atl

						WHERE adr.id_producto_alquiler = apcsc.id AND 
						adr.activo = '1' AND
						apcsc.id_producto = ? AND
						atl.codigo = ? AND
						adr.id_tipo_local = atl.id
						ORDER BY orden";

		$query = $this->db->query($queryString,array($producto_id,$tipoLocalCodigo));

		return $query;


	}

	public function getImpuestosRecargosSalud($producto_id,$numeroPersonas)
	{
		$queryString = "SELECT sdr.* 
						from salud_descuentos_recargos sdr, rango_numero_personas rnp

						WHERE sdr.id_producto_salud = ? AND 
						sdr.activo = '1' AND
						sdr.id_rango_numero_personas = rnp.id AND
						rnp.valor_minimo <= ? AND
						rnp.valor_maximo >= ?
						ORDER BY orden";

		$query = $this->db->query($queryString,array($producto_id,$numeroPersonas,$numeroPersonas));

		return $query;


	}	


	public function getImpuestosRecargosVida($producto_id,$capital)
	{
		$queryString = "SELECT vdr.*
						FROM vida_descuentos_recargos vdr
						WHERE vdr.id_producto_vida = ? AND
						vdr.activo = '1' 
						ORDER BY orden";

		$query = $this->db->query($queryString,array($producto_id));

		return $query;


	}



	public function getImpuestosRecargosDecesos($producto_id,$numeroPersonas)
	{
		$queryString = "SELECT ddr.* 
						from decesos_descuentos_recargos ddr, rango_numero_personas rnp

						WHERE ddr.id_producto_decesos = ? AND 
						ddr.activo = '1' AND
						ddr.id_rango_numero_personas = rnp.id AND
						rnp.valor_minimo <= ? AND
						rnp.valor_maximo >= ?
						ORDER BY orden";

		$query = $this->db->query($queryString,array($producto_id,$numeroPersonas,$numeroPersonas));

		return $query;


	}		

	public function getDatosProductoSalud($id)
	{
		$queryString = "SELECT nombre FROM salud_producto_companyia_seguros_correduria WHERE id=?";

		$query = $this->db->query($queryString,array($id));

		return $query->row();	

	}

	public function getCoberturasAlquiler($idProductoCorreduria)
	{

		// Primero se va a comprobar si para el producto existen coberturas en la tabla de coberturas especificas (alquiler_coberturas). Si existen, se devuelven éstas, y sino, se devuelven las 
		// coberturas generales que estan en la tabla "productos_coberturas_default" (Si aqui tampoco existen para dicho producto, se devuelve NULL)

		$queryString ="SELECT data FROM alquiler_coberturas WHERE id_producto = ?";
		$query = $this->db->query($queryString,array($idProductoCorreduria));

		if (is_null($query->row())){
			$queryString ="SELECT 
							pcd.data 
						FROM 
							productos_coberturas_default pcd, alquiler_producto_companyia_seguros_correduria apcsc 
						WHERE 
							apcsc.id_producto = pcd.id_producto and
							apcsc.id = ?";
			$query = $this->db->query($queryString,array($idProductoCorreduria));
		}
		return $query->row();
	}

	public function getCoberturasDecesos($idProductoCorreduria)
	{

		// Primero se va a comprobar si para el producto existen coberturas en la tabla de coberturas especificas (decesos_coberturas). Si existen, se devuelven éstas, y sino, se devuelven las 
		// coberturas generales que estan en la tabla "productos_coberturas_default" (Si aqui tampoco existen para dicho producto, se devuelve NULL)

		$queryString ="SELECT data FROM decesos_coberturas WHERE id_producto = ?";
		$query = $this->db->query($queryString,array($idProductoCorreduria));

		if (is_null($query->row())){
			$queryString ="SELECT 
							pcd.data 
						FROM 
							productos_coberturas_default pcd, decesos_producto_companyia_seguros_correduria dpcsc 
						WHERE 
							dpcsc.id_producto = pcd.id_producto and
							dpcsc.id = ?";
			$query = $this->db->query($queryString,array($idProductoCorreduria));
		}
		return $query->row();
	}	

	public function getCoberturasVida($idProductoCorreduria)
	{

		// Primero se va a comprobar si para el producto existen coberturas en la tabla de coberturas especificas (vida_coberturas). Si existen, se devuelven éstas, y sino, se devuelven las 
		// coberturas generales que estan en la tabla "productos_coberturas_default" (Si aqui tampoco existen para dicho producto, se devuelve NULL)

		$queryString ="SELECT data FROM vida_coberturas WHERE id_producto = ?";
		$query = $this->db->query($queryString,array($idProductoCorreduria));

		if (is_null($query->row())){
			$queryString ="SELECT 
							pcd.data 
						FROM 
							productos_coberturas_default pcd, vida_producto_companyia_seguros_correduria dpcsc 
						WHERE 
							dpcsc.id_producto = pcd.id_producto and
							dpcsc.id = ?";
			$query = $this->db->query($queryString,array($idProductoCorreduria));
		}
		return $query->row();
	}		



	public function getDocumentosSalud($idProductoCorreduria)
	{
		$queryString = "SELECT data FROM salud_productos_documentacion_correduria WHERE id_producto = ?";							
		$query = $this->db->query($queryString,array($idProductoCorreduria));
		return $query->row();		
	}

	public function getDocumentosDecesos($idProductoCorreduria)
	{
		$queryString = "SELECT data FROM decesos_productos_documentacion_correduria WHERE id_producto = ?";							
		$query = $this->db->query($queryString,array($idProductoCorreduria));
		return $query->row();		
	}	


	public function getCoberturasSalud($idProductoCorreduria)
	{

		// Primero se va a comprobar si para el producto existen coberturas en la tabla de coberturas especificas (salud_coberturas). Si existen, se devuelven éstas, y sino, se devuelven las 
		// coberturas generales que estan en la tabla "productos_coberturas_default" (Si aqui tampoco existen para dicho producto, se devuelve NULL)
		$queryString = "SELECT data_cm,data_re FROM salud_coberturas WHERE id_producto = ?";							
		$query = $this->db->query($queryString,array($idProductoCorreduria));

		if (is_null($query->row())){
			$queryString ="SELECT (SELECT pcd.data FROM productos_coberturas_default pcd, salud_producto_companyia_seguros_correduria spcsc WHERE 
							spcsc.id_producto_CM = pcd.id_producto and
							spcsc.id = ?) as data_cm,

							(SELECT 
								pcd.data FROM productos_coberturas_default pcd, salud_producto_companyia_seguros_correduria spcsc WHERE 
								spcsc.id_producto_RE = pcd.id_producto and
								spcsc.id = ?) as data_re";
			$query = $this->db->query($queryString,array($idProductoCorreduria,$idProductoCorreduria));
		}		
		return $query->row();
	}	

	public function getDatosProducto($idProducto)
	{
		$queryString = "SELECT pcs.id as prod_id, pcs.nombre_producto as nomProducto, pcs.logo as logoProducto, cs.nombre as nomCia, rs.codigo as codigoRamo, rs.name as nombreRamo, fp.codigo as formaPagoCodigo, fp.nombre as formaPagoNombre, ot.codigo as origenTasasCodigo, ot.nombre as origenTasasNombre
			FROM producto_companyia_seguros pcs, companyia_seguros cs, ramo_seguros rs, forma_de_pago fp, origen_tasas ot
			WHERE pcs.id = '" . $idProducto . "' and pcs.id_companyia = cs.id and pcs.id_ramo = rs.id and pcs.id_formaPago = fp.id and pcs.id_origen_tasas = ot.id";

		$query = $this->db->query($queryString);

		return $query->row();

	}

	public function getTasasProducto($idProducto,$codigoRamo)
	{
		switch ($codigoRamo) {
			case 'SAL':
				$queryString ="SELECT 
								   spcst.edad, spcst.tasa, z.nombre
								FROM 
								   salud_producto_companyia_seguros_tasas spcst, zonas z
								WHERE 
								   id_producto = ? AND
								   spcst.id_zona = z.id";
				$query = $this->db->query($queryString,array($idProducto));
			break;

			case 'VID':
			break;

			case 'DCS':
			break;

			case 'HOG':
			break;


		}
		

		return $query;
	}

	public function insertarTasas($idProducto,$queryString,$queryString_borrar_tasas_antiguas)
	{

		// ejecutamos la query que borrará las tasas antiguas que van a ser sustituidas
		$query_borrado = $this->db->query($queryString_borrar_tasas_antiguas);

		if ($query_borrado){

			$query = $this->db->query($queryString);

			if ($query){ // si se han podido agregar las tasas correctamente, vamos a activar el flag de "tieneTasas" y a actualizar la fecha de "fecha_ultima_actualizacion"
				$sql = "UPDATE producto_companyia_seguros SET tieneTasas='1', fecha_ultima_actualizacion=now() WHERE id=" . $idProducto;
				$query_update = $this->db->query($sql);
			}
			return array("success" => $query, "queryString" => $queryString);	
		}
		else{

			return array("success" => $query_borrado, "queryString" => $queryString_borrar_tasas_antiguas);	
		}
	}	

	public function crearProducto($idCia,$codigoRamo,$idFormaPago,$idOrigenTasas,$nombreProducto,$nombreLogo)
	{
		// Primero saco la id del ramo
		$query = $this->db->query("SELECT id FROM ramo_seguros WHERE codigo ='" . $codigoRamo . "'");
		$row = $query->row();
		$idRamo = $row->id;

		// Inserto el producto en la tabla
		$queryString = "INSERT INTO producto_companyia_seguros (id_companyia,id_ramo,id_formaPago,id_origen_tasas,nombre_producto,logo) VALUES ('" . $idCia . "' ,'" . $idRamo . "','" . $idFormaPago . "','" . $idOrigenTasas . "' ,'" . $nombreProducto . "','" . $nombreLogo . "')";


		$query = $this->db->query($queryString);
		$id = $this->db->insert_id(); // Will return the last insert id.	


		// testing
		//$query = FALSE;
		//$id = 69;
	
		return array("id" => $id, "success" => $query, "queryString" => $queryString);	
	}

	public function asignarProductoCorreduria($idCorreduria, $idCia, $codigoRamo, $idProducto = NULL, $nombreProducto = NULL, $idProductoCM = NULL, $idProductoRE = NULL)
	{

		switch ($codigoRamo) {
		    case 'SAL':
		        $queryString = "INSERT INTO salud_producto_companyia_seguros_correduria (nombre,id_correduria, id_cia, id_producto_CM,id_producto_RE) VALUES ('" . $nombreProducto . "' ,'" . $idCorreduria . "' ,'" . $idCia . "' ,'" . $idProductoCM . "' ,'" . $idProductoRE . "')";
		        break;
		    case 'VID':
		        $queryString = "INSERT INTO vida_producto_companyia_seguros_correduria (id_correduria,id_cia, id_producto) VALUES ('" . $idCorreduria . "' ,'" . $idCia . "' ,'" . $idProducto . "')";
		        break;
		    case 'DCS':
		        $queryString = "INSERT INTO decesos_producto_companyia_seguros_correduria (id_correduria,id_cia, id_producto) VALUES ('" . $idCorreduria . "' ,'" . $idCia . "' ,'" . $idProducto . "')";
		        break;
		    case 'HOG':
		        $queryString = "";
		        break;		        
		}		


		$query = $this->db->query($queryString);
		$id = $this->db->insert_id(); // Will return the last insert id.

/*
		$query = TRUE;
		$id = 69;
*/
		return array("id" => $id, "success" => $query, "queryString" => $queryString);	
	}	

	public function getDescuentosRecargos()
	{
		$queryString = "SELECT * FROM descuentos_recargos";
		$query = $this->db->query($queryString);

		return $query;

	}

	public function getPorcentajeCantidad()
	{
		$queryString = "SELECT * FROM porcentaje_cantidad";
		$query = $this->db->query($queryString);

		return $query;

	}

	public function getRangoNumeroPersonas()
	{
		$queryString = "SELECT * FROM rango_numero_personas";
		$query = $this->db->query($queryString);

		return $query;

	}

	public function getDatosCorreduriaFromGroupId($group_id)
	{

		$queryString = "SELECT * FROM correduria c, groups_correduria gc 
		WHERE gc.correduria_id = c.id
		AND gc.group_id = ?";
		$query = $this->db->query($queryString,array($group_id));
		foreach ($query->result() as $row)
		{
	    	return $row;
		}
	}

	public function getMensajes($mensajeTipoCodigo = NULL)
	{
		$queryString = "SELECT m.* FROM mensajes m, mensaje_tipo mt
		WHERE mt.id = m.mensaje_tipo_id
		AND mt.codigo = ?
		ORDER BY m.id DESC";

		$query = $this->db->query($queryString,array($mensajeTipoCodigo));
		return $query->result();

	}


	public function getMensaje($idMensaje)
	{
		$queryString = "SELECT * FROM mensajes
		WHERE id = ?";
		

		$query = $this->db->query($queryString,array($idMensaje));
		foreach ($query->result() as $row)
		{
	    	return $row;
		}

	}

	public function getRamosFromGroupId($groupId)
	{
		$queryString = "SELECT rs.codigo FROM ramo_seguros rs, ramos_correduria rc, groups_correduria gc
						WHERE gc.correduria_id = rc.correduria_id 
						AND rc.ramo_id = rs.id
						AND gc.group_id = ?";
		$query = $this->db->query($queryString,array($groupId));
		return $query->result_array();

	}

	public function getColaboradores($groupId)
	{
		/*
		$queryString = "SELECT COUNT(parent_id) as cantidad FROM correduria c,groups_correduria gc
						WHERE gc.group_id = ? and 
							  gc.correduria_id = c.parent_id";*/

		$queryString = "SELECT c.id,c.codigo,c.nombre FROM correduria c,groups_correduria gc
						WHERE gc.group_id = ? and 
							  gc.correduria_id = c.parent_id";							  

		$query = $this->db->query($queryString,array($groupId));
		return $query;
	}

	public function getDatosDuracionProductoDeporteRiesgo($duracion)
	{
		$queryString = "SELECT id, nombre, meses_adicionales FROM especial_deportesRiesgo_duracion
						WHERE min_dias <= ? and max_dias >= ?";

		$query = $this->db->query($queryString,array($duracion,$duracion));
		return $query->row();
	}	

	public function getDatosResidenteProductoDeporteRiesgo($idProducto,$zona_precio)
	{
		$queryString = "SELECT nombre, valor_mes_adicional FROM especial_deportesRiesgo_residente
						WHERE id_producto = ? AND id_zona = ?";

		$query = $this->db->query($queryString,array($idProducto,$zona_precio));
		return $query->row();
	}	

	public function getDatosPrimaProductoDeporteRiesgo($id,$duracion_aux,$residente)
	{
		$queryString = "SELECT valor FROM especial_deportesRiesgo_tasas
						WHERE id_producto = ? AND
						      id_duracion = ? AND
						      id_residente = ?";

		$query = $this->db->query($queryString,array($id,$duracion_aux,$residente));
		return $query->row();						      

	}	

	public function getDatosPrimaProductoTransporte($id,$limite_viaje,$averia_frio,$garantias){
		$queryString = "SELECT * FROM especial_transporte_tasas
						WHERE id_producto = ? AND
						      limite = ? AND frio = ? AND garantias = ?";

		$query = $this->db->query($queryString,array($id,$limite_viaje,$averia_frio,$garantias));
		return $query->row();

	}

	public function getDatosPrimaProductoRCTransporte($id,$suma,$adr){
		$queryString = "SELECT * FROM especial_rctransporte_tasas
						WHERE id_producto = ? AND
								suma_asegurada = ? AND
						      adr = ?";

		$query = $this->db->query($queryString,array($id,$suma,$adr));
		return $query->row();

	}	


	public function getComentariosTarificacion($idTarif)
	{
		$queryString = "SELECT m.*,u.first_name,u.last_name FROM mensajes m,users u WHERE 
						m.owner_id = u.id AND
						tarificacion_id = ?";

		$query = $this->db->query($queryString,array($idTarif));
		return $query;
	}


	public function addComentarioTarificacion($idTarif,$userId,$commentaryReminderDate,$commentaryText)
	{
		if (is_null($commentaryReminderDate))
			$queryString = "INSERT INTO mensajes (tarificacion_id,owner_id,mensaje_tipo_id,fecha_creacion,contenido) VALUES ('" . $idTarif . "' ,'" . $userId . "','2', NOW(), '" . $commentaryText . "' )";
		else
			$queryString = "INSERT INTO mensajes (tarificacion_id,owner_id,mensaje_tipo_id,fecha_creacion,fecha_recordatorio,contenido) VALUES ('" . $idTarif . "' ,'" . $userId . "','2', NOW(),STR_TO_DATE('" . $commentaryReminderDate . "','%d/%m/%Y'), '" . $commentaryText. "' )";
		$query = $this->db->query($queryString);

		return $queryString;
	}

	public function incrementarMensajesTarificacion($idTarif)
	{
		$queryString = "UPDATE tarificaciones SET numMensajes = numMensajes + 1 WHERE id = ?";
		$query = $this->db->query($queryString,array($idTarif));
		return $query;
	}

	public function decrementarMensajesTarificacion($idTarif)
	{
		$queryString = "UPDATE tarificaciones SET numMensajes = numMensajes - 1 WHERE id = ?";
		$query = $this->db->query($queryString,array($idTarif));
		return $query;
	}	

	public function updateEnvioDocumentacionTarificacion($idTarif,$valor)
	{
		$queryString = "UPDATE tarificaciones SET documentacionEnviada = ? WHERE id = ?";
		$query = $this->db->query($queryString,array($valor,$idTarif));
		return $query;
	}	

	public function updateContratarTarificacion($idTarif,$valor)
	{
		$queryString = "UPDATE tarificaciones SET contratada = ? WHERE id = ?";
		$query = $this->db->query($queryString,array($valor,$idTarif));
		return $query;
	}		

	public function getComentario($idComentario)
	{
		$queryString ="SELECT * FROM mensajes WHERE id = ?";
		$query = $this->db->query($queryString,array($idComentario));
		return $query->row();
	}

	public function deleteComentarioTarificacion($idComentario)
	{
		$queryString = "DELETE FROM mensajes WHERE id = ?";

			$query = $this->db->query($queryString,array($idComentario));
			return $query;	
	}

	public function editComentarioTarificacion($idComentario,$commentaryReminderDate,$commentaryText)
	{
		$queryString = "UPDATE mensajes SET contenido = ?, fecha_recordatorio = STR_TO_DATE(?,'%d/%m/%Y')  WHERE id = ?";
		$query = $this->db->query($queryString,array($commentaryText,$commentaryReminderDate,$idComentario));
		return $query;

	}

	public function getNumeroMensajesPendientes($userId){

		$queryString = "SELECT count(*) as cantidad FROM mensajes WHERE owner_id = ? AND fecha_recordatorio = DATE(NOW())";
		$query = $this->db->query($queryString,array($userId));
		return $query->row();
	}

	public function getAnotacionesDia($userId) {

		$queryString = "SELECT m.tarificacion_id, t.codigo_tarificacion, t.ramo_id, m.fecha_creacion, m.contenido 
						FROM  mensajes m, tarificaciones t
						WHERE mensaje_tipo_id =2
						AND owner_id =?
						AND m.tarificacion_id = t.id
						AND m.fecha_recordatorio = DATE(NOW())
						AND m.hasBeenRead = 0";

		$query = $this->db->query($queryString,array($userId));
		return $query;


	}

}

