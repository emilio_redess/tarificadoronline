<?php

class Salud_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}


	
	public function activacionProducto($idProducto,$activo_flag)
	{
		$sql = "UPDATE salud_producto_companyia_seguros_correduria SET activo='" . $activo_flag . "' WHERE id=" . $idProducto;
		$this->db->query($sql);

		return array("success" => '1', "queryString" => $sql);
	}
	

	public function getProductosCorreduria($idCorreduria,$idCia,$estado_activo)
	{

		switch ($estado_activo){
			case "0":
				$params = " and activo = '0'";
			break;

			case "1":
				$params = " and activo = '1'";
			break;

			case "2":
				$params = "";
			break;

		}

		$queryString = "SELECT pscsc.id, pscsc.nombre FROM salud_producto_companyia_seguros_correduria pscsc 
						WHERE pscsc.id_correduria = " . $idCorreduria . " and pscsc.id_cia = " . $idCia . $params;

		$query = $this->db->query($queryString);

		return $query;

	}

	public function getDatosProducto($idProducto)
	{

		$queryString = "SELECT spcsc.*, cs.nombre as cia_nombre FROM salud_producto_companyia_seguros_correduria spcsc, companyia_seguros cs WHERE spcsc.id = ? AND spcsc.id_cia = cs.id";


		$query = $this->db->query($queryString,array($idProducto));
		return $query->row();
	}

	public function crearImpuestoRecargo($idProducto,$idSubproducto,$idOperacion,$idFormaOperacion,$idFormaPago,$idRangoPersonas,$edadMinima,$edadMaxima,$valor)
	{
		$valor2 = str_replace(",", ".", $valor);
		if (!is_numeric($valor2))
		{
			return array("success" => FALSE, "queryString" => "No se han podido guardar los datos porque el valor introducido no es numérico.");

		}
		else
		{

			if ($idFormaPago == "TODAS")
				$valorFormaPago = "NULL";
			else
				$valorFormaPago = $idFormaPago;

			$queryString ="INSERT INTO salud_descuentos_recargos (id_producto_salud,cm_re,id_tipo_operacion,id_forma_pago,id_tipo_valor,id_rango_numero_personas,edad_minima,edad_maxima,valor) 
							VALUES ('" . $idProducto . "' ,'" . $idSubproducto . "' ,'" . $idOperacion . "' ," . $valorFormaPago . " ,'" . $idFormaOperacion . "' ,'" . $idRangoPersonas . "' ,'" . $edadMinima . "' ,'" . $edadMaxima . "' ,'" . $valor2 . "')";

			$query = $this->db->query($queryString);
			$id = $this->db->insert_id(); // Will return the last insert id.

			/*
			$query = TRUE;
			$id = 69;
			*/
			return array("id" => $id, "success" => $query, "queryString" => $queryString);	
		}		
	}		
}