<?php

class Alquiler_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}


	
	public function activacionProducto($idProducto,$activo_flag)
	{
		$sql = "UPDATE alquiler_producto_companyia_seguros_correduria SET activo='" . $activo_flag . "' WHERE id=" . $idProducto;
		$this->db->query($sql);

		return array("success" => '1', "queryString" => $sql);
	}
	


	public function actualizar_factor_calculo($valor,$idProducto) {

		$sql = "UPDATE alquiler_factor_de_calculo SET 12meses=? WHERE id_producto=?";
		$this->db->query($sql,array($valor,$idProducto));

		return array("success" => '1', "queryString" => $sql);
	}	

	public function get_tipo_local_nombre($codigo) {

		$queryString = "SELECT nombre FROM alquiler_tipo_local WHERE codigo = ?";
		$query = $this->db->query($queryString,array($codigo));
		return $query->row();
	}	
}