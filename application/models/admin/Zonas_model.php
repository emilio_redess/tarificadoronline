<?php
// este model no lo uso
class Zonas_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
     
        public function crearZona($nombre)
        {
			$queryString = "INSERT INTO zonas (nombre) VALUES ('" . $nombre . "')";
			$query = $this->db->query($queryString);

			$id = $this->db->insert_id(); // Will return the last insert id.



			$cadena = "INSERT INTO provincias_zonas (id_zona,id_provincia) VALUES ";
			for ($i = 1; $i <= 51; $i++) {
				$cadena.= "(" . $id . "," . $i . "),";			
			}
			$cadena.= "(" . $id . ", 52);";
			$query = $this->db->query($cadena);


        }

        public function borrarZona($id)
        {
        	$queryString = "DELETE FROM provincias_zonas
							WHERE id_zona = " . $id;

			$query = $this->db->query($queryString);	
			
			if ($query == TRUE){
				$queryString = "DELETE FROM zonas WHERE id =" . $id;
				$query = $this->db->query($queryString);
				if ($query == TRUE) return TRUE;
				else return FALSE;

			}
			else return FALSE;


        }

        // La variable $todas la usamos para distinguir cuando no queremos mostrar la zona especial que incluye todas las provincias
		public function getZonas($todas = FALSE)
		{
			if ($todas == FALSE)
				$queryString = "SELECT * FROM zonas WHERE id != '1'";
			else
				$queryString = "SELECT * FROM zonas";
			$query = $this->db->query($queryString);

			return $query;

		}

		public function getNombreZona($id)
		{
			$queryString = "SELECT nombre FROM zonas WHERE id='" . $id . "'";
			$query = $this->db->query($queryString);

			$row = $query->row();
			return $row->nombre;

		}

		public function setNombreZona($id,$nombre)
		{
			$queryString = "UPDATE zonas SET nombre='" . $nombre . "' WHERE id= " . $id;
			$query = $this->db->query($queryString);
		}		

		public function getZonasProducto($idProducto)
		{
			$queryString = "SELECT DISTINCT 
							    z.id, z.nombre
							FROM 
							    salud_producto_companyia_seguros_tasas spcst, zonas z
							WHERE 
							    id_producto = ? AND
							    spcst.id_zona = z.id";


			
			$query = $this->db->query($queryString,array($idProducto));

			return $query;

		}

		public function getDatosZona($zona)
		{
			$queryString = "SELECT p.id, p.id_provincia, p.provincia, pz.incluida
							FROM provincia p, provincias_zonas pz
							WHERE pz.id_zona = '" . $zona . "' and pz.id_provincia = p.id";


			
			$query = $this->db->query($queryString);

			return $query;
		}

		public function modificarZona($zonaId,$arrayProvincias)
		{
			for ($i = 1; $i <= 52; $i++) {
				$queryString = "UPDATE provincias_zonas SET incluida=" . $arrayProvincias[$i] . " WHERE id_zona= " . $zonaId . " and id_provincia=" . $i;
				$query = $this->db->query($queryString);
			}
		}	
}