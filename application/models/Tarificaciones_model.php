<?php

class Tarificaciones_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

		public function emptyTrash()
		{
			$usuario = $this -> ion_auth -> get_user_id();

/*
			$queryString = "DELETE FROM tarificaciones USING tarificaciones INNER JOIN users INNER JOIN usuarios_correduria
			WHERE 
				tarificaciones.activo = '0' AND
				tarificaciones.correduria_id = usuarios_correduria.correduria_id AND
			    users.id = usuarios_correduria.user_id AND
			    usuarios_correduria.user_id = " . $usuario;
*/

			$queryString = "DELETE FROM tarificaciones USING tarificaciones INNER JOIN users INNER JOIN users_groups INNER JOIN groups INNER JOIN groups_correduria INNER JOIN correduria
			WHERE 
				users.id = users_groups.user_id AND
				users_groups.group_id = groups.id AND
				groups.id = groups_correduria.group_id AND
				groups_correduria.correduria_id = correduria.id AND
				tarificaciones.correduria_id = groups_correduria.correduria_id AND
				tarificaciones.activo = '0' AND
				users.id = " . $usuario;

			$query = $this->db->query($queryString);

			return $query;

		}

		public function deleteSingle($tarificacionId)
		{

			$queryString ="DELETE FROM mensajes WHERE tarificacion_id = " . $tarificacionId;
			$query = $this->db->query($queryString);

			if ($query == TRUE){
				$queryString ="DELETE FROM tarificaciones WHERE id = " . $tarificacionId;
				$query = $this->db->query($queryString);
			}

			return $query;
		}


		public function restoreSingle($tarificacionId)
		{
			$queryString ="UPDATE tarificaciones 
						   SET activo = 1, fecha_borrado = NULL, deletedBy_id = NULL 
						   WHERE id = " . $tarificacionId;
			$query = $this->db->query($queryString);

			return $query;
		}

		public function sendToTrash($tarificacionId)
		{
			$usuario = $this -> ion_auth -> get_user_id();
			
			$queryString ="UPDATE tarificaciones 
						   SET activo = 0, fecha_borrado = NOW(), deletedBy_id = " . $usuario . " 
						   WHERE id = " . $tarificacionId;
			$query = $this->db->query($queryString);

			return $query;
		}

		public function get_correduria_id($codigoCorreduria)
		{
			// get correduria_id
			$queryString = "SELECT id FROM correduria WHERE codigo = ?";
			$query = $this->db->query($queryString,array($codigoCorreduria));

			$row = $query->row_array();
			$correduria_id = $row['id'];

			return $correduria_id;

		}

		public function get_correduria_codigo($idCorreduria)
		{
			// get correduria_codigo
			$queryString = "SELECT codigo FROM correduria WHERE id = ?";
			$query = $this->db->query($queryString,array($idCorreduria));

			$row = $query->row_array();
			$correduria_codigo = $row['codigo'];

			return $correduria_codigo;

		}	

		public function get_datos_correduria($codigoCorreduria)
		{
			$queryString = "SELECT * FROM correduria WHERE codigo = ?";
			$query = $this->db->query($queryString,array($codigoCorreduria));

			$row = $query->row();
			return $row;

		}	

		public function get_parent_correduria($idCorreduria)
		{
			$queryString = "SELECT * FROM correduria WHERE id = ?";
			$query = $this->db->query($queryString,array($idCorreduria));

			$row = $query->row();
			return $row;			

		}

		public function nueva_tarificacion_formulario($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$codigoReferencia,$cadenaJson)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('FRM'));

			$row = $query->row_array();
			$ramo_id = $row['id'];	

			$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $cadenaJson));

			$id = $this->db->insert_id(); 			

			return array("id" => $id, "success" => $query, "queryString" => $queryString);					

		}

		public function nueva_tarificacion_alquiler($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$valorAlquiler,$primas,$codigoReferencia)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('ALQ'));

			$row = $query->row_array();
			$ramo_id = $row['id'];		



			if((is_numeric($valorAlquiler)) === FALSE) {
				return array("success" => 0, "queryString" => "El campo del valor del alquiler no es numerico");
			}else{


				$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data,alquiler_valorAlquiler) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

				$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $primas, $valorAlquiler));

				$id = $this->db->insert_id(); 			

				return array("id" => $id, "success" => $query, "queryString" => $queryString);	
			}	
		}


		public function nueva_tarificacion_especial($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$primas,$codigoReferencia)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('ESP'));

			$row = $query->row_array();
			$ramo_id = $row['id'];		

			$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $primas));	
			
			$id = $this->db->insert_id(); 			

			return array("id" => $id, "success" => $query, "queryString" => $queryString);						


		}

		public function nueva_tarificacion_salud($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$numPersonas,$provincia_id,$saludContratanteId,$edades,$primas,$codigoReferencia)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('SAL'));

			$row = $query->row_array();
			$ramo_id = $row['id'];		

			if((is_numeric($numPersonas)) === FALSE) {
				return array("success" => 0, "queryString" => "El campo del numero de personas no es numerico");
			}else{


				$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data,numeroPersonas,salud_provincia_id,salud_contratante_id,salud_edad1,salud_edad2,salud_edad3,salud_edad4,salud_edad5,salud_edad6,salud_edad7,salud_edad8,salud_edad9,salud_edad10) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $primas, $numPersonas, $provincia_id, $saludContratanteId, $edades[1], $edades[2], $edades[3], $edades[4], $edades[5], $edades[6], $edades[7], $edades[8], $edades[9], $edades[10]));

				$id = $this->db->insert_id(); 			

				return array("id" => $id, "success" => $query, "queryString" => $queryString);	
			}	
		}		

		public function nueva_tarificacion_vida($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$capital,$edad,$primas,$codigoReferencia)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('VID'));

			$row = $query->row_array();
			$ramo_id = $row['id'];		

			if((is_numeric($capital)) === FALSE) {
				return array("success" => 0, "queryString" => "El campo capital no es numerico");
			}else{


				$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data,vida_capital,vida_edad) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $primas, $capital, $edad));

				$id = $this->db->insert_id(); 			

				return array("id" => $id, "success" => $query, "queryString" => $queryString);	
			}	
		}


		public function nueva_tarificacion_decesos($user_id,$correduria_id,$nombreApellidos,$email,$telefono,$numPersonas,$provinciaId,$edades,$primas,$codigoReferencia)
		{
			// get ramo_id
			$queryString = "SELECT id FROM ramo_seguros WHERE codigo = ?";
			$query = $this->db->query($queryString, array('DCS'));

			$row = $query->row_array();
			$ramo_id = $row['id'];		

			if((is_numeric($numPersonas)) === FALSE) {
				return array("success" => 0, "queryString" => "El campo del numero de personas no es numerico");
			}else{


				$queryString = "INSERT INTO tarificaciones (correduria_id,ramo_id,codigo_tarificacion,creada_por,nombre_completo,telefono,email,data,numeroPersonas,decesos_provincia_id_1,decesos_edad1,decesos_edad2,decesos_edad3,decesos_edad4,decesos_edad5,decesos_edad6,decesos_edad7,decesos_edad8,decesos_edad9,decesos_edad10) 
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				$query = $this->db->query($queryString, array($correduria_id, $ramo_id, $codigoReferencia, $user_id, $nombreApellidos, $telefono, $email, $primas, $numPersonas, $provinciaId, $edades[1], $edades[2], $edades[3], $edades[4], $edades[5], $edades[6], $edades[7], $edades[8], $edades[9], $edades[10]));

				$id = $this->db->insert_id(); 			

				return array("id" => $id, "success" => $query, "queryString" => $queryString);	
			}	
		}			

		public function getDatosTarificacion($id)
		{
			$queryString = "SELECT * FROM tarificaciones WHERE id = ?";

			$query = $this->db->query($queryString,array($id));

			return $query;

		}

		public function getCorreduriasAlquiler(){

			$queryString = "SELECT DISTINCT c.id, c.codigo, c.nombre FROM correduria c, alquiler_producto_companyia_seguros_correduria apcsc
							WHERE apcsc.id_correduria = c.id and apcsc.activo = '1'";

			$query = $this->db->query($queryString);

			return $query->result();							
		}

		public function getCorreduriasVida(){

			$queryString = "SELECT DISTINCT c.id, c.codigo, c.nombre FROM correduria c, vida_producto_companyia_seguros_correduria apcsc
							WHERE apcsc.id_correduria = c.id and apcsc.activo = '1'";

			$query = $this->db->query($queryString);

			return $query->result();							
		}		

		public function getCorreduriasSalud(){

			$queryString = "SELECT DISTINCT c.id, c.codigo, c.nombre FROM correduria c, salud_producto_companyia_seguros_correduria spcsc
							WHERE spcsc.id_correduria = c.id and spcsc.activo = '1'";

			$query = $this->db->query($queryString);

			return $query->result();							
		}	

		public function getListadoAnotaciones($tarif_id)
		{
			$queryString = "SELECT DATE_FORMAT(fecha_creacion,'%d/%m/%Y %H:%i:%s') as 'Fecha de creación', contenido as 'Texto' FROM mensajes WHERE tarificacion_id = ? AND mensaje_tipo_id = 2";

			$query = $this->db->query($queryString,array($tarif_id));



			return $query;	
		}

		public function getListadoTodasAnotaciones($correduria_id,$ramo_id,$inid,$ordenar){


			if ($inid != "orig")
				$fechaini = " AND m.fecha_creacion >='" . $inid . "' ";
			else
				$fechaini= "";


			if ($ordenar === '0')
				$sort = "m.fecha_creacion DESC";
			elseif ($ordenar === '1')
				$sort = "m.fecha_creacion";
			else 
				$sort = "t.codigo_tarificacion";

			
			$queryString ="SELECT t.codigo_tarificacion as 'Referencia', DATE_FORMAT(m.fecha_creacion,'%d/%m/%Y %H:%i:%s') as 'Fecha de creación', CONCAT(u.first_name ,' ', u.last_name) as 'Creada por', m.contenido as 'Texto' 
						FROM mensajes m, tarificaciones t, users u, users_groups ug, groups_correduria gc
						WHERE m.owner_id= ug.user_id
						AND ug.group_id = gc.group_id 
						AND gc.correduria_id = ?
						AND m.mensaje_tipo_id = 2 
						AND m.tarificacion_id = t.id
						AND m.owner_id = u.id
						AND t.ramo_id = ?" . $fechaini . " ORDER BY " . $sort;

			$query = $this->db->query($queryString,array($correduria_id,$ramo_id));

			//log_message('debug', $queryString);

			return $query;	
		}


		public function getListadoTarificaciones($correduria_id,$ramo_id,$inid,$filter = NULL){

			// filter: 
			// primer bit = filtro contratada
			// segundo bit = filtro documentacion enviada
			// Si bit = 0 -> valor false
			// Si bit = 1 -> valor true
			// Si bit = 2 -> no filtrar

			if ($inid != "orig")
				$fechaini = " AND fecha_creacion >='" . $inid . "' ";
			else
				$fechaini= "";

			$filtrosc = "";
			switch ($filter[0]) {
			    case "0":
			        $filtrosc =  " AND contratada = 0 ";
			        break;
			    case "1":
			        $filtrosc =  " AND contratada = 1 ";
			        break;
			    default:
			    	$filtrosc = " ";
			    	break;
			}

		$filtrosd = "";
			switch ($filter[1]) {
			    case "0":
			        $filtrosd =  " AND documentacionEnviada = 0 ";
			        break;
			    case "1":
			        $filtrosd =  " AND documentacionEnviada = 1 ";
			        break;
			    default:
			    	$filtrosd = " ";
			    	break;
			}			


			$queryString ="SELECT codigo_tarificacion as Referencia, DATE_FORMAT(fecha_creacion,'%d/%m/%Y %H:%i:%s') as 'Fecha de tarificación', nombre_completo as 'Nombre y apellidos', telefono as 'Teléfono', email as Email, if(documentacionEnviada, 'si', 'no') as 'Documentación enviada', if(contratada, 'si', 'no') as 'Póliza contratada' FROM tarificaciones WHERE correduria_id = ? AND ramo_id = ?" . $filtrosc . $filtrosd . $fechaini . " order by id desc";
			$query = $this->db->query($queryString,array($correduria_id,$ramo_id));

			//log_message('debug', $queryString);

			return $query;	
		}	
	
}
