-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Servidor: mysql5
-- Tiempo de generación: 08-02-2017 a las 11:43:36
-- Versión del servidor: 5.1.56
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tarifonline_tarificador`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `salud_vista_descuentos_recargos`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`tarifonline`@`10.10.1.%` SQL SECURITY DEFINER VIEW `salud_vista_descuentos_recargos` AS select `sdr`.`id` AS `id`,`sdr`.`id_producto_salud` AS `id_producto_salud`,`c`.`codigo` AS `correduria`,`cs`.`nombre` AS `companyia`,`spcsc`.`nombre` AS `nombre_producto`,`sdr`.`cm_re` AS `cm_re`,`sdr`.`id_forma_pago` AS `id_forma_pago`,`rnp`.`nombre` AS `rango_personas`,`sdr`.`edad_minima` AS `edad_minima`,`sdr`.`edad_maxima` AS `edad_maxima`,`sdr`.`valor` AS `valor`,`sdr`.`orden` AS `orden`,`sdr`.`activo` AS `activo`,`pc`.`nombre` AS `porcentaje_o_cantidad`,`dr`.`nombre` AS `descuento_o_recargo` from ((((((`salud_descuentos_recargos` `sdr` join `salud_producto_companyia_seguros_correduria` `spcsc`) join `correduria` `c`) join `companyia_seguros` `cs`) join `porcentaje_cantidad` `pc`) join `descuentos_recargos` `dr`) join `rango_numero_personas` `rnp`) where ((`sdr`.`id_producto_salud` = `spcsc`.`id`) and (`spcsc`.`id_correduria` = `c`.`id`) and (`spcsc`.`id_cia` = `cs`.`id`) and (`sdr`.`id_tipo_valor` = `pc`.`id`) and (`sdr`.`id_tipo_operacion` = `dr`.`id`) and (`sdr`.`id_rango_numero_personas` = `rnp`.`id`));

--
-- VIEW  `salud_vista_descuentos_recargos`
-- Datos: Ninguna
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
