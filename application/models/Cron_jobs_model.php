<?php

class Cron_jobs_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}    	

	public function get_anotaciones_diarias() {

		$queryString = "SELECT DISTINCT u.id, u.first_name, u.last_name, u.email
						FROM users u, mensajes m
						WHERE m.owner_id = u.id
						AND fecha_recordatorio = DATE(NOW())";

		$query = $this->db->query($queryString);

		return $query;		
   }

	public function getNumeroMensajesPendientes($userId){

		$queryString = "SELECT count(*) as cantidad FROM mensajes WHERE owner_id = ? AND fecha_recordatorio = DATE(NOW())";
		$query = $this->db->query($queryString,array($userId));
		return $query->row();
	}  

	public function getCodigosReferenciaPendientes($userId){

		$queryString = "SELECT t.codigo_tarificacion FROM mensajes m,tarificaciones t WHERE m.owner_id = ? AND m.fecha_recordatorio = DATE(NOW()) and m.tarificacion_id = t.id";
		$query = $this->db->query($queryString,array($userId));
		return $query;		
	} 
}
?>