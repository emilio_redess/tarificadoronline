<?php

    class Datatables_especiales extends CI_Model implements DatatableModel{
    	
		/**
		 * @ return
		 * 		Expressions / Columns to append to the select created by the Datatable library
		 */
		public function appendToSelectStr() {
			//_protect_identifiers needs to be FALSE in the database.php when using custom expresions to avoid db errors.
			//CI is putting `` around the expression instead of just the column names
				return array(
					//'contactNameFull' => 'concat(t.nombre, \' \', t.apellidos)'
					'contactNameTruncated' => 'concat(SUBSTRING(t.nombre_completo,1, 15), \'...\')',
					'contactEmailTruncated' => 'concat(SUBSTRING(t.email,1, 13), \'...\')',
					'contactNameFull' => 't.nombre_completo'
				);
		}
    	
		public function fromTableStr() {
			return 'tarificaciones t';
		}
    
	    /**
	     * @return
	     *     Associative array of joins.  Return NULL or empty array  when not joining
	     */
	    public function joinArray(){
	    	return array(
	    		'correduria c' => 't.correduria_id = c.id',
	    		'groups_correduria gc' => 't.correduria_id = gc.correduria_id',
	    		'users_groups ug' => 'ug.group_id = gc.group_id',
	    		'users u' => 'u.id = ug.user_id',
	    		'ramo_seguros r' => 't.ramo_id = r.id'
	    		
			);
	    }
 	    
	    
    /**
     * 
     *@return
     *  Static where clause to be appended to all search queries.  Return NULL or empty array
     * when not filtering by additional criteria
     */
    	public function whereClauseArray(){
            return array(
                'r.id' => '7',
                't.activo' => '1',
                'u.id' => $this -> ion_auth -> get_user_id()
                );
        }

   }
?>