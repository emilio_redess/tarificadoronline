<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



/*
|--------------------------------------------------------------------------
| Panel de administracion del tarificador
|--------------------------------------------------------------------------
*/
define("URL_TARIFICADOR", "http://www.tarificadoronline.com");

define("URL_CURL", "http://www.tarificadoronline.com/index.php/ws-api/");
//define("URL_CURL", "http://www.base30.es/tarificador/index.php/ws-api/");

define("TITLE_CREAR_PRODUCTO_RESUMEN", "Resumen del proceso");
define("TITLE_CREAR_PRODUCTO_SALUD_PASO_1", "Paso 1 - Crear producto de salud");

define("TITLE_CREAR_PRODUCTO_DECESOS_PASO_1", "Paso 1 - Crear producto de decesos");




define("TITLE_CREAR_PRODUCTO_PASO_2", "Paso 2 - Asignar producto a correduría");
define("TITLE_CREAR_PRODUCTO_PASO_3", "Paso 3 - Añadir las tasas al producto");
define("TITLE_CREAR_PRODUCTO_PASO_4", "Paso 4 - Añadir impuestos y recargos");
define("TITLE_CREAR_PRODUCTO_PASO_5", "Paso 5 - Activación del producto");


define("TITLE_CREAR_PRODUCTO_ASIGNAR_CORREDURIA", "Asignar producto a correduría");
define("TITLE_CREAR_PRODUCTO_ADD_TASAS", "Añadir las tasas al producto");
define("TITLE_CREAR_PRODUCTO_ADD_CAPITALES_PROVINCIA", "Añadir capitales de provincia");
define("TITLE_CREAR_PRODUCTO_ADD_IMPUESTOS_RECARGOS", "Añadir impuestos y recargos");
define("TITLE_CREAR_PRODUCTO_ACTIVACION_PRODUCTO", "Activación del producto");


define("TITLE_DETALLES_PRODUCTO", "Detalles del producto");
define("CODIGO_TARIFICACION_TIPO", 'numeric');
define("CODIGO_TARIFICACION_LONGITUD", 10);


define("EMAIL_PROTOCOL", "smtp");
define("EMAIL_SMTP_PORT", 25);


define("EMAIL_SMTP_HOST", "smtp.redess.es");
define("EMAIL_SMTP_USER", "sistema@redess.es");
define("EMAIL_SMTP_PASSWORD", "asdf+1234");

define("EMAIL_SMTP_TIMEOUT", "4");
define("EMAIL_SMTP_MAILTYPE", "html");
define("EMAIL_SMTP_CHARSET", "utf-8");

define("TELEFONO_CONTACTO_REDESS1", "615 32 57 59");
define("EMAIL_CONTACTO_REDESS1", "info@redess.es");
define("TELEFONO_CONTACTO_REDESS2", "");
define("TELEFONO_CONTACTO_REDESS3", "");

define("EMAIL_RECIPIENTE", "emilio@redess.es");
define("EMAIL_CC", "");
define("EMAIL_BCC", "");

define("EMAIL_FROM", "sistema@redess.es");
define("EMAIL_FROMNAME", "TARIFICADOR REDESS");





/*
|--------------------------------------------------------------------------
| Vista de tarificaciones
|--------------------------------------------------------------------------
*/
define("TITLE_LISTADO_TARIFICACIONES_0","Listado de tarificaciones de la papelera de reciclaje");
define("TITLE_LISTADO_TARIFICACIONES_99","Listado de tarificaciones de todos los ramos");
define("TITLE_LISTADO_TARIFICACIONES_1","Listado de tarificaciones de Vida");
define("TITLE_LISTADO_TARIFICACIONES_2","Listado de tarificaciones de Salud");
define("TITLE_LISTADO_TARIFICACIONES_3","Listado de tarificaciones de Decesos");
define("TITLE_LISTADO_TARIFICACIONES_4","Listado de tarificaciones de Hogar");
define("TITLE_LISTADO_TARIFICACIONES_5","Listado de tarificaciones de Protección de Alquiler");
define("TITLE_LISTADO_TARIFICACIONES_6","Listado de tarificaciones de Formulario de contacto");
define("TITLE_LISTADO_TARIFICACIONES_7","Listado de tarificaciones de Productos especiales");
define("TITLE_LISTADO_TARIFICACIONES_8","Listado de tarificaciones de ------- ");
define("TITLE_LISTADO_TARIFICACIONES_9","Listado de tarificaciones de ------- ");
define("TITLE_LISTADO_TARIFICACIONES_10","Listado de tarificaciones de ------- ");

