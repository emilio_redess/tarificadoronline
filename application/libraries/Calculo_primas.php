<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase para el calculo de las primas de los distintos ramos de seguros
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Calculo_primas {
    public function __construct()
    {
        $this->load->model('admin/funciones_comunes_model');
    }    

    /**
     * __get
     *
     * Enables the use of CI super-global without having to define an extra variable.
     *
     * I can't remember where I first saw this, so thank you if you are the original author. -Militis
     *
     * @access  public
     * @param   $var
     * @return  mixed
     */
    public function __get($var)
    {
        return get_instance()->$var;
    }       

   /**
     * Funcion que recibe el codigo de la correduria y la renta mensual.
     * La funcion una cadena JSON con las primas de los productos que tenga asociados la correduria.
     *
     * @access  public
     * @param   codigoCorreduria    cadena identificativa de la correduria
     * @param   rentaMensual  integer   La renta mensual sobre la que se va a calcular la prima
     * @return  string
     */
    public function calcular_primas_alquiler($productos, $valorAlquiler,$impuestos_recargos,$tipoLocalCodigo)
    {
        // Formula: valorAlquiler * 12 * factorCalculo

        
        $arr = array();

        $arr["primas"] = array();


        // Se obtiene el nombre del tipo de local a partir del codigo
        $this->load->model('admin/alquiler_model');
        $tipoLocalNombre = $this->alquiler_model->get_tipo_local_nombre($tipoLocalCodigo);

        $arr["Tipo local"] = $tipoLocalNombre->nombre;


        foreach ($productos->result_array() as $row)
        {
            $id_producto_correduria = $row['id_producto_correduria'];
            $id =  $row['id'];
           // $factorCalculo = $row['valor'];

            $factorCalculo_6m = $row['6meses'];
            $factorCalculo_9m = $row['9meses'];
            $factorCalculo_12m = $row['12meses'];
            $factorCalculo_18m = $row['18meses'];

           // $prima = (float)$valorAlquiler * 12 * ((float)$factorCalculo/100);

            $prima_6meses = (float)$valorAlquiler * 12 * ((float)$factorCalculo_6m/100);
            $prima_9meses = (float)$valorAlquiler * 12 * ((float)$factorCalculo_9m/100);
            $prima_12meses = (float)$valorAlquiler * 12 * ((float)$factorCalculo_12m/100);
            $prima_18meses = (float)$valorAlquiler * 12 * ((float)$factorCalculo_18m/100);

            // Ahora se calcula la prima final aplicando a la prima neta los posibles descuentos o impuestos/recargos
            foreach ($impuestos_recargos[$id]->result_array() as $row2)
            {
                switch($row2['id_tipo_valor']) {
                    case '1':  // porcentaje
                        if ($row2['id_tipo_operacion'] == '1') { // descuento
                            //$prima = $prima - ($prima*$row2['valor']/100);

                            $prima_6meses = $prima_6meses - ($prima_6meses*$row2['valor']/100);
                            $prima_9meses = $prima_9meses - ($prima_9meses*$row2['valor']/100);
                            $prima_12meses = $prima_12meses - ($prima_12meses*$row2['valor']/100);
                            $prima_18meses = $prima_18meses - ($prima_18meses*$row2['valor']/100);
                        }elseif ($row2['id_tipo_operacion'] == '2') { // suplemento
                           // $prima = $prima + ($prima*$row2['valor']/100);

                            $prima_6meses = $prima_6meses + ($prima_6meses*$row2['valor']/100);
                            $prima_9meses = $prima_9meses + ($prima_9meses*$row2['valor']/100);
                            $prima_12meses = $prima_12meses + ($prima_12meses*$row2['valor']/100);
                            $prima_18meses = $prima_18meses + ($prima_18meses*$row2['valor']/100);
                        }

                    break;

                    case '2':  // cantidad fija
                        if ($row2['id_tipo_operacion'] == '1') { // descuento
                           // $prima = $prima - $row2['valor'];

                            $prima_6meses = $prima_6meses - $row2['valor'];
                            $prima_9meses = $prima_9meses - $row2['valor'];
                            $prima_12meses = $prima_12meses - $row2['valor'];
                            $prima_18meses = $prima_18meses - $row2['valor'];
                        }elseif ($row2['id_tipo_operacion'] == '2') {  // suplemento
                           // $prima = $prima + $row2['valor'];

                            $prima_6meses = $prima_6meses + $row2['valor'];
                            $prima_9meses = $prima_9meses + $row2['valor'];
                            $prima_12meses = $prima_12meses + $row2['valor'];
                            $prima_18meses = $prima_18meses + $row2['valor'];
                        }                    

                    break;
                }
            }

            if (is_null($row['6meses'])) $prima_6meses = NULL;
            if (is_null($row['9meses'])) $prima_9meses = NULL;
            if (is_null($row['12meses'])) $prima_12meses = NULL;
            if (is_null($row['18meses'])) $prima_18meses = NULL;


            $arr["primas"][$id_producto_correduria] = array("id" => $id, "prima_6m" => $prima_6meses, "prima_9m" => $prima_9meses, "prima_12m" => $prima_12meses, "prima_18m" => $prima_18meses);
            
        }

        $cadenaJson = json_encode($arr);
        
        //return $cadenaJson;
        return array("result" => $cadenaJson, "success" => TRUE); 
    }

    public function calcular_primas_salud($productos,$numPersonas,$edades,$impuestos_recargos,$provinciaPrefijo)
    {
        $arr = array();
        foreach ($productos->result_array() as $row)
        {
            $id = $row['id'];
            $id_cm =  $row['id_cm'];
            $id_re =  $row['id_re'];

            $cm_temp = 0;
            $re_temp = 0;
            for ($i = 1; $i <= $numPersonas; $i++) {
                if (!is_null($id_cm)){
                    $cm = $this->funciones_comunes_model->get_prima_salud($edades[$i],$id_cm,$provinciaPrefijo);

                    if (!is_null($cm))
                        $cm_temp = $cm_temp + $cm->tasa;
                }
                if (!is_null($id_re)){
                    $re = $this->funciones_comunes_model->get_prima_salud($edades[$i],$id_re,$provinciaPrefijo);
                    if (!is_null($re))
                        $re_temp = $re_temp + $re->tasa;     
                }            
            }

            if (($cm_temp != 0) || ($re_temp != 0))   // si ambas son 0, significa que para esa provincia, ese producto no tiene tasas. En cuyo caso, no se guarda nada de ese producto en la tarificacion.
            {
                // Ahora se calcula la prima final aplicando a la prima neta los posibles descuentos o impuestos/recargos
                foreach ($impuestos_recargos[$id]->result_array() as $row2)
                {
                    switch($row2['id_tipo_valor']) {
                        case '1':  // porcentaje
                            if ($row2['id_tipo_operacion'] == '1') { // descuento

                                if ($row2['cm_re'] == '0')
                                    $cm_temp = $cm_temp - ($cm_temp*$row2['valor']/100);
                                elseif ($row2['cm_re'] == '1')
                                    $re_temp = $re_temp - ($re_temp*$row2['valor']/100);
                                else {
                                    $cm_temp = $cm_temp - ($cm_temp*$row2['valor']/100);
                                    $re_temp = $re_temp - ($re_temp*$row2['valor']/100);
                                }
                            }elseif ($row2['id_tipo_operacion'] == '2') { // suplemento

                                if ($row2['cm_re'] == '0')
                                    $cm_temp = $cm_temp + ($cm_temp*$row2['valor']/100);
                                elseif ($row2['cm_re'] == '1')
                                    $re_temp = $re_temp + ($re_temp*$row2['valor']/100);
                                else {
                                    $cm_temp = $cm_temp + ($cm_temp*$row2['valor']/100);
                                    $re_temp = $re_temp + ($re_temp*$row2['valor']/100);
                                };
                            }

                        break;

                        case '2':  // cantidad fija
                            if ($row2['id_tipo_operacion'] == '1') { // descuento

                                if ($row2['cm_re'] == '0')
                                    $cm_temp = $cm_temp - $row2['valor'];
                                elseif ($row2['cm_re'] == '1')
                                    $re_temp = $re_temp - $row2['valor'];
                                else {
                                    $cm_temp = $cm_temp - $row2['valor'];
                                    $re_temp = $re_temp - $row2['valor'];
                                }

                            }elseif ($row2['id_tipo_operacion'] == '2') {  // suplemento
                                if ($row2['cm_re'] == '0')
                                    $cm_temp = $cm_temp + $row2['valor'];
                                elseif ($row2['cm_re'] == '1')
                                    $re_temp = $re_temp + $row2['valor'];
                                else {
                                    $cm_temp = $cm_temp + $row2['valor'];
                                    $re_temp = $re_temp + $row2['valor'];
                                }
                            }                    

                        break;
                    }
                }     

                $arr[$id] = array("id_cm" => $id_cm, "prima_cm" => $cm_temp, "id_re" => $id_re, "prima_re" => $re_temp);
            }
        }
       
        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }



    public function calcular_primas_vida($productos,$capital,$edad,$impuestos_recargos)
    {

        $arr = array();
        foreach ($productos->result_array() as $row)
        {
            $id_producto_correduria = $row['id_producto_correduria'];
            $id =  $row['id'];



            // Se calcula el valor del impuesto del consorcio
            $imp_consorcio = 0;
            if ($row['consorcio_incluido'] == FALSE){
                $imp_consorcio = ($capital * 5)/1000000;
                $imp_consorcio2 = $imp_consorcio*2;
                $imp_consorcio3 = $imp_consorcio*3;
            }


            $tasas = $this->funciones_comunes_model->get_prima_vida($edad,$capital,$id);

            if (!is_null($tasas))
            {

                // impuestos o recargos del producto
                $imp_rec = 0;


                $tasa_f = $tasas->tasa_f;
                $tasa_i = $tasas->tasa_i;
                $tasa_ia = $tasas->tasa_ia;
                $tasa_fa = $tasas->tasa_fa;
                $tasa_iac = $tasas->tasa_iac;
                $tasa_fac = $tasas->tasa_fac;



                $prima_neta_f = ($capital/1000) * $tasa_f;
                $prima_neta_i = ($capital/1000) * $tasa_i;



                foreach ($impuestos_recargos[$id]->result_array() as $row2)
                {
                    switch($row2['id_tipo_valor']) {
                        case '1':  // porcentaje
                            if ($row2['id_tipo_operacion'] == '1') { // descuento

                                $prima_neta_f = $prima_neta_f - ($prima_neta_f*$row2['valor']/100);
                                $prima_neta_i = $prima_neta_i - ($prima_neta_i*$row2['valor']/100);
                              
                            }elseif ($row2['id_tipo_operacion'] == '2') { // recargo
                             
                                $prima_neta_f = $prima_neta_f + ($prima_neta_f*$row2['valor']/100);
                                $prima_neta_i = $prima_neta_i + ($prima_neta_i*$row2['valor']/100);                            
                            }

                        break;

                        case '2':  // cantidad fija
                            if ($row2['id_tipo_operacion'] == '1') { // descuento
                            
                                $prima_neta_f = $prima_neta_f - $row2['valor'];
                             
                            }elseif ($row2['id_tipo_operacion'] == '2') {  // recargo
                             
                                $prima_neta_f = $prima_neta_f + $row2['valor'];
                             
                            }                    

                        break;
                    }
                }


                // prima final de fallecimiento
                $prima_f = $prima_neta_f - $imp_rec + $imp_consorcio;


                // prima auxiliar de invalidez a falta de sumarle los impuestos clea e isps
                $prima_i_aux = $prima_neta_i - $imp_rec;

                // impuestos clea y isps
                $clea = 0;
                $isps = 0;

                if ($row['clea_incluido'] == FALSE)
                    $clea = ($prima_i_aux * 1.5)/ 1000;

                if ($row['isps_incluido'] == FALSE)
                    $isps = ($prima_i_aux * 6)/ 100;  

                // prima final de invalidez
                $prima_i =  $prima_i_aux + $clea + $isps;

                // prima final de fallecimiento + invalidez
                $prima_fi = $prima_f + $prima_i;


                // TODO : calcular doble y triple capital


                // Doble capital
                $tasa_doble = $tasa_ia + $tasa_fa;
                $ia = ($capital/1000)* $tasa_doble;
                $clea_doble = ($ia/1000)*1.5;
                $isps_doble = ($ia/100)*6;

                $ia = $ia + $clea_doble + $isps_doble;

                $prima_doble_capital = $prima_fi + $ia + $imp_consorcio2;

                // Triple capital
                $tasa_triple = $tasa_iac + $tasa_fac;
                $iac = ($capital/1000)* $tasa_triple;
                $clea_triple = ($iac/1000)*1.5;
                $isps_triple = ($iac/100)*6;

                $iac = $iac + $clea_triple + $isps_triple;

                $prima_triple_capital = $prima_fi + $ia + $iac + $imp_consorcio3;                

            }

            $arr[$id_producto_correduria] = array("id" => $id, "prima_f" => $prima_f, "prima_i" => $prima_i, "prima_fi" => $prima_fi, "prima_doble_capital" => $prima_doble_capital, "prima_triple_capital" => $prima_triple_capital);

        }


        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }



    public function calcular_primas_decesos($productos,$numPersonas,$edades,$impuestos_recargos,$provinciaId,$overwrite_capitales,$nuevo_capital)
    {

        $arr = array();
        foreach ($productos->result_array() as $row)
        {
            $id_producto_correduria = $row['id_producto_correduria'];
            $id =  $row['id'];

            if ($overwrite_capitales)
                $capitalProvincia = $nuevo_capital;
            else
                $capitalProvincia = $this->funciones_comunes_model->get_capital_provincia($id,$provinciaId)->valor_capital; 


            $prima_temp = 0;
            for ($i = 1; $i <= $numPersonas; $i++) {
                if (!is_null($id)){                   
                    $prima = $this->funciones_comunes_model->get_prima_decesos($edades[$i],$id);

                    if ((!is_null($prima)) && (!is_null($capitalProvincia)))
                        $prima_temp = $prima_temp + ($prima->tasa * $capitalProvincia);
                }
            }



            if ($prima_temp != 0)
            {
                // Ahora se calcula la prima final aplicando a la prima neta los posibles descuentos o impuestos/recargos
                foreach ($impuestos_recargos[$id]->result_array() as $row2)
                {
                    switch($row2['id_tipo_valor']) {
                        case '1':  // porcentaje
                            if ($row2['id_tipo_operacion'] == '1') { // descuento

                              
                                $prima_temp = $prima_temp - ($prima_temp*$row2['valor']/100);
                               
                            }elseif ($row2['id_tipo_operacion'] == '2') { // suplemento

                               
                                    $prima_temp = $prima_temp + ($prima_temp*$row2['valor']/100);
                             
                            }

                        break;

                        case '2':  // cantidad fija
                            if ($row2['id_tipo_operacion'] == '1') { // descuento

                              
                                    $prima_temp = $prima_temp - $row2['valor'];
                              

                            }elseif ($row2['id_tipo_operacion'] == '2') {  // suplemento
                               
                                    $prima_temp = $prima_temp + $row2['valor'];
                               
                            }                    

                        break;
                    }
                }    

                $arr[$id_producto_correduria] = array("id" => $id, "prima" => $prima_temp, "capital_provincia" => $capitalProvincia);
            }
        }
       
        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }    

    public function calcular_primas_especial_deportesRiesgo($productos,$id_ramo_especial,$fechaIni,$fechaFin,$duracion,$residente,$residenteNombre,$zonaCobertura,$zonaCoberturaNombre,$numAseg)
    {
        $arr["ramo_especial"] = $id_ramo_especial;
        $arr["primas"] = array();


        // Vamos a obtener los datos adicionales del seguro de deportes de riesgo (duracion, lugar residencia, num aseg, etc)


        // Obtengo los datos de la tabla de duracion
        $duracion_datos = $this->funciones_comunes_model->getDatosDuracionProductoDeporteRiesgo($duracion);

        $arr["params"] = array();
        $arr["params"] += array("Duración" => $duracion_datos->nombre);
        $arr["params"] += array("Fecha inicio" => $fechaIni);
        $arr["params"] += array("Fecha fin" => $fechaFin);
        $arr["params"] += array("Duración" => $duracion_datos->nombre);
        $arr["params"] += array("Residente" => $residenteNombre);
        $arr["params"] += array("Zona de cobertura" => $zonaCoberturaNombre);
        $arr["params"] += array("Número de asegurados" => $numAseg);



        foreach ($productos->result_array() as $row)
        {
            $id =  $row['id'];


            // El precio va a depender tanto de la zona de residencia como de la zona de cobertura.
            // Si zona_residencia == España entonces zona_precio = zona_cobertura
            // Si zona_residencia != España entonces zona_precio = zona_residencia


            if ($residente === "1"){
                $zona_precio = $zonaCobertura;
            }
            else{
                $zona_precio = $residente;
            }



            // Obtengo los datos del valor de la prima por mes adicional
            $residente_datos = $this->funciones_comunes_model->getDatosResidenteProductoDeporteRiesgo($id,$zona_precio);

            // Si el valor de meses_adicionales != 0, significa  que estamos en el caso de que la duracion es mas de 31 dias, por lo que hay que sumar el valor de los meses adicionales que toque. Ademas, en este caso ponemos la variable duracion = 6 que es la id necesaria para obtener el valor de la prima a la que habra que sumar los meses adicionales.
            if ($duracion_datos->meses_adicionales != '0')
                $duracion_aux = '6';
            else
                $duracion_aux = $duracion_datos->id;



            $prima_datos = $this->funciones_comunes_model->getDatosPrimaProductoDeporteRiesgo($id,$duracion_aux,$zona_precio);

            // Se calcula la prima
            $prima = $prima_datos->valor + ($duracion_datos->meses_adicionales * $residente_datos->valor_mes_adicional);

            // Se multiplica la prima obtenida por el numero de asegurados
            $prima = $prima * $numAseg;



            $arr["primas"] += array($id => $prima);
        }
        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }

    public function calcular_primas_especial_transporte($productos,$id_ramo_especial,$limite_viaje,$garantias,$averia_frio)
    {
        $arr["ramo_especial"] = $id_ramo_especial;
        $arr["primas"] = array();
        $arr["params"] = array();

        $arr["params"] += array("Limite por viaje" => $limite_viaje);
        $arr["params"] += array("Garantías" => $garantias);
        $arr["params"] += array("Avería frío" => $averia_frio);
      
        foreach ($productos->result_array() as $row)
        {
            $id =  $row['id'];



            $prima_datos = $this->funciones_comunes_model->getDatosPrimaProductoTransporte($id,$limite_viaje,$averia_frio,$garantias);




            // Se calcula la prima
            $precio = $prima_datos->precio;
            


            $arr["limite_viaje"] += array($id => $limite_viaje);
            $arr["averia_frio"] += array($id => $averia_frio);
            $arr["garantias"] += array($id => $garantias);
            $arr["primas"] += array($id => $precio);

        }
        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }

    public function calcular_primas_especial_rctransporte($productos,$id_ramo_especial,$suma,$adr,$ambito)
    {
        $arr["ramo_especial"] = $id_ramo_especial;
        $arr["primas"] = array();
        $arr["params"] = array();

        $arr["params"] += array("Suma" => $suma);
        $arr["params"] += array("ADR" => $adr);
        

        foreach ($productos->result_array() as $row)
        {
            $id =  $row['id'];

            $prima_datos = $this->funciones_comunes_model->getDatosPrimaProductoRCTransporte($id,$suma,$adr);     


            if (($ambito) == "nacional") {
                $precio = $prima_datos->precio_nacional;
                //$franquicia = 200;
                $ambito = "Nacional";
            } else if (($ambito) == "ue") {
                $precio = $prima_datos->precio_ue;
                //$franquicia = 300;
                $ambito = "UE";
            }


            //$arr["params"] += array("Franquicia" => $franquicia);
            $arr["params"] += array("Ambito" => $ambito);
            $arr["primas"] += array($id => $precio);
        }  
        $cadenaJson = json_encode($arr);
        return array("result" => $cadenaJson, "success" => TRUE);
    }    
}
