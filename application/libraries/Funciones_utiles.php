<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase con algunas funciones comunes 
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Funciones_utiles {
	public function __construct()
	{
		$this->load->library(array('ion_auth'));
		$this->load->model('admin/funciones_comunes_model');
	}

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * I can't remember where I first saw this, so thank you if you are the original author. -Militis
	 *
	 * @access	public
	 * @param	$var
	 * @return	mixed
	 */
	public function __get($var)
	{
		return get_instance()->$var;
	}	

	public function get_group_correduria_id($user_groups) {

		if (!empty($user_groups))
		{
			$group_id = "";
			// filtraremos los grupos admin, encargado y usuarios para quedarnos con el grupo de la correduria a la que pertenece el usuario.
			foreach($user_groups as &$valor) {
				if (($valor->name != $this->ion_auth->config->item('encargados_group', 'ion_auth')) && ($valor->name != $this->ion_auth->config->item('default_group', 'ion_auth')) && ($valor->name != $this->ion_auth->config->item('admin_group', 'ion_auth')))  // Si pertenece al grupo encargados no queremos meterlo en el array				
				    	$group_id = $valor->id;
			}

			if ($group_id === "")
				if ($this->ion_auth->is_admin())  
				// Se supone que si $group_id = "" sabemos que el usuario es admin, pero para asegurarnos,
				// llamamos a is_admin()
					return "admin";
				else return null;
			else{
				return $group_id;
			}
		}else{  // $user_groups is empty
			return null;
		}		
	}

	public function get_colaboradores($groupId)
	{

		if ($groupId == "admin")
			return NULL;
		else
		{

			$colaboradores = $this->funciones_comunes_model->getColaboradores($groupId);
			return $colaboradores;
		}
	}



	public function get_ramos_correduria($groupId){
		
		$ramos_de_correduria = $this->funciones_comunes_model->getRamosFromGroupId($groupId);
		// Ahora se va a montar un array donde cada indice será el codigo del ramo
		// y el valor será true o false segun si la tiene o no.

		

		// guardamos en $aux los ramos
		$aux = array();
		foreach ($ramos_de_correduria as $row)
		{
		        array_push($aux, $row['codigo']);
		}

		$resul = array();
		$todos_ramos = $this->funciones_comunes_model->getRamos();
		foreach ($todos_ramos as $row)
		{
			if ((in_array($row->codigo, $aux)) || ($groupId === "admin")) {
				$resul[$row->codigo] = true;
			}else{
				$resul[$row->codigo] = false;	
			}
		       
		}


		return $resul;
	}

	public function formatear_decimales($valor){

		$resul = str_replace(",",".",$valor);
		
		if(is_numeric($resul))  {

			

			return $resul;

		}else
			return null;

	}

	public function get_numero_mensajes_pendientes($userId)
	{
		$resul = $this->funciones_comunes_model->getNumeroMensajesPendientes($userId);

		return $resul->cantidad;

	}
   

}
