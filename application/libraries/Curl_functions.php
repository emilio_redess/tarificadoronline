<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase para el manejo de las llamadas CURL al servidor RESTful
 *
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Curl_functions {

   /**
     * Funcion para hacer llamada CURL mediante get
     * La funcion recibe la url del servidor al que se va a hacer la llamada.
     *
     * @access  public
     * @param   url    url del servidor REST
     * @return  array
     */
    function httpGet($url)
    {
        $ch = curl_init();  
     
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
     
        $output = curl_exec($ch);
     
        if($output === false)
        {
            echo "Error Number:".curl_errno($ch)."<br>";
            echo "Error String:".curl_error($ch);
            curl_close($ch);
            return array("success" => FALSE, "output" => "Error " . curl_errno($ch) . ": " . curl_error($ch));
        }
        else{
            curl_close($ch);
            return array("success" => TRUE, "output" => $output);
        }

    }


   /**
     * Funcion para hacer llamada CURL mediante post
     * La funcion recibe la url del servidor al que se va a hacer la llamada.
     *
     * @access  public
     * @param   url    url del servidor REST
     * @param   params    variable que contiene los parametros que hay que enviar al servidor 
     * @return  array
     */
    function httpPost($url,$params)
    {
      $postData = '';
       //create name value pairs separated by &
       foreach($params as $k => $v) 
       { 
          $postData .= $k . '='.$v.'&'; 
       }
       $postData = rtrim($postData, '&');
     
        $ch = curl_init();  
     
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
     
        $output=curl_exec($ch);

        if($output === false)
        {
            echo "Error Number:".curl_errno($ch)."<br>";
            echo "Error String:".curl_error($ch);
            curl_close($ch);
            return array("success" => FALSE, "output" => "Error " . curl_errno($ch) . ": " . curl_error($ch));
        }
        else{
            curl_close($ch);
            return array("success" => TRUE, "output" => $output);
        }
     
    }



}