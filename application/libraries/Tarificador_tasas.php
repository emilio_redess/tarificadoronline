<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter clase para el tratamiento de tasas
 *
 * Esta libreria nos ayudará a generar las consultas mysql para la inserción o modificación de tasas para el tarificador
 * 
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Emilio Valls
 */

class Tarificador_tasas {

    private $handle = "";
    private $filepath = FALSE;
    private $column_headers = FALSE;
    private $initial_line = 0;
    private $delimiter = ",";
    private $detect_line_endings = FALSE;

   /**
     * Funcion que recibo los id de producto, zona y tipo y un array con las edades y sus tasas correspondientes. 
     * LA funcion devolverá una cadena con la consulta mysql para insertar en la tabla de tasas de salud.
     *
     * @access  public
     * @param   idProducto        string  id del producto de la compañia seleccionada previamente
     * @param   idZona  string   id de la zona de provincias seleccionada previamente
     * @param   idTipo  string  id del tipo de producto seleccionado previamente
     * @param   csv_arr  array  Array indexado que contiene los campos leidos de un archivo CSV que contiene las edades y sus precios correspondientes
     * @return  array
     */
    public function generar_tasas_salud($idProducto, $file_name, $idZona)
    {
        $success = TRUE;
        // Se parsea el contenido del archivo csv a un array
        $csv_arr = array_map('str_getcsv', file('public/uploads/' . $file_name));   

        // Primero generamos la query que va a borrar las tasas que tenga este producto, en el caso de que las tenga
        $query_borrar_tasas_antiguas = "DELETE FROM salud_producto_companyia_seguros_tasas WHERE id_producto = " . $idProducto;     

        $result = "INSERT INTO salud_producto_companyia_seguros_tasas (id_producto,id_zona,edad,tasa) VALUES ";


        $numItems = count($csv_arr);
        if ($numItems < 1)
            $success = FALSE;
        $i = 0;
        foreach ($csv_arr as $valor) {
            if ((isset($valor[0])) && (isset($valor[1]))) {
                $edad = $valor[0];
                $tasa = $valor[1];

                if((is_numeric($edad)) && (is_numeric($tasa))) {

                    if(++$i === $numItems) { // ultimo elemento del array
                        $result.= "(" . $idProducto . "," . $idZona . "," . $edad . "," . $tasa . ");";
                    }
                    else{
                        $result.= "(" . $idProducto . "," . $idZona . "," . $edad . "," . $tasa . "),";
                    }                      
                }else{
                   $success = FALSE; 
                }
            }else{
                $success = FALSE;
            }
        }

        return array("queryString" => $result, "queryString_borrar_tasas_antiguas" => $query_borrar_tasas_antiguas, "success" => $success); 
    }

    public function generar_tasas_decesos($idProducto, $file_name)
    {
        $success = TRUE;
        // Se parsea el contenido del archivo csv a un array
        $csv_arr = array_map('str_getcsv', file('public/uploads/' . $file_name));   

        // Primero generamos la query que va a borrar las tasas que tenga este producto, en el caso de que las tenga
        $query_borrar_tasas_antiguas = "DELETE FROM decesos_producto_companyia_seguros_tasas WHERE id_producto = " . $idProducto;     

        $result = "INSERT INTO decesos_producto_companyia_seguros_tasas (id_producto,edad,tasa) VALUES ";


        $numItems = count($csv_arr);
        if ($numItems < 1)
            $success = FALSE;
        $i = 0;
        foreach ($csv_arr as $valor) {
            if ((isset($valor[0])) && (isset($valor[1]))) {
                $edad = $valor[0];
                $tasa = $valor[1];

                if((is_numeric($edad)) && (is_numeric($tasa))) {

                    if(++$i === $numItems) { // ultimo elemento del array
                        $result.= "(" . $idProducto . "," . $edad . "," . $tasa . ");";
                    }
                    else{
                        $result.= "(" . $idProducto . "," . $edad . "," . $tasa . "),";
                    }                      
                }else{
                   $success = FALSE; 
                }
            }else{
                $success = FALSE;
            }
        }

        return array("queryString" => $result, "queryString_borrar_tasas_antiguas" => $query_borrar_tasas_antiguas, "success" => $success); 
    } 

    public function generar_capitales_decesos($idProducto, $file_name)
    {
        $success = TRUE;
        // Se parsea el contenido del archivo csv a un array
        $csv_arr = array_map('str_getcsv', file('public/uploads/' . $file_name));   

        // Primero generamos la query que va a borrar los capitales que tenga este producto, en el caso de que las tenga
        $query_borrar_capitales_antiguos = "DELETE FROM decesos_capitales_provincia WHERE id_producto = " . $idProducto;     

        $result = "INSERT INTO decesos_capitales_provincia (id_producto,id_provincia,valor_capital) VALUES ";


        $numItems = count($csv_arr);
        if ($numItems < 1)
            $success = FALSE;
        $i = 0;
        foreach ($csv_arr as $valor) {
            if ((isset($valor[0])) && (isset($valor[2]))) {
                $id_provincia = $valor[0];
                $capital = $valor[2];

                if((is_numeric($id_provincia)) && (is_numeric($capital))) {

                    if(++$i === $numItems) { // ultimo elemento del array
                        $result.= "(" . $idProducto . "," . $id_provincia . "," . $capital . ");";
                    }
                    else{
                        $result.= "(" . $idProducto . "," . $id_provincia . "," . $capital . "),";
                    }                      
                }else{
                   $success = FALSE; 
                }
            }else{
                $success = FALSE;
            }
        }

        return array("queryString" => $result, "query_borrar_capitales_antiguos" => $query_borrar_capitales_antiguos, "success" => $success); 
    }        



    public function configurar_file_upload(){
        // File upload configuration
        $config['upload_path'] = './public/uploads/';
        //$config['allowed_types'] = 'text/comma-separated-values|application/csv|application/excel|application/vnd.ms-excel|application/vnd.msexcel|text/anytext|text/plain';
        $config['allowed_types'] = '*';
        $config['max_size'] = '100';
        //$config['max_width']  = '1024';
        //$config['max_height']  = '768';

        return $config;
        
    }
    
 
}
