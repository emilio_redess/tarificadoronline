<!--<header class="codrops-header">
	<h1>Anuncio</h1>
</header>
-->

<?php 
	$date = date("d-m-Y", strtotime($mensaje->fecha_creacion));
?>

<div class="h-timeline-row">
	<div class="row">
		<div class="col-md-12">
			<div class="entry br-<?php echo $mensaje->color; ?>">				
				<div class="entry-content">
					<div class="meta">
						<i class="fa fa-clock-o <?php echo $mensaje->color; ?>"></i>&nbsp; <?php echo $date; ?>
					</div>
					<h4><?php echo anchor('tarificador/',$mensaje->descripcion) ?></h4>
					<p><?php echo $mensaje->contenido; ?></p>
				</div>
				<i class="fa <?php echo $mensaje->icon; ?> entry-icon bg-<?php echo $mensaje->color; ?>"></i>
			</div>
		</div>
	</div>
</div>

