<!--<header class="codrops-header">
	<h1><?php echo $title; ?></h1>
</header>
-->


<?php 
$contador = 0;
foreach ($mensajes as $mensaje) {
	$date = date("d-m-Y", strtotime($mensaje->fecha_creacion));

	if ($contador === 0){
		//echo '</div>';
		//echo '</div>';
		echo '<div class="h-timeline-row">';
		echo '<div class="row">';
	}

	echo '<div class="col-md-3 col-sm-6">';
	echo '<div class="entry br-' . $mensaje->color . '">';
	echo '<div class="entry-content">';
	echo '<div class="meta">';
	echo '<i class="fa fa-clock-o ' . $mensaje->color . '"></i>&nbsp; ' . $date;
	echo '</div>';
 
	echo '<h4>' . anchor('tarificador/anuncio/' . $mensaje->id,$mensaje->descripcion) . '</h4>';
	echo '<p>' . $mensaje->intro . '</p>';
  	echo '</div>';
  	echo '<i class="fa ' . $mensaje->icon . ' entry-icon bg-' . $mensaje->color . '"></i>';
  	echo '</div>';
  	echo '</div>';


	$contador++;
	if ($contador === 4){
		echo '</div>';
		echo '</div>';
		$contador = 0;
	}
		

}
if ($contador != 0){
	echo '</div>';
	echo '</div>';
}
?>
