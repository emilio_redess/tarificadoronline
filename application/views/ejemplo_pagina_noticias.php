<header class="codrops-header">
	<h1><?php echo $title; ?></h1>
</header>



				<div class="h-timeline-row">
					<div class="row">


						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-red">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o red"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">Started Business</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-trophy entry-icon bg-red"></i>
							</div>
						</div>


						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-green">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o green"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Client</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-user entry-icon bg-green"></i>
							</div>
						</div>


						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-lblue">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o lblue"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Own Office</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-paw entry-icon bg-lblue"></i>
							</div>
						</div>


						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-blue">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o blue"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Internation Client</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-bullhorn entry-icon bg-blue"></i>
							</div>
						</div>


						
					</div>
				</div>
				
				<!-- Time line row -->
				<div class="h-timeline-row">
					<div class="row">
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-orange">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o orange"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">Reached 25 Clients</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-rocket entry-icon bg-orange"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-yellow">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o yellow"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">Second Office</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-taxi entry-icon bg-yellow"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-purple">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o purple"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">150 Emloyees</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-car entry-icon bg-purple"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-rose">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o rose"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">50 Clients</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-tree entry-icon bg-rose"></i>
							</div>
						</div>
						
					</div>
				</div>
				
				<!-- Time line row -->
				<div class="h-timeline-row">
					<div class="row">
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-brown">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o brown"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Internation Office</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-road entry-icon bg-brown"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-red">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o red"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Warehouse</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-sliders entry-icon bg-red"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-green">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o green"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">First Super Computer</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-language entry-icon bg-green"></i>
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<!-- time line content entry -->
							<div class="entry br-lblue">
								<!-- Time line entry content -->
								<div class="entry-content">
									<!-- Time line entry meta -->
									<div class="meta">
										<i class="fa fa-clock-o lblue"></i>&nbsp; Apr 26, 2014
									</div>
									<h4><a href="#">Shutting Company</a></h4>
									<p>Nemo enim ipsam vo lu ptate qu ia v olu ptas sit as aut fugit, sed quia con sequu ntur magni eos qui ratione.</p>
								</div>
								<!-- Icon -->
								<i class="fa fa-cube entry-icon bg-lblue"></i>
							</div>
						</div>
						
					</div>
				</div>






