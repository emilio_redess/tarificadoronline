<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Tarificador online de Redess</title>

  <style type="text/css">

  ::selection { background-color: #E13300; color: white; }
  ::-moz-selection { background-color: #E13300; color: white; }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  a {
    color: #003399;
    background-color: transparent;
    font-weight: normal;
  }

  h1 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  code {
    font-family: Consolas, Monaco, Courier New, Courier, monospace;
    font-size: 12px;
    background-color: #f9f9f9;
    border: 1px solid #D0D0D0;
    color: #002166;
    display: block;
    margin: 14px 0 14px 0;
    padding: 12px 10px 12px 10px;
  }

  #body {
    margin: 0 15px 0 15px;
  }

  p.footer {
    text-align: right;
    font-size: 11px;
    border-top: 1px solid #D0D0D0;
    line-height: 32px;
    padding: 0 10px 0 10px;
    margin: 20px 0 0 0;
  }

  #container {
    margin: 10px;
    border: 1px solid #D0D0D0;
    box-shadow: 0 0 8px #D0D0D0;
  }
  #infoMessage{

    color:red;
    font-size:2em;
  }
  
  </style>
</head>
<body>

<div id="container">
  <h1>Bienvenido a REDESS 3.0</h1>

  <div id="body">
    <p>Esta página es el punto de acceso al tarificador online de REDESS</p>

    <p>Si ya dispones de usuario y contraseña puedes entrar a través del siguiente enlace:</p>    
    <code>
      <div id="infoMessage"><?php echo strtoupper($message); ?></div>

      <?php echo form_open("auth/login");?>

        <p>
          <?php echo lang('login_identity_label', 'identity');?>
          <?php echo form_input($identity);?>
        </p>

        <p>
          <?php echo lang('login_password_label', 'password');?>
          <?php echo form_input($password);?>
        </p>

        <p>
          <?php echo lang('login_remember_label', 'remember');?>
          <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
        </p>


        <p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>

      <?php echo form_close();?>

      <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>


    </code>

    <p>Si todavía no dispones de acceso al área del tarificador puedes solicitarlo mediante email a <a href="mailto:<?php echo EMAIL_CONTACTO_REDESS1; ?>?Subject=Alta%20tarificador"><?php echo EMAIL_CONTACTO_REDESS1; ?></a> o bien a través de nuestra web:</p>
    <code><a href="http://www.redess.es" target="_blank">www.redess.es</a></code>

    
  </div>

  <p class="footer">&copy; <?php echo date("Y"); ?> Redess3.0</p>
</div>

</body>
</html>










<!--
<h1><?php echo lang('login_heading');?></h1>
<p><?php echo lang('login_subheading');?></p>
-->
