<div class="col-md-4">
	<h1><?php echo lang('create_group_heading');?></h1>
	<p><?php echo lang('create_group_subheading');?></p>

	<?php if ($message != ""){ ?>
		<div class="alert alert-info" role="alert"><?php echo $message;?></div>
	<?php } ?>

	<?php echo form_open("auth/create_group");?>

	      <div class="form-group">
	            <?php echo lang('create_group_name_label', 'group_name');?>
	            <?php echo form_input($group_name);?>
	      </div>

	      <div class="form-group">
	            <?php echo lang('create_group_desc_label', 'description');?>
	            <?php echo form_input($description);?>
	      </div>

	      <?php echo form_submit('submit', lang('create_group_submit_btn'),"class='btn btn-warning btn-block'");?>

	<?php echo form_close();?>
</div>