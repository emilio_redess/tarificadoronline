<div class="col-md-4">
	<h1><?php echo lang('deactivate_heading');?></h1>
	<p><?php echo sprintf(lang('deactivate_subheading'), $user->first_name . " " . $user->last_name);?></p>

	<?php echo form_open("auth/deactivate/".$user->id);?>


	<label class="radio-inline">
	  <input type="radio" name="confirm" value="yes" checked="checked"> <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
	</label>
	<label class="radio-inline">
	  <input type="radio" name="confirm" value="no"> <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
	</label>





	  <?php echo form_hidden($csrf); ?>
	  <?php echo form_hidden(array('id'=>$user->id)); ?>

	  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),"class='btn btn-warning btn-block'");?></p>

	<?php echo form_close();?>
</div>