<header class="codrops-header">
    <h1><?php echo lang('index_heading');?></h1>
</header>

<div class="container">


<?php if ($message != ""){ ?>
	<div class="alert alert-info" role="alert"><?php echo $message;?></div>
<?php } ?>

<table class="table table-bordered table-hover">
	<tr>
		
		<th><?php echo lang('index_fname_th');?></th>
		<th><?php echo lang('index_lname_th');?></th>
		<th><?php echo lang('index_email_th');?></th>
		<?php if ($this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))): ?>
			<th><?php echo lang('index_tipo_usuario_th');?></th>
		<?php endif ?>		
		<?php if ($this->ion_auth->is_admin()): ?>
			<th><?php echo lang('index_groups_th');?></th>
		<?php endif ?>
		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<tr>

			
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>

			<?php 
			if ($this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'))){
				if ($this->ion_auth->in_group($this->config->item('encargados_group', 'ion_auth'),$user->id))
					echo '<td>Admin</td>';
				else
					echo '<td>Normal</td>';
			}
			?>

            <?php if ($this->ion_auth->is_admin()): ?>
			<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                <?php endforeach?>
			</td>
			<?php endif ?>
            <?php if ($logged_user_id != $user->id) { ?>
				<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
			<?php }else{ ?>
				<td><?php echo ($user->active) ? lang('index_active_link') : lang('index_inactive_link');?></td>
			<?php } ?>			
			<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
		</tr>
	<?php endforeach;?>
</table>

<p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> <?php if ($this->ion_auth->is_admin()): echo '| ' . anchor('auth/create_group', lang('index_create_group_link'));  endif ?></p>

</div>