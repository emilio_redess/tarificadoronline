<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>



<div class="content clearfix">
    <h1>Welcome to CodeIgniter!</h1>

    <div id="body">

         <h2><a href="<?php echo site_url('rest_server'); ?>">REST Server Tests</a></h2>

        <?php if (file_exists(FCPATH.'documentation-restserver/index.html')) : ?>
        <h2><a href="<?php echo base_url('documentation-restserver/index.html'); ?>" target="_blank">REST Server Documentation</a></h2>
        <?php endif ?>

       
    </div>


</div>

