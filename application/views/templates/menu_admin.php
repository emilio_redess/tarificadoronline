<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Menu Administradores<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cloud" aria-hidden="true"></span> Webservice</a>
											<ul class="dropdown-menu">																									
												<li><?php echo anchor('rest_server/welcome_rest', '<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> REST Server', 'class="icon icon-cloud"'); ?></li>

												<li><?php echo anchor('tarificador/exportarTarificaciones', '<span class="glyphicon glyphicon-export" aria-hidden="true"></span> Exportar tarificaciones', 'class="icon icon-cloud"'); ?></li>
												
											</ul>
										</li>

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuarios y grupos</a>
											<ul class="dropdown-menu">	
												<li><?php echo anchor('auth', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listado de usuarios'); ?></li>	
											</ul>
										</li>												
																			

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> General</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Gestión de zonas</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('admin/zonas/crear_zona', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nueva zona'); ?></li>
														<li><?php echo anchor('admin/zonas/modificar_zona', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar zona', 'class="icon icon-pen"'); ?></li>
														<li><?php echo anchor('admin/zonas/borrar_zona', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar zona', 'class="icon icon-trash"'); ?></li>											
													</ul>
												</li>
											</ul>
										</li>

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Salud</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Gestión de productos</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('admin/productos_salud/ver_detalles_producto', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto'); ?></li>
														<li><?php echo anchor('admin/productos_salud/ver_detalles_producto_de_correduria', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto de correduria'); ?></li>
														<li><?php echo anchor('admin/productos_salud/crear_producto_paso1', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Añadir producto'); ?></li>															
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar producto', 'class="icon icon-pen"'); ?></li>
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Desactivar producto', 'class="icon icon-trash"'); ?></li>
													</ul>
												</li>
											</ul>
										</li>

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Decesos</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Gestión de productos</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('#', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto'); ?></li>
														<li><?php echo anchor('#', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto de correduria'); ?></li>
														<li><?php echo anchor('admin/productos_decesos/crear_producto_paso1', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Añadir producto'); ?></li>															
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar producto', 'class="icon icon-pen"'); ?></li>
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Desactivar producto', 'class="icon icon-trash"'); ?></li>
													</ul>
												</li>
											</ul>
										</li>

										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-key" aria-hidden="true"></span> Alquiler</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Gestión de productos</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('admin/productos_alquiler/ver_detalles_producto', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto'); ?></li>
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Añadir producto'); ?></li>															
														<li><?php echo anchor('admin/productos_alquiler/modificar_factor_calculo', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar factor de cálculo', 'class="icon icon-pen"'); ?></li>
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar producto', 'class="icon icon-trash"'); ?></li>
													</ul>
												</li>
											</ul>
										</li>
										
									</ul>
								</li>							

