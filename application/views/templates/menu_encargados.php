<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
					<?php 
						// Vamos a comprobar que ramos tiene la correduria del usuario logeado.
						// El resultado se montará con un array donde cada indice será el codigo del ramo
						// y el valor será true o false segun si la tiene o no.
						//$user_groups = $this->ion_auth->get_users_groups()->result();
						//$groupId = $this->funciones_utiles->get_group_correduria_id($user_groups); 
						//$ramosArray = $this->funciones_utiles->get_ramos_correduria($groupId); 
						$ramosArray = $_SESSION['ramosArray'];
					?>



								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Menu Administradores<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">



										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuarios y grupos</a>
											<ul class="dropdown-menu">	
												<li><?php echo anchor('auth', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listado de usuarios'); ?></li>	
											</ul>
										</li>
										<?php 
										if ($ramosArray['SAL']) { 
									

											?>
										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> Salud</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Gestión de productos</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('admin/productos_salud/ver_detalles_producto_de_correduria', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto'); ?></li>
														
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar producto', 'class="icon icon-pen"'); ?></li>
														<li><?php echo anchor('tarificador/construccion/1', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Desactivar producto', 'class="icon icon-trash"'); ?></li>
													</ul>
												</li>
											</ul>
										</li>	
										<?php } ?>									

										<?php if ($ramosArray['ALQ']) { ?>
										<li class="dropdown-submenu">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-key" aria-hidden="true"></span> Alquiler</a>
											<ul class="dropdown-menu">
												<li class="dropdown-submenu">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> Gestión de productos</a>
													<ul class="dropdown-menu">
														<li><?php echo anchor('admin/productos_alquiler/ver_detalles_producto', '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Ver producto'); ?></li>
														
														<li><?php echo anchor('admin/productos_alquiler/modificar_factor_calculo', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Modificar factor de cálculo', 'class="icon icon-pen"'); ?></li>
														
													</ul>
												</li>
											</ul>
										</li>
										<?php } ?>																						
																			
									</ul>
								</li>

					

