<?php
	$user = $this->ion_auth->user()->row();
	$nombreApellidos =  $user->first_name . " " . $user->last_name;
	//$userId = $this->ion_auth->get_user_id();
	$userId = $user->id;
	$mensajesPendientes = $this->funciones_utiles->get_numero_mensajes_pendientes($userId);
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Tarificador Redess3.0</title>
		<link rel="shortcut icon" href="<?=base_url()?>/public/images/favicon.ico" type="image/x-icon" />		
		<link rel="icon" href="<?=base_url()?>/public/images/favicon.ico" type="image/x-icon">
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="Redess" />

		<?php 
			echo link_tag('public/css/main.css');
			echo link_tag('public/css/style_inicio.css');
			echo link_tag('public/css/bootstrap.min.css');
			echo link_tag('public/css/sticky-footer-navbar.css');
			echo link_tag('public/css/balloon.min.css'); // balloon tooltips


		?>
		<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />-->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://use.fontawesome.com/7cc7b06555.js"></script>
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->


		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs/jq-2.2.0,dt-1.10.11,b-1.1.2,b-print-1.1.2,r-2.0.2/datatables.min.css"/>
		
	</head>
	<body>
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
             <div class="container-fluid">
                 <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                     </button>
               
                     <?php echo anchor("http://www.redess.es",'Redess 3.0','target="_blank" class="navbar-brand"') ?>
                 </div>

				<!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">-->
				<div id="navbar" class="collapse navbar-collapse">
					<!-- menu zona izquierda -->
		          <ul class="nav navbar-nav">		         
		            <li class="active"><?php echo anchor('tarificador/', 'Área de cliente'); ?></li>


<!--
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Soporte <span class="caret"></span></a>
		              <ul class="dropdown-menu">
		                <li><?php echo anchor('tarificador/construccion','Tickets'); ?></li>
		                <li><?php echo anchor('tarificador/construccion','Anuncios'); ?></li>          
		                <li><?php echo anchor('tarificador/construccion','Preguntas frecuentes - FAQ'); ?></li>		          
		              </ul>
		            </li>
		             <li><?php echo anchor('tarificador/construccion','Nuevo ticket'); ?></li>
-->		             


		             <li><?php echo anchor('tarificador/contactanos','Contacto'); ?></li>
		          </ul>					

					<!-- menu zona derecha -->
					<ul class="nav navbar-nav navbar-right">
					<?php 

				// Obtenemos el numero de mensajes pendientes para hoy
				//$user = $this->ion_auth->user()->row();
				//$userId = $user->id;
				//$mensajesPendientes = $this->funciones_utiles->get_numero_mensajes_pendientes($userId); 
								


						if ($mensajesPendientes > 0)
							$texto_mensajes = 'Mensajes sin leer <span id="unreadMessages" class="badge pulse">' . $mensajesPendientes . '</span>';
						else
							$texto_mensajes = 'Mensajes sin leer <span id="unreadMessages" class="badge">' . $mensajesPendientes . '</span>';


						echo '<li role="presentation">' . anchor('tarificador/anotaciones',$texto_mensajes) . ' </li>';
				
					?>
						
						

						<li class="dropdown">
							<?php
							//	$user = $this->ion_auth->user()->row();
							//	$nombreApellidos =  $user->first_name . " " . $user->last_name;
							//	$userId = $this->ion_auth->get_user_id();
							?>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nombreApellidos; ?><span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><?php echo anchor('auth/edit_user/' . $userId, 'Editar datos de tu usuario'); ?></li>
								<li><?php echo anchor('auth/logout', 'Cerrar sesión'); ?></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>