<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
					<?php 
						// Vamos a comprobar que ramos tiene la correduria del usuario logeado.
						// El resultado se montará con un array donde cada indice será el codigo del ramo
						// y el valor será true o false segun si la tiene o no.

						//$user_groups = $this->ion_auth->get_users_groups()->result();
						//$groupId = $this->funciones_utiles->get_group_correduria_id($user_groups); 

						
						
						$colaboradores = $_SESSION['colaboradores'];
						//$ramosArray = $this->funciones_utiles->get_ramos_correduria($groupId); 
						$ramosArray = $_SESSION['ramosArray'];


					?>

								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nueva tarificación<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
																
										<li><?php 
												if ($ramosArray['VID'])
													echo anchor('tarificador/nueva_tarificacion_vida','Vida'); 
												//else
												//	echo anchor('tarificador/ramo_disabled','Vida'); 
										?></li>	
										<li><?php 
												if ($ramosArray['SAL'])
													echo anchor('tarificador/nueva_tarificacion_salud','Salud'); 
												//else
												//	echo anchor('tarificador/ramo_disabled','Salud'); 
										?></li>	
										<li><?php 
												if ($ramosArray['DCS'])
													echo anchor('tarificador/construccion/1','Decesos'); 
												//else
												//	echo anchor('tarificador/ramo_disabled','Decesos'); 
										?></li>	
										<li><?php 
												if ($ramosArray['HOG'])
													echo anchor('tarificador/construccion/1','Hogar'); 
												//else
												//	echo anchor('tarificador/ramo_disabled','Hogar'); 
										?></li>	

										<li><?php 
												if ($ramosArray['ALQ'])
													echo anchor('tarificador/nueva_tarificacion_alquiler','Alquiler'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Alquiler'); 
										?></li>																																														
																
					
									</ul>							
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listado tarificaciones<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">

										<li><?php 
												if ($ramosArray['VID'])
													echo anchor('tarificador/listado/1','Vida'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Vida'); 
										?></li>	
										<li><?php 
												if ($ramosArray['SAL'])
													echo anchor('tarificador/listado/2','Salud'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Salud'); 
										?></li>	
										<li><?php 
												if ($ramosArray['DCS'])
													echo anchor('tarificador/listado/3','Decesos'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Decesos'); 
										?></li>	
										<li><?php 
												if ($ramosArray['HOG'])
													echo anchor('tarificador/listado/4','Hogar'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Hogar'); 
										?></li>	

										<li><?php 
												if ($ramosArray['ALQ'])
													echo anchor('tarificador/listado/5','Alquiler'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Alquiler'); 
										?></li>		

										<li><?php 
												if ($ramosArray['FRM'])
													echo anchor('tarificador/listado/6','Formularios de contacto'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Formularios de contacto'); 
										?></li>		

										<li><?php 
												if ($ramosArray['ESP'])
													echo anchor('tarificador/listado/7','Productos especiales'); 
											//	else
											//		echo anchor('tarificador/ramo_disabled','Formularios de contacto'); 
										?></li>																				


										<li><?php echo anchor('tarificador/listado/', 'Todos los ramos'); ?></li>
									</ul>							
								</li>
								<?php 
								
								if ($_SESSION['numero_colaboradores'] > 0) { 

									?>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> Colaboradores<span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
										<?php foreach ($colaboradores as $clave => $valor) { ?>
											<li><?php echo anchor('tarificador/listado_colaborador/' . $clave, $valor['nombre']); ?></li>
											<?php } ?>
										</ul>
								</li>	
								<?php } ?>


								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Exportar datos<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><?php echo anchor('tarificador/exportarTarificaciones', 'Exportar tarificaciones'); ?></li>
										<li><?php echo anchor('tarificador/exportarAnotaciones', 'Exportar anotaciones'); ?></li>
									</ul>
								</li>

								<!--<li><?php echo anchor('tarificador/exportarTarificaciones', '<span class="glyphicon glyphicon-export" aria-hidden="true"></span> Exportar tarificaciones', 'class="icon icon-cloud"'); ?></li>					-->

								<li><?php echo anchor('tarificador/listado/0', '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Papelera de reciclaje', 'class="icon icon-trash"'); ?></li>