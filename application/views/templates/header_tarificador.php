
		<div class="container">
			<div class="page-header">
				<div class="container">
<!--
					<div class="row">
						<img class="center-block img-responsive" src="<?php echo base_url('public/images/logo_stz.png'); ?>">
					</div>
-->
					<nav class="navbar navbar-default navbar-static-top" role="navigation">
						<div class="container-fluid">
							<ul class="nav navbar-nav">
								<!--<li class="active"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio</a></li>-->
								<li><?php echo anchor('tarificador', '<span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio'); ?></li>

								<?php 
									// Menú para el superadmin
									if ($this->ion_auth->in_group('administradores')){ 
										include 'menu_admin.php';
									}elseif ($this->ion_auth->in_group('encargados')){ 
										include 'menu_encargados.php';
									}

									include 'menu_usuarios.php';
								
								?>	

							</ul>							
						</div>
					</nav>
				</div>
			</div>
