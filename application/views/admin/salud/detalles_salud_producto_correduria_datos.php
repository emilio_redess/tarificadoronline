<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>


<div class="row bottom_buffer">
	<div class="col-md-12">
		<a href="<?php echo site_url($url_back) ?>" class="btn btn-warning" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> <?php echo $url_next_text; ?></a>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2" style="background-color:rgba(234,173,78,0.61)">Datos generales</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Compañía</b></td>
					<td><?php echo $datosProducto->cia_nombre; ?></td>
				</tr>
				<tr>
					<td><b>Ramo</b></td>
					<td><?php echo $ramo; ?></td>
				</tr>
				<tr>
					<td><b>Producto</b></td>
					<td><?php echo $datosProducto->nombre; ?></td>
				</tr>
																																			
			</tbody>
		</table>
	
</div>
<div class="col-md-6">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2" style="background-color:rgba(234,173,78,0.61)">Zonas</th>
				</tr>
			</thead>
			<tbody>	
		<?php 

			foreach ($provinciasZona as $k=>$v)
			{
?>

				<tr>
					<td><b><?php echo $k; ?></b></td>
					<td><?php 
							foreach ($v as $provincia){ 
								echo $provincia;
								if ($provincia === end($v)) 
									echo '.';												 
								else
									echo ', '; 
								} 
						?>
					</td>
				</tr>

		<?php } ?>
			</tbody>
		</table>					
	</div>
</div>
<div class="row">
	<div class="col-md-6">
	<?php if (!is_null($tasasCM)){ ?>
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="3" style="background-color:rgba(234,173,78,0.61)">Tasas sin copago (Cuadro médico)</th>
				</tr>
				<tr>
					<th>Edad</th>
					<th>Tasa</th>
					<th>Zona</th>
				</tr>
			</thead>
			<tbody>
			<?php 

				foreach ($tasasCM as $k=>$v)
				{
			?>
				<tr>
					<td><?php echo $v['edad']; ?></td>	
					<td><?php echo $v['tasa']; ?></td>	
					<td><?php echo $v['nombreZona']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php } ?>

	</div>
	<div class="col-md-6">
	<?php if (!is_null($tasasRE)){ ?>
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="3" style="background-color:rgba(234,173,78,0.61)">Tasas con copago (Reembolso)</th>
				</tr>
				<tr>
					<th>Edad</th>
					<th>Tasa</th>
					<th>Zona</th>
				</tr>
			</thead>
			<tbody>
			<?php 

				foreach ($tasasRE as $k=>$v)
				{
			?>
				<tr>
					<td><?php echo $v['edad']; ?></td>	
					<td><?php echo $v['tasa']; ?></td>	
					<td><?php echo $v['nombreZona']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>			
		<?php } ?>

	</div>
</div>

<div class="row bottom_buffer">
	<div class="col-md-12">
		<a href="<?php echo site_url($url_back) ?>" class="btn btn-warning" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> <?php echo $url_next_text; ?></a>
	</div>
</div>
