<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_salud/crear_producto_paso2'); ?>
			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					<a href="<?php echo site_url($url_paso_anterior) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Volver al paso anterior</a>
				</div>
				<div class="col-md-4 alignCentro">
					
				</div>
				<div class="col-md-4 alignDer">
					<a href="<?php echo site_url($url_omitir) ?>" class="btn btn-info" role="button">Saltar paso <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
				</div>
			</div>			

			<input type="hidden" name="soloVacios" id="soloVacios" value="<?php echo $soloVacios; ?>">
			<input type="hidden" name="selectRamo" id="selectRamo" value="<?php echo $codigoRamo; ?>">

			<div class="form-group">
				<label for="selectCorredurias">Selecciona la correduría de seguros a la que quieres asignar el producto</label>
				<select id="selectCorredurias" name="selectCorredurias" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorredurias', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($corredurias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCorredurias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros que tenga el producto que quieres asignar</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="" <?php echo set_select('selectCias', '', TRUE); ?>>Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="nombreProducto">Nombre del producto (es el nombre que engloba tanto a CM como a RE)</label>
				<input type="text" class="form-control" name="nombreProducto" id="nombreProducto" value="<?php echo set_value('nombreProducto'); ?>" />
			</div>

			<div class="form-group">
				<label for="selectProductoCM">Selecciona el producto para asignarlo a Cuadro Médico</label>
				<select id="selectProductoCM" name="selectProductoCM" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>	
				</select>
			</div>

			<div class="form-group">
				<label for="selectProductoRE">Selecciona el producto para asignarlo a Reembolso</label>
				<select id="selectProductoRE" name="selectProductoRE" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>	

			<div class="form-group">
				<label>Selecciona a que tipo de contratantes aplicar este producto</label>
				<?php foreach ($contratantes as $item): ?>
					<div class="checkbox">
					  <label>
					    <input type="checkbox" checked id="<?php echo "contratante_" . $item['id']; ?>" name="<?php echo "contratante_" . $item['id']; ?>" value="<?php echo $item['id']; ?>">
					    <?php echo $item['nombre']; ?>
					  </label>
					</div>	
				<?php endforeach; ?>				
			</div>			

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
				
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar datos</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>	
			
		</form>
	</div>
</div>


