<header class="codrops-header">
	<h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_salud/crear_producto_paso1'); ?>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					<button type="button" class="btn btn-info" disabled><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Volver al paso anterior</button>
				</div>
				<div class="col-md-4 alignCentro">
					
				</div>
				<div class="col-md-4 alignDer">
					<a href="<?php echo site_url($url_omitir) ?>" class="btn btn-info" role="button">Saltar paso <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
				</div>
			</div>			

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros de la que vas a añadir las tasas</label>
				<select id="selectCias" name="selectCias" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCias', '', TRUE); ?>>Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>


			<div class="form-group">
				<label for="selectFormaPago">Selecciona la forma de pago del producto</label>
				<select id="selectFormaPago" name="selectFormaPago" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectFormaPago', '', TRUE); ?>>Selecciona una forma de pago</option>
				<?php 
				foreach ($formaPago as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectFormaPago', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectOrigentasas">Selecciona el origen de las tasas del producto</label>
				<select id="selectOrigentasas" name="selectOrigentasas" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectOrigentasas', '', TRUE); ?>>Selecciona un origen</option>
				<?php 
				foreach ($origenTasas as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectOrigentasas', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>



			<div class="form-group">
				<label for="nombreProducto">Nombre del producto</label>
				<input type="text" class="form-control" name="nombreProducto" id="nombreProducto" value="<?php echo set_value('nombreProducto'); ?>" />
			</div>

			<div class="form-group">
				<label for="nombreLogo">Nombre del logo del producto</label>
				<input type="text" class="form-control" name="nombreLogo" id="nombreLogo" value="<?php echo set_value('nombreLogo'); ?>" />
			</div>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar datos</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>				



		</form>
	</div>
</div>
