<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_salud/crear_producto_paso4'); ?>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					<a href="<?php echo site_url($url_paso_anterior) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Volver al paso anterior</a>
				</div>
				<div class="col-md-4 alignCentro">
					
				</div>
				<div class="col-md-4 alignDer">
					<a href="<?php echo site_url($url_omitir) ?>" class="btn btn-info" role="button">Saltar paso <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
				</div>
			</div>	

			<input type="hidden" name="soloVacios" id="soloVacios" value="<?php echo $soloVacios; ?>">
			<input type="hidden" name="selectRamo" id="selectRamo" value="<?php echo $codigoRamo; ?>">					
			<input type="hidden" name="estadoActivo" id="estadoActivo" value="2">					


			<div class="form-group">
				<label for="selectCorredurias">Selecciona la correduría de seguros a la que quieres asignar el producto</label>
				<select id="selectCorredurias" name="selectCorredurias" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorredurias', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($corredurias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCorredurias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros que tenga el producto que quieres asignar</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="" <?php echo set_select('selectCias', '', TRUE); ?>>Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectProducto">Selecciona el producto</label>
				<select id="selectProducto" name="selectProducto" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>

			<div class="form-group">
				<label for="selectSubproducto">Selecciona a que subproducto aplicar el descuento</label>
				<select id="selectSubproducto" name="selectSubproducto" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectSubproducto', '', TRUE); ?>>Selecciona un subproducto</option>
				<option value="0">Cuadro Médico (CM)</option>
				<option value="1">Reembolso (RE)</option>
				<option value="2">Ambas</option>
				</select>
			</div>				

			<div class="form-group">
				<label for="selectOperacion">Selecciona el tipo de operación</label>
				<select id="selectOperacion" name="selectOperacion" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectOperacion', '', TRUE); ?>>Selecciona una operación</option>
				<?php 
				foreach ($operacion as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectOperacion', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>	

			<div class="form-group">
				<label for="selectFormaOperacion">Selecciona la forma en la que se aplicará la operación</label>
				<select id="selectFormaOperacion" name="selectFormaOperacion" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectFormaOperacion', '', TRUE); ?>>Selecciona una forma de operación</option>
				<?php 
				foreach ($forma_operacion as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectFormaOperacion', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>				

			<div class="form-group">
				<label for="selectFormaPago">Selecciona la forma de pago a la que se aplicará la operación</label>
				<select id="selectFormaPago" name="selectFormaPago" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectFormaPago', '', TRUE); ?>>Selecciona una forma de pago</option>
				<option value="TODAS" <?php echo set_select('selectFormaPago', 'TODAS'); ?>>Todas</option>
				<?php 
				foreach ($formaPago as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectFormaPago', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>	

			<div class="form-group">
				<label for="selectRangoPersonas">Selecciona el rango del número de personas al que aplicar la operación</label>
				<select id="selectRangoPersonas" name="selectRangoPersonas" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectRangoPersonas', '', TRUE); ?>>Selecciona un rango</option>
				<?php 
				foreach ($rangos_personas as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectRangoPersonas', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>		

			<div class="form-group">
				<label for="selectEdadMinima">Selecciona la edad mínima para aplicarle la operación</label>
				<select id="selectEdadMinima" name="selectEdadMinima" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectEdadMinima', '', TRUE); ?>>Selecciona la edad mínima</option>
				<?php 
				for ($i = 0; $i <= 100; $i++) {
					echo '<option value="' . $i . '" ' . set_select('selectRangoPersonas', $i) . '>' . $i . '</option>';
				}
				?>
				</select>
			</div>	

			<div class="form-group">
				<label for="selectEdadMaxima">Selecciona la edad máxima para aplicarle la operación</label>
				<select id="selectEdadMaxima" name="selectEdadMaxima" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectEdadMaxima', '', TRUE); ?>>Selecciona la edad máxima</option>
				<?php 
				for ($i = 0; $i <= 100; $i++) {
					echo '<option value="' . $i . '" ' . set_select('selectRangoPersonas', $i) . '>' . $i . '</option>';
				}
				?>
				</select>
			</div>		

			<div class="form-group">
				<label for="valor">Valor numérico</label>
				<input type="text" class="form-control" name="valor" id="valor" value="<?php echo set_value('valor'); ?>" />
			</div>														



			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
				
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar datos</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>	
		</form>
	</div>
</div>
