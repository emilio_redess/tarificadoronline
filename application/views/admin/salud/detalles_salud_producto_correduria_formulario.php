<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_salud/ver_detalles_producto_de_correduria'); ?>


			<div class="form-group">
<?php if ($this->ion_auth->is_admin()){ ?>			
				<label for="selectCorredurias">Selecciona la correduría de seguros</label>
				<select id="selectCorredurias" name="selectCorredurias" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorredurias', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($corredurias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCorredurias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
<?php }else{ ?>	
				<input type="hidden" name="selectCorredurias" id="selectCorredurias" value="<?php echo $correduria_id; ?>">

<?php } ?>			
			</div>


			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="">Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '">' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectProducto">Selecciona el producto</label>
				<select id="selectProducto" name="selectProducto" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>

			<div class="row bottom_buffer">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Continuar</button>
				</div>
			</div>
		</form>
	</div>
</div>
