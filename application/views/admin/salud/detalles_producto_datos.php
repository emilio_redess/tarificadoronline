<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-6">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">Datos generales</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Compañía</b></td>
					<td><?php echo $datosProducto->nomCia; ?></td>
				</tr>
				<tr>
					<td><b>Ramo</b></td>
					<td><?php echo $datosProducto->nombreRamo; ?></td>
				</tr>
				<tr>
					<td><b>Producto</b></td>
					<td><?php echo $datosProducto->nomProducto; ?></td>
				</tr>
				<?php
					if ($this->ion_auth->is_admin()) {
				?>				
				<tr>
					<td><b>Logo producto</b></td>
					<td><?php echo $datosProducto->logoProducto; ?></td>
				</tr>
				<?php } ?>					
				<tr>
					<td><b>Forma de pago</b></td>
					<td><?php echo $datosProducto->formaPagoNombre; ?></td>
				</tr>
				<?php
					if ($this->ion_auth->is_admin()) {
				?>
				<tr>
					<td><b>Origen de las tasas</b></td>
					<td><?php echo $datosProducto->origenTasasNombre; ?></td>
				</tr>	
				<?php } ?>																														
			</tbody>
		</table>
	

				<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
							<thead>
								<tr>
									<th colspan="2">Zonas</th>
								</tr>
							</thead>
							<tbody>	
		<?php 

			foreach ($provinciasZona as $k=>$v)
			{
?>

								<tr>
									<td><b><?php echo $k; ?></b></td>
									<td><?php 
											foreach ($v as $provincia)
												{ 
													echo $provincia;
													if ($provincia === end($v)) 
												        echo '.';												 
												    else
												    	echo ', '; 

												} 
										?>
									</td>
								</tr>

		<?php } ?>
							</tbody>
				</table>		
	</div>

	<div class="col-md-6">

		<?php 
			$this->table->set_heading('Edad', 'Tasa', 'Zona');
			$template = array(
				'table_open' => '<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">'
			);
			$this->table->set_template($template);					
			echo $this->table->generate($tasas);
		?>					
	</div>
</div>

<div class="row bottom_buffer">
	<div class="col-md-12">
		<a href="<?php echo site_url($url_back) ?>" class="btn btn-warning" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> <?php echo $url_next_text; ?></a>
	</div>
</div>
