<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open('admin/zonas/crear_zona'); ?>

		<div class="form-group">
			<label for="nombreZona">Nombre de la zona</label>
			<input type="text" class="form-control" name="nombreZona" id="nombreZona" value="<?php echo set_value('nombreZona'); ?>" />
		</div>


			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aceptar</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>
		</form>
	</div>
</div>