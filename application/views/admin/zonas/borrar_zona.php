<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open('admin/zonas/borrar_zona'); ?>

			<div class="form-group">
				<label for="selectZonas">Zonas de provincias</label>
				<select id="selectZonas" name="selectZonas" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectZonas', '', TRUE); ?>>Selecciona una zona</option>
				<?php 
				foreach ($zonas as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectZonas', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aceptar</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>
		</form>
	</div>
</div>