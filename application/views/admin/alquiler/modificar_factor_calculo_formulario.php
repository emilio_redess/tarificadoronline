<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_alquiler/modificar_factor_calculo'); ?>

			<input type="hidden" name="soloVacios" id="soloVacios" value="<?php echo $soloVacios; ?>">
			<input type="hidden" name="selectRamo" id="selectRamo" value="<?php echo $codigoRamo; ?>">
			<input type="hidden" name="idProd" id="idProd" value="">


<?php if ($this->ion_auth->is_admin()){ ?>
			<div class="form-group">
				<label for="selectCorreduria">Correduría a la que se va a modificar:</label>
				<select id="selectCorreduria" name="selectCorreduria" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorreduria', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($correduriasArray as $item):
					echo '<option value="' . $item->id . '" ' . set_select('selectCorreduria', $item->id) . '>' . $item->nombre . '</option>';
				endforeach;
				?>
				</select>
			</div>	
<?php } ?>	

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="">Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '">' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectProducto">Selecciona el producto</label>
				<select id="selectProducto" name="selectProducto" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>

			<div class="form-group">
				<label for="fc_actual">Valor actual del factor de cálculo</label>
				<input type="text" name="fc_actual" id="fc_actual" class="form-control input_fc" readonly>
			</div>	

			<div class="form-group">
				<label for="fc_nuevo">Nuevo valor del factor de cálculo</label>
				<input type="text" name="fc_nuevo" id="fc_nuevo" class="form-control input_fc" disabled>
				
			</div>			

			<div class="row bottom_buffer">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Continuar</button>
				</div>
			</div>
		</form>
	</div>
</div>
