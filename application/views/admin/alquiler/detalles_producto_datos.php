<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row bottom_buffer">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">Datos generales</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Compañía</b></td>
					<td><?php echo $datosProducto->nomCia; ?></td>
				</tr>
				<tr>
					<td><b>Ramo</b></td>
					<td><?php echo $datosProducto->nombreRamo; ?></td>
				</tr>
				<tr>
					<td><b>Producto</b></td>
					<td><?php echo $datosProducto->nomProducto; ?></td>
				</tr>
				<?php
					if ($this->ion_auth->is_admin()) {
				?>				
				<tr>
					<td><b>Logo producto</b></td>
					<td><?php echo $datosProducto->logoProducto; ?></td>
				</tr>
				<?php } ?>	
				<tr>
					<td><b>Forma de pago</b></td>
					<td><?php echo $datosProducto->formaPagoNombre; ?></td>
				</tr>
				<?php
					if ($this->ion_auth->is_admin()) {
				?>
				<tr>
					<td><b>Origen de las tasas</b></td>
					<td><?php echo $datosProducto->origenTasasNombre; ?></td>
				</tr>	
				<?php } ?>																											
			</tbody>
		</table>
	
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="4">Datos específicos</th>
				</tr>
				<tr>
				<?php
					if ($this->ion_auth->is_admin())
						echo '<th>Correduría</th>';

				?>					
					<th>Factor de cálculo</th>
					<th>Producto activo</th>
				</tr>				
			</thead>
			<tbody>
				<?php
					if ($this->ion_auth->is_admin()) {
						foreach ($factorCalculo as $item):
							echo '<tr><td>' . $item['nombre'] . '</td><td>' . $item['valor'] . '</td><td>' . $item['activo'] . '</td></tr>';
						endforeach;						

					}else{
				?>				
				<tr>
					
					<td><?php echo $factorCalculo; ?></td>
					<td><?php echo $activo; ?></td>
				</tr>	
				<?php } ?>
			</tbody>
		</table>
	
	</div>	
	</div>	


<div class="row bottom_buffer">
	<div class="col-md-12">
		<a href="<?php echo site_url($url_back) ?>" class="btn btn-warning" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> <?php echo $url_next_text; ?></a>
	</div>
</div>
