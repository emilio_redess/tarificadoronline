<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_decesos/crear_producto_paso6'); ?>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					<a href="<?php echo site_url($url_paso_anterior) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Volver al paso anterior</a>
				</div>
				<div class="col-md-4 alignCentro">
					
				</div>
				<div class="col-md-4 alignDer">					
					<button type="button" class="btn btn-info" disabled>Saltar paso <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></button>
				</div>
			</div>	

			<input type="hidden" name="soloVacios" id="soloVacios" value="<?php echo $soloVacios; ?>">
			<input type="hidden" name="selectRamo" id="selectRamo" value="<?php echo $codigoRamo; ?>">					
			<input type="hidden" name="estadoActivo" id="estadoActivo" value="0">					


			<div class="form-group">
				<label for="selectCorredurias">Selecciona la correduría de seguros a la que quieres asignar el producto</label>
				<select id="selectCorredurias" name="selectCorredurias" class="form-control ajax_select">
				<option selected="selected" value="" <?php echo set_select('selectCorredurias', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($corredurias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCorredurias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros que tenga el producto que quieres asignar</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="" <?php echo set_select('selectCias', '', TRUE); ?>>Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '" ' . set_select('selectCias', $item['id']) . '>' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

			<div class="form-group">
				<label for="selectProducto">Selecciona el producto</label>
				<select id="selectProducto" name="selectProducto" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>

			<div class="row">
				<div class="col-md-4 alignIzq"></div>
				<div class="col-md-4 alignCentro">			
					<div class="mk-trc" data-color="deep-grey" data-size="3" data-text="true" data-radius="true">
						<input id="checkActivar" name="checkActivar" class="checkActivar" type="checkbox" disabled>
						<label for="checkActivar"><i></i></label>
					</div>
				</div>
				<div class="col-md-4 alignDer"></div>			
			</div>



			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
				
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Confirmar cambios</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>	
		</form>
	</div>
</div>
