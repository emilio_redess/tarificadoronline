<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('admin/productos_decesos/crear_producto_paso4'); ?>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					<a href="<?php echo site_url($url_paso_anterior) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> Volver al paso anterior</a>
				</div>
				<div class="col-md-4 alignCentro">
					
				</div>
				<div class="col-md-4 alignDer">
					<a href="<?php echo site_url($url_omitir) ?>" class="btn btn-info" role="button">Saltar paso <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
				</div>
			</div>			

			<input type="hidden" name="soloVacios" id="soloVacios" value="<?php echo $soloVacios; ?>">
			<input type="hidden" name="selectRamo" id="selectRamo" value="<?php echo $codigoRamo; ?>">

			<div class="form-group">
				<label for="selectCias">Selecciona la compañía de seguros de la que vas a añadir las tasas</label>
				<select id="selectCias" name="selectCias" class="form-control ajax_select">
				<option selected="selected" value="">Selecciona una compañía</option>
				<?php 
				foreach ($companyias as $item):
					echo '<option value="' . $item['id'] . '">' . $item['nombre'] . '</option>';
				endforeach;
				?>
				</select>
			</div>

		

			<div class="form-group">
				<label for="selectProducto">Selecciona el producto</label>
				<select id="selectProducto" name="selectProducto" class="form-control selectProducto" disabled>
				<option selected="selected" value="">Selecciona un producto</option>
				</select>
			</div>

			<div class="form-group">
				<label for="userfile">Selecciona un archivo</label>
				<input type="file" name="userfile" id="userfile" class="form-control" />
				<p class="help-block">Archivo .csv que contenga las tasas con el formato <a href="<?php echo base_url('public/images/misc/ejemplo_capitales_decesos.png'); ?>" target="_blank">siguiente</a></p>
				<p class="help-block">Descargar plantilla de <a href="<?php echo base_url('public/csv/ejemplo_capitales_provincia_decesos.csv'); ?>" target="_blank">ejemplo</a> en formato .csv </p>




			</div>

			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
				
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Guardar datos</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>	
		</form>
	</div>
</div>
