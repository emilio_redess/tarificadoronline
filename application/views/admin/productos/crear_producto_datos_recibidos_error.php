<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert"><?php echo $mensaje; ?></div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if (isset($error1)){
			echo '<p>Mensaje devuelto: </p>';
			echo '<p><code>' . $error1 . '</code></p>';
		}?>
	</div>		
</div>		



<div class="row bottom_buffer">
	<div class="col-md-12 alignCentro">
		<a href="<?php echo site_url($url_destino) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> <?php echo $url_destino_text; ?></a>
	</div>
</div>


	
		