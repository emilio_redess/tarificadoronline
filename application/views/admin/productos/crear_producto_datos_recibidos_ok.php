<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row bottom_buffer">
	<div class="col-md-6 alignIzq">
		<a href="<?php echo site_url($url_back) ?>" class="btn btn-info" role="button"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> <?php echo $url_back_text; ?></a>
	</div>
	<div class="col-md-6 alignDer">
		<a href="<?php echo site_url($url_next) ?>" class="btn btn-info" role="button"><?php echo $url_next_text; ?> <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="progress">
			<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $porcentaje_completado; ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?php echo $porcentaje_completado; ?>%;">
				<?php echo $porcentaje_completado; ?>% Completo
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success" role="alert"><?php echo $mensaje; ?></div>
	</div>
</div>

<div class="row bottom_buffer">
	<div class="col-md-12">
		<?php if (isset($datos1_arr)){
			echo '<p>Consulta SQL generada: </p>';
			echo '<p><code>' . $datos1_arr . '</code></p>';
		}?>
	</div>
</div>		
		








	