<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>



	

<div class="row bottom_buffer">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">DATOS GENERALES</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Fecha de tarificación</b></td>
					<td><?php echo $fechaTarificacion; ?></td>
				</tr>
				<tr>
					<td><b>Origen</b></td>
					<td><?php echo $origen; ?></td>
				</tr>			
				<tr>
					<td><b>Código de referencia</b></td>
					<td><?php echo $codtar; ?></td>
				</tr>
				<tr>
					<td><b>Nombre y apellidos</b></td>
					<td><?php echo $nombreApellidos; ?></td>
				</tr>
				<tr>
					<td><b>E-mail</b></td>
					<td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td><b>Teléfono</b></td>
					<td><?php echo $telefono; ?></td>
				</tr>
				<tr>
					<td><b>Valor del alquiler</b></td>
					<td><?php echo $valorAlquiler; ?></td>
				</tr>																													
				<tr>
					<td><b>Tipo de local</b></td>
					<td><?php echo $tipoLocal; ?></td>
				</tr>				
			</tbody>
		</table>		       

	</div>
</div>


<div class="row">
	<div class="col-md-12">
	<table class="table table-striped table-hover table-bordered">
	<tr>
	<th colspan="4" class="table_th_left">PRECIOS</th>
	</tr>
		<tr>
			<th>Compañía</th>
			<th>6 meses</th>
			<th>12 meses</th>
			<th>18 meses</th>
		</tr>
<?php

		foreach ($primas as $k => $v) {

			if (is_null($v["prima_6m"]))
				$prima_6m = "-";
			else
				$prima_6m = $v["prima_6m"] . " €";

			if (is_null($v["prima_12m"]))
				$prima_12m = "-";
			else
				$prima_12m = $v["prima_12m"] . " €";

			if (is_null($v["prima_18m"]))
				$prima_18m = "-";
			else
				$prima_18m = $v["prima_18m"] . " €";	



			echo "<tr>";
			echo "<td>";
			echo img(array('src'=>'public/images/logos_cias/' . $v["logoProducto"], 'alt'=> $v['nombreCia']));
			echo "</td>";

			echo "<td class='precio_parrilla'><span>" . $prima_6m . "</span></td>";
			echo "<td class='precio_parrilla'><span>" . $prima_12m . "</span></td>";
			echo "<td class='precio_parrilla'><span>" . $prima_18m . "</span></td>";
			echo "</tr>";
		}
?>
	</table>		       

	</div>
</div>






