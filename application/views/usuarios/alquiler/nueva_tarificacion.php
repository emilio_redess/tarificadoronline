
    <header class="codrops-header">
        <h1>Nueva tarificación de seguro de protección de alquiler</h1>
    </header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>
	

		<div class="row">
		    <div class="col-md-12">
		        <!--<form action= "http://localhost/webs/tarificador-server/index.php/ws-api/tarificador/alquiler" method="POST">-->
		        <?php echo form_open_multipart('tarificador/nueva_tarificacion_alquiler'); ?>
		       
		            <div class="form-group">
		                <label for="nombreApellidos">nombre y apellidos</label>
		                <input type="text" name="nombreApellidos" id="nombreApellidos" class="form-control" value="<?php echo set_value('nombreApellidos'); ?>">		            
		            </div>


		            <div class="form-group">
		                <label for="email">email</label>
		                <input type="email" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>">
		            </div>

		            <div class="form-group">
		                <label for="telefono">telefono</label>
		                <input type="tel" name="telefono" id="telefono" class="form-control" value="<?php echo set_value('telefono'); ?>">
		            </div>

					<div class="form-group">
						<label for="tipoLocal">Tipo de local</label>
                        <select id="tipoLocal" name="tipoLocal" class="form-control">
                           
                            <option value="VIV" selected="selected">Vivienda</option>
                            <option value="COM">Local comercial</option>
                        </select>										
					</div>		            

		            <div class="form-group">
		                <label for="valorAlquiler">valor del alquiler mensual</label>
		                <input type="text" name="valorAlquiler" id="valorAlquiler" class="form-control" value="<?php echo set_value('valorAlquiler'); ?>">
		            </div>

<?php if ($this->ion_auth->is_admin()){ ?>
			<div class="form-group">
				<label for="selectCorreduria">Correduría a la que se va a asignar la tarificación:</label>
				<select id="selectCorreduria" name="selectCorreduria" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorreduria', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($correduriasArray as $item):
					echo '<option value="' . $item->codigo . '" ' . set_select('selectCorreduria', $item->codigo) . '>' . $item->nombre . '</option>';
				endforeach;
				?>
				</select>
			</div>	
<?php } ?>	            


			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aceptar</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>






		             
		        </form>

		    </div>
		</div>




