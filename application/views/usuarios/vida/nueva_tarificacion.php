
    <header class="codrops-header">
        <h1>Nueva tarificación de seguro de vida</h1>
    </header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>
	

		<div class="row">
		    <div class="col-md-12">
		        <!--<form action= "http://localhost/webs/tarificador-server/index.php/ws-api/tarificador/alquiler" method="POST">-->
		        <?php echo form_open_multipart('tarificador/nueva_tarificacion_vida'); ?>
		       
		            <div class="form-group">
		                <label for="nombreApellidos">nombre y apellidos</label>
		                <input type="text" name="nombreApellidos" id="nombreApellidos" class="form-control" value="<?php echo set_value('nombreApellidos'); ?>">		            
		            </div>


		            <div class="form-group">
		                <label for="email">email</label>
		                <input type="email" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>">
		            </div>

		            <div class="form-group">
		                <label for="telefono">telefono</label>
		                <input type="tel" name="telefono" id="telefono" class="form-control" value="<?php echo set_value('telefono'); ?>">
		            </div>

					<div class="form-group">
						<label for="capital">Capital a asegurar</label>
                        <select id="capital" name="capital" class="form-control">
                           
                           <option  value="" <?php echo set_select('capital', '', TRUE); ?>>Selecciona un capital</option>
                            <option value="50000"  <?php echo set_select('capital', '50000'); ?>>50.000 €</option>
                            <option value="100000"  <?php echo set_select('capital', '100000'); ?>>100.000 €</option>
                            <option value="150000"  <?php echo set_select('capital', '150000'); ?>>150.000 €</option>
                            <option value="200000"  <?php echo set_select('capital', '200000'); ?>>200.000 €</option>
                            <option value="250000"  <?php echo set_select('capital', '250000'); ?>>250.000 €</option>
                            <option value="300000"  <?php echo set_select('capital', '300000'); ?>>300.000 €</option>
                            <option value="350000"  <?php echo set_select('capital', '350000'); ?>>350.000 €</option>
                            <option value="400000"  <?php echo set_select('capital', '400000'); ?>>400.000 €</option>
                            <option value="450000"  <?php echo set_select('capital',  '450000'); ?>>450.000 €</option>
                            <option value="500000"  <?php echo set_select('capital',  '500000'); ?>>500.000 €</option>                       
                        </select>										
					</div>		            


									<div class="form-group">
									<label for="edad">Edad del asegurado</label>
										<select name="edad" id="edad" class="form-control"> 
											<option value="">Selecciona una opción</option>                                          
                                              <?php
                                                  for ($i=0; $i<=99; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                          </select>
									</div>




<?php if ($this->ion_auth->is_admin()){ ?>
			<div class="form-group">
				<label for="selectCorreduria">Correduría a la que se va a asignar la tarificación:</label>
				<select id="selectCorreduria" name="selectCorreduria" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorreduria', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($correduriasArray as $item):
					echo '<option value="' . $item->codigo . '" ' . set_select('selectCorreduria', $item->codigo) . '>' . $item->nombre . '</option>';
				endforeach;
				?>
				</select>
			</div>	
<?php } ?>	            


			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aceptar</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>






		             
		        </form>

		    </div>
		</div>




