<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>



	

<div class="row bottom_buffer">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">DATOS GENERALES</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Fecha de tarificación</b></td>
					<td><?php echo $fechaTarificacion; ?></td>
				</tr>			
				<tr>
					<td><b>Código de referencia</b></td>
					<td><?php echo $codtar; ?></td>
				</tr>
				<tr>
					<td><b>Nombre y apellidos</b></td>
					<td><?php echo $nombreApellidos; ?></td>
				</tr>
				<tr>
					<td><b>E-mail</b></td>
					<td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td><b>Teléfono</b></td>
					<td><?php echo $telefono; ?></td>
				</tr>
			</tbody>
		</table>		       

	</div>
</div>

<div class="row bottom_buffer">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">DATOS DEL PRODUCTO</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Tipo de seguro</b></td>
					<td><?php echo $primas['Tipo de seguro']; ?></td>
				</tr>			
				<?php 
				unset($primas['Tipo de seguro']);

				if (!is_null($primas)){
					foreach ($primas as $k => $v) { 
						echo'<tr>';
							echo'<td><b>' . $k . '</b></td>';
							echo '<td>' . $v . '</td>';
						echo '</tr>';
					} 
				}?>
			</tbody>
		</table>		       

	</div>
</div>







