<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>



	

<div class="row bottom_buffer">
	<div class="col-md-12">
		<table border="1" cellpadding="2" cellspacing="1" class="table table-striped table-hover table-bordered tabla_tasas">
			<thead>
				<tr>
					<th colspan="2">DATOS GENERALES</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>Fecha de tarificación</b></td>
					<td><?php echo $fechaTarificacion; ?></td>
				</tr>
				<tr>
					<td><b>Origen</b></td>
					<td><?php echo $origen; ?></td>
				</tr>								
				<tr>
					<td><b>Código de referencia</b></td>
					<td><?php echo $codtar; ?></td>
				</tr>
				<tr>
					<td><b>Nombre y apellidos</b></td>
					<td><?php echo $nombreApellidos; ?></td>
				</tr>
				<tr>
					<td><b>E-mail</b></td>
					<td><?php echo $email; ?></td>
				</tr>
				<tr>
					<td><b>Teléfono</b></td>
					<td><?php echo $telefono; ?></td>
				</tr>

				<tr>
					<td><b>Provincia</b></td>
					<td><?php echo $provincia; ?></td>
				</tr>

				<tr>
					<td><b>Contratante</b></td>
					<td><?php echo $contratanteTipo; ?></td>
				</tr>								

				<tr>
					<td><b>Número de personas</b></td>
					<td><?php echo $numeroPersonas; ?></td>
				</tr>
				<?php for ($i = 1; $i <= $numeroPersonas; $i++) {
					echo "<tr>";
					echo "<td><b>Edad asegurado " . $i . "</b></td>";
					echo "<td>" . $edades_arr[$i] . "</td>";
					echo "</tr>";
				} ?>			
																																
			</tbody>
		</table>		       

	</div>
</div>


<div class="row">
	<div class="col-md-12">
	<table class="table table-striped table-hover table-bordered">
	<tr>
	<th colspan="3" class="table_th_left">PRECIOS (totales mensuales)</th>
	</tr>
		<tr>
			<th>Compañía</th>
			<th>Cuadro médico (sin copago)</th>
			<th>Reembolso (con copago)</th>
		</tr>
<?php

		
		
		foreach ($primas as $k => $v) {

			if ($v["nombreProducto_cm"] == NULL)
				$prima_cm = "-";
			else
				$prima_cm = $v["prima_cm"] . " €<br>" . $v["nombreProducto_cm"];			

			if ($v["nombreProducto_re"] == NULL)
				$prima_re = "-";
			else
				$prima_re = $v["prima_re"] . " €<br>" . $v["nombreProducto_re"];	

			echo "<tr>";
			echo "<td>";
			echo img(array('src'=>'public/images/logos_cias/' . $v["logoProducto"], 'alt'=> $v['nombreCia']));
			echo "</td>";

			echo "<td class='precio_parrilla'><span>";
			echo $prima_cm . "</span>";
			echo "</td>";
			echo "<td class='precio_parrilla'><span>";
			echo $prima_re . "</span>";
			echo "</td>";			
			echo "</tr>";
		}
?>
	</table>		       

	</div>
</div>






