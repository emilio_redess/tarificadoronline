
    <header class="codrops-header">
        <h1>Nueva tarificación de seguro de salud</h1>
    </header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>
	

		<div class="row">
		    <div class="col-md-12">
		        <?php echo form_open_multipart('tarificador/nueva_tarificacion_salud'); ?>
		       
		            <div class="form-group">
		                <label for="nombreApellidos">nombre y apellidos</label>
		                <input type="text" name="nombreApellidos" id="nombreApellidos" class="form-control" value="<?php echo set_value('nombreApellidos'); ?>">		            
		            </div>


		            <div class="form-group">
		                <label for="email">email</label>
		                <input type="email" name="email" id="email" class="form-control" value="<?php echo set_value('email'); ?>">
		            </div>

		            <div class="form-group">
		                <label for="telefono">telefono</label>
		                <input type="tel" name="telefono" id="telefono" class="form-control" value="<?php echo set_value('telefono'); ?>">
		            </div>

					<div class="form-group">
					<label for="provincia">provincia</label>
						<select id="provincia" name="provincia" class="form-control" > 
									     	<option value="">Selecciona una provincia</option>
						                    <option value="01">Álava</option>
						                    <option value="02">Albacete</option>
						                    <option value="03">Alicante</option>
						                    <option value="04">Almería</option>
						                    <option value="33">Asturias</option>
						                    <option value="05">Ávila</option>
						                    <option value="06">Badajoz</option>
						                    <option value="07">Baleares (Islas)</option>
						                    <option value="08">Barcelona</option>
						                    <option value="09">Burgos</option>
						                    <option value="10">Cáceres</option>
						                    <option value="11">Cádiz</option>
						                    <option value="39">Cantabria</option>
						                    <option value="12">Castellón</option>
						                    <option value="51">Ceuta</option>
						                    <option value="13">Ciudad real</option>
						                    <option value="14">Córdoba</option>
						                    <option value="15">Coruña (A)</option>
						                    <option value="16">Cuenca</option>
						                    <option value="17">Girona</option>
						                    <option value="18">Granada</option>
						                    <option value="19">Guadalajara</option>
						                    <option value="20">Guipúzcoa</option>
						                    <option value="21">Huelva</option>
						                    <option value="22">Huesca</option>
						                    <option value="23">Jaén</option>
						                    <option value="24">León</option>
						                    <option value="25">Lleida</option>
						                    <option value="27">Lugo</option>
						                    <option value="28">Madrid</option>
						                    <option value="29">Málaga</option>
						                    <option value="52">Melilla</option>
						                    <option value="30">Murcia</option>
						                    <option value="31">Navarra</option>
						                    <option value="32">Orense</option>
						                    <option value="34">Palencia</option>
						                    <option value="35">Palmas (las)</option>
						                    <option value="36">Pontevedra</option>
						                    <option value="26">Rioja (la)</option>
						                    <option value="37">Salamanca</option>
						                    <option value="40">Segovia</option>
						                    <option value="41">Sevilla</option>
						                    <option value="42">Soria</option>
						                    <option value="38">Sta. Cruz Tenerife</option>
						                    <option value="43">Tarragona</option>
						                    <option value="44">Teruel</option>
						                    <option value="45">Toledo</option>
						                    <option value="46">Valencia</option>
						                    <option value="47">Valladolid</option>
						                    <option value="48">Vizcaya</option>
						                    <option value="49">Zamora</option>
						                    <option value="50">Zaragoza</option>		                                   
						</select>
					</div>

									<div class="form-group">
									<label for="autonomo">Contratante</label>
										<select id="autonomo" name="autonomo" class="form-control">
                                            <option value="">¿Quien será el contratante del seguro?</option>
                                            <option value="1">Empresa</option>
                                            <option value="2">Autónomo</option>
                                            <option value="3">Autónomo con empleados</option>
                                            <option value="4">Particular</option>
                                            </select>
									</div>
									

									<div class="form-group">
									<label for="numeroPersonas">Número de personas</label>
                                        <select id="numeroPersonas" name="numeroPersonas" class="form-control">
                                            <option value="">Número de personas a asegurar</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>										
									</div>

									<div class="form-group hidden-field datepicker-row disabled" val="1">
									<label for="date-1">Edad asegurado 1</label>
										<select name="date-1" id="date-1" class="form-control"> 
											<option value="">Edad del asegurado 1</option>                                          
                                              <?php
                                                  for ($i=0; $i<=99; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                          </select>
									</div>

									<div class="form-group hidden-field datepicker-row disabled" val="2">
									<label for="date-2">Edad asegurado 2</label>
										<select name="date-2" id="date-2" class="form-control"> 
											<option value="">Edad del asegurado 2</option>                                          
                                              <?php
                                                  for ($i=0; $i<=99; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                          </select>
									</div>	

									<div class="form-group hidden-field datepicker-row disabled" val="3">
									<label for="date-3">Edad asegurado 3</label>
										<select name="date-3" id="date-3" class="form-control"> 
											<option value="">Edad del asegurado 3</option>                                          
                                              <?php
                                                  for ($i=0; $i<=99; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                          </select>
									</div>

									<div class="form-group hidden-field datepicker-row disabled" val="4">
									<label for="date-4">Edad asegurado 4</label>
										<select name="date-4" id="date-4" class="form-control"> 
											<option value="">Edad del asegurado 4</option>                                          
                                              <?php
                                                  for ($i=0; $i<=99; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                              <?php } ?>
                                          </select>
									</div>






<?php if ($this->ion_auth->is_admin()){ ?>
			<div class="form-group">
				<label for="selectCorreduria">Correduría a la que se va a asignar la tarificación:</label>
				<select id="selectCorreduria" name="selectCorreduria" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorreduria', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($correduriasArray as $item):
					echo '<option value="' . $item->codigo . '" ' . set_select('selectCorreduria', $item->codigo) . '>' . $item->nombre . '</option>';
				endforeach;
				?>
				</select>
			</div>	
<?php } ?>	            


			<div class="row bottom_buffer">
				<div class="col-md-4 alignIzq">					
					
				</div>
				<div class="col-md-4 alignCentro">
					<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Aceptar</button>
				</div>
				<div class="col-md-4 alignDer">
					
				</div>
			</div>






		             
		        </form>

		    </div>
		</div>




