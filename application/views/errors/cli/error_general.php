<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo "\nERROR: ",
	$heading,
	"\n\n",
	var_dump($message),
	"\n\n",
	var_dump($resp),
	"\n\n";