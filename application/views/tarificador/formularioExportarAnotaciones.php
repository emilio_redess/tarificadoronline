<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>

<div class="row">
	<div class="col-md-12">
		<?php echo validation_errors(); ?>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
		<?php echo form_open_multipart('tarificador/exportarAnotaciones'); ?>

			
			
			<fieldset>
				<legend>Opciones:</legend>
<?php if ($this->ion_auth->is_admin()){ ?>
			<div class="form-group">
				<label for="selectCorreduria">Correduría</label>
				<select id="selectCorreduria" name="selectCorreduria" class="form-control">
				<option selected="selected" value="" <?php echo set_select('selectCorreduria', '', TRUE); ?>>Selecciona una correduría</option>
				<?php 
				foreach ($correduriasArray as $item):
					echo '<option value="' . $item->id . '" ' . set_select('selectCorreduria', $item->id) . '>' . $item->nombre . '</option>';
				endforeach;
				?>
				</select>
			</div>	
<?php } ?>	

			<div class="form-group">
				<label for="selectRamo">Selecciona el ramo de seguros</label>
				<select id="selectRamo" name="selectRamo" class="form-control">
				<option selected="selected" value="">Selecciona un ramo</option>
				<?php 
				foreach ($ramosArray as $item):
					echo '<option value="' . $item->id . '">' . $item->name . '</option>';
				endforeach;
				?>
				</select>
			</div>

                        <div class="form-group">
                            <label for="fechaInicio" class="control-label">A partir de la fecha</label>
                            <input type="text" class="form-control fechaInicio" id="fechaInicio" name="fechaInicio">
                        </div>	



						<div class="form-group">
							<label for="ordenar">Ordenar por:</label>
							<select id="ordenar" name="ordenar" class="form-control">							
							<option selected="selected" value="0">Por fecha (mas reciente primero)</option>
							<option value="1">Por fecha (mas antigua primero)</option>
							<option value="2">Por código de referencia</option>
							</select>
						</div>	

		</fieldset>




	

			<div class="row bottom_buffer">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Continuar</button>
				</div>
			</div>
		</form>
	</div>
</div>
