<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>


<div class="row">
	<div class="col-md-12">
		<table class="ej-costes table" style="font-size: 1em;">
			<thead>
				<tr>
					<th width="33%" style="text-decoration: underline;text-align: center;">Opciones seleccionadas</th>
					<th width="33%" style="text-decoration: underline;text-align: center;">Enlaces de descarga</th>
				</tr>
			</thead>
			<tbody style="text-align: center;">
				<tr>
					<td>Fecha de inicio: <?php echo $fechaInicioeu; ?></td>

					<td>
       
            <?php if ($this->ion_auth->is_admin()){ ?>
            <a href="<?php echo site_url('ws-api/tarificaciones/listadoTodasAnotaciones/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/sort/' . $ordenar . '/format/xml' ); ?>" target="_blank">Ver en XML</a><br>
			<a href="<?php echo site_url('ws-api/tarificaciones/listadoTodasAnotaciones/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/sort/' . $ordenar); ?>" target="_blank">Ver JSON</a><br>          
            <?php } ?>

			<a href="<?php echo site_url('ws-api/tarificaciones/listadoTodasAnotaciones/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/sort/' . $ordenar . '/format/html' ); ?>" target="_blank">Ver tabla de resultados</a><br>

            <a href="<?php echo site_url('ws-api/tarificaciones/listadoTodasAnotaciones/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/sort/' . $ordenar . '/format/csv' ); ?>" target="_blank">descargar en CSV</a><br>


								

					</td>
				</tr>
								
			</tbody>
		</table>
	</div>
</div>



