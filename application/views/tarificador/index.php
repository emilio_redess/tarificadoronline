<?php
//$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');

//$this -> load -> view('templates/header', $data);
?>

    <header class="codrops-header">
        <h1><?php echo $title; ?></h1>
    </header>

<div class="container">

<table id="tarificaciones" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Referencia</th>
            <th>Ramo</th>
            <th>Fecha</th>
            <th>Compañía</th>
            <th>Nombre y apellidos</th>
            <th>Teléfono</th>
            <th>E-mail</th>
            <th>Acciones</th>
              
        </tr>
    </thead>
    <tbody></tbody>
</table>

</div>



    <!-- Modal: tarificacion contratada -->
    <div class="modal fade" id="ModalContratada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Contratación de tarificación</h4><span class="modal-add-contratada-wait-icon"></span>
                </div>

                <div class="modal-contratada-body modal-body"></div>
                <input type="hidden" name="contratadaTarificacionId" id="contratadaTarificacionId" value=""/>               
                <input type="hidden" name="contratadaEstado" id="contratadaEstado" value=""/>               
                <div class="modal-footer">
                    <button type="button" class="btn-contratada-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-contratada-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: documentacion enviada -->
    <div class="modal fade" id="ModalDocu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Envío de documentación</h4><span class="modal-add-docu-wait-icon"></span>
                </div>

                <div class="modal-docu-body modal-body"></div>
                <input type="hidden" name="docuTarificacionId" id="docuTarificacionId" value=""/>               
                <input type="hidden" name="docuEstado" id="docuEstado" value=""/>               
                <div class="modal-footer">
                    <button type="button" class="btn-docu-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-docu-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>






    <!-- Modal: enviar a papelera tarificacion -->
    <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Enviar a la papelera</h4>
                </div>
                <div class="modal-delete-body modal-body"></div>
                <input type="hidden" name="tarificacionId" id="tarificacionId" value=""/>
                <input type="hidden" name="ramo" id="ramo" value="<?php echo $ramo; ?>"/>
                <div class="modal-footer">
                    <button type="button" class="btn-delete-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-delete-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal listar comentarios -->
    <div class="modal fade" id="ModalCommentaries" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="modalCommentariesTitle">Anotaciones de tarificación</h4>
                </div>
                <div class="modal-commentaries-body modal-body">

                    <table id="commentaries-table" class="table table-striped">
                        <thead>
                        <th>Fecha de creación</th>
                        <th>Fecha recordatorio</th>
                        <th>Usuario</th>
                        <th>Anotaciones</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody id="commentaries-table-tbody">
                        </tbody>
                    </table>
                </div>
                <div class="commentaries-footer modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btn-add-commentary" class="btn btn-add-commentary btn-primary">Añadir</button>
                </div>
            </div>
        </div>
    </div>


        <!-- Modal add comentarios -->
    <div class="modal fade" id="ModalAddCommentary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Añadir anotación</h4><span class="modal-add-commentary-wait-icon"></span>
                </div>
                <div class="modal-addCommentary-body modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="reminderDate" class="control-label">Fecha recordatorio</label>
                            <input type="text" class="form-control" id="reminderDate">
                        </div>

                        <div class="form-group-commentary-text form-group required">
                            <label for="commentaryText" class="control-label">Texto *</label>
                            <input type="text" class="form-control" id="commentaryText" placeholder="">
                        </div>
                    </form>
                    <span class="red">* campos obligatorios</span>
                </div>
                <input type="hidden" name="addCommentaryPricingId" id="addCommentaryPricingId" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn-add-commentary-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-add-commentary-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

        <!-- Modal edit commentary -->
    <div class="modal fade" id="ModalEditCommentary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar anotación</h4><span class="modal-edit-commentary-wait-icon"></span>
                </div>
                <div class="modal-editCommentary-body modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="edit-reminderDate" class="control-label">Fecha recordatorio</label>
                            <input type="text" class="form-control" id="edit-reminderDate">
                        </div>

                        <div class="form-group-commentary-text form-group required">
                            <label for="commentaryText" class="control-label">Texto *</label>
                            <input type="text" class="form-control" id="edit-commentaryText" placeholder="">
                        </div>
                    </form>
                    <span class="red">* campos obligatorios</span>
                </div>
                <input type="hidden" name="editCommentaryId" id="editCommentaryId" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn-edit-commentary-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-edit-commentary-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal delete commentary -->
    <div class="modal fade" id="ModalDeleteCommentary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Borrar anotación</h4>
                </div>
                <div class="modal-deleteCommentary-body modal-body"></div>
                <input type="hidden" name="commentaryId" id="commentaryId" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn-deleteCommentary-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-deleteCommentary-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>