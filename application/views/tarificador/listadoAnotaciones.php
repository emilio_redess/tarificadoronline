<?php
//$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificaciones.js');

//$this -> load -> view('templates/header', $data);
?>

    <header class="codrops-header">
        <h1>Anotaciones pendientes</h1>
    </header>

<div class="container">

<table id="tarificaciones" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Referencia</th>
            <th>Añadido el día</th>
            <th>Mensaje</th>
            <th>Enlace</th>
        </tr>
    </thead>
    <tbody>
<?php 

    if (count($listadoAnotaciones->result()) == 0)
        echo "<tr><td colspan='4'>No existen anotaciones pendientes de revisar para hoy</td></tr>";
    else{
    foreach ($listadoAnotaciones->result() as $row)
    {
        echo '<tr>';

        echo '<td>';
        echo $row->codigo_tarificacion;
        echo '</td>';

        echo '<td>';

        $date = new DateTime($row->fecha_creacion);
        echo $date->format('d-m-Y H:i:s');

        echo '</td>';

        echo '<td>';
        echo $row->contenido;
        echo '</td>';   

        echo '<td>';
        echo anchor('tarificador/listado/' . $row->ramo_id . '/' . $row->codigo_tarificacion, 'Ir a tarificación');
        echo '</td>';     

        echo '</tr>';     



    }
}
?>

    </tbody>
</table>

</div>


 