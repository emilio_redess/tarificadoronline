<?php
//$data['jsArray'] = array('public/js/jQuery.dtplugin.js','public/js/datatable.listadoTarificacionesPapelera.js');

//$this -> load -> view('templates/header', $data);
?>
<div class="content clearfix">
    <header class="codrops-header">
        <h1><?php echo $title; ?></h1>
    </header>

    <div id="container">

        <div id="emptyBinDiv" class="text-center">
            <button id="emptyBin" name="emptyBin" type="button" class="btn btn-delete-all btn-primary btn-lg" data-toggle="modal" data-target="#ModalDeleteAll">
                Vaciar papelera de reciclaje
            </button>
        </div>

        <table id="tarificaciones" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Referencia</th>
                    <th>Fecha borrado</th>
                    <th>Eliminada por</th>
                    <th>Ramo</th>
                    <th>Nombre y apellidos</th>
                    <th>Correduría</th>
                    <th>Acciones</th>
                  
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

    <!-- Modal: Vaciar papelera reciclaje -->
    <div class="modal fade" id="ModalDeleteAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Borrar</h4>
                </div>
                <div class="modal-delete-body modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn-delete-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-delete-all-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: Borrar permanentemente tarificacion -->
    <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Borrar</h4>
                </div>
                <div class="modal-delete-body modal-body"></div>
                <input type="hidden" name="tarificacionId" id="tarificacionId" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn-delete-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-delete-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal restaurar tarificacion -->
    <div class="modal fade" id="ModalRestore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Restaurar tarificación</h4>
                </div>
                <div class="modal-restore-body modal-body"></div>
                <input type="hidden" name="tarificacionId" id="tarificacionId" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn-restore-cancel btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn-restore-accept btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>



<?php
//$this->load->view('templates/footer');
?>