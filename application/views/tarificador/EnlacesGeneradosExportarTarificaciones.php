<header class="codrops-header">
    <h1><?php echo $title; ?></h1>
</header>


<div class="row">
	<div class="col-md-12">
		<table class="ej-costes table" style="font-size: 1em;">
			<thead>
				<tr>
					<th width="33%" style="text-decoration: underline;text-align: center;">Opciones seleccionadas</th>
					<th width="33%" style="text-decoration: underline;text-align: center;">Filtros</th>
					<th width="33%" style="text-decoration: underline;text-align: center;">Enlaces de descarga</th>
				</tr>
			</thead>
			<tbody style="text-align: center;">
				<tr>
					<td>Fecha de inicio: <?php echo $fechaInicioeu; ?></td>
					<td><?php if (isset($filtroContratadaTexto)) echo $filtroContratadaTexto . '<br>'; ?>
						
						<?php if (isset($filtroDocuTexto)) echo $filtroDocuTexto; ?>

					</td>
					<td>
       
            <?php if ($this->ion_auth->is_admin()){ ?>
            <a href="<?php echo site_url('ws-api/tarificaciones/listado/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/filter/' . $filtroContratada . $filtroDocu . '/format/xml' ); ?>" target="_blank">Ver en XML</a><br>
			<a href="<?php echo site_url('ws-api/tarificaciones/listado/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/filter/' . $filtroContratada .  $filtroDocu ); ?>" target="_blank">Ver JSON</a><br>          
            <?php } ?>

			<a href="<?php echo site_url('ws-api/tarificaciones/listado/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/filter/' . $filtroContratada .  $filtroDocu . '/format/html' ); ?>" target="_blank">Ver tabla de resultados</a><br>

            <a href="<?php echo site_url('ws-api/tarificaciones/listado/cid/' . $correduria_id . '/rid/' . $ramo_id . '/inid/' . $fechaInicio . '/filter/' . $filtroContratada . $filtroDocu . '/format/csv' ); ?>" target="_blank">descargar en CSV</a><br>


								

					</td>
				</tr>
								
			</tbody>
		</table>
	</div>
</div>



