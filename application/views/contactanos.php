<div class="container">
<header class="codrops-header">
	<h1>Contáctanos</h1>
</header>

<div class="row bottom_buffer">
	<h5>Puedes ponerte en contacto con nosotros para resolver cualquier duda en el número de teléfono <?php echo $telefono1; ?> o bien enviando el siguiente formulario.</h5>
</div>

<div class="row">
	<div class="col-md-6">
		<?php echo form_open('tarificador/contactanos'); ?>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" class="form-control">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" name="telefono" id="telefono" class="form-control">
                </div> 

                <div class="form-group">
                  <label for="indicaciones">Indicaciones</label>
                  <textarea class="form-control" rows="3" name="indicaciones" id="indicaciones"></textarea>     
                </div>           

               
    
                <button type="submit" class="btn btn-success">Enviar</button>
              </form>	
	
	</div>
</div>

</div>












