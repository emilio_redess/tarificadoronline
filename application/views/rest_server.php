<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>



<div class="content clearfix">
    <h1>REST Server Tests</h1>

    <div id="body">

      <!--  <h2><a href="<?php echo site_url(); ?>">Home</a></h2>-->

        <p>
            See the article
            <a href="http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/" target="_blank">
                http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
            </a>
        </p>

        <p>
            The master project repository is
            <a href="https://github.com/chriskacerguis/codeigniter-restserver" target="_blank">
                https://github.com/chriskacerguis/codeigniter-restserver
            </a>
        </p>

        <p>
            Click on the links to check whether the REST server is working.
        </p>

        <ol>
            <li><a href="<?php echo site_url('ws-api/example/users'); ?>" target="_blank">Users</a> - defaulting to JSON</li>
            <li><a href="<?php echo site_url('ws-api/example/users/format/csv'); ?>">Users</a> - get it in CSV</li>
            <li><a href="<?php echo site_url('ws-api/example/users/id/1'); ?>" target="_blank">User #1</a> - defaulting to JSON  (users/id/1)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/1'); ?>" target="_blank">User #1</a> - defaulting to JSON  (users/1)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/id/3.xml'); ?>" target="_blank">User #3</a> - get it in XML (users/id/1.xml)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/id/1/format/xml'); ?>" target="_blank">User #1</a> - get it in XML (users/id/1/format/xml)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/id/1?format=xml'); ?>" target="_blank">User #1</a> - get it in XML (users/id/1?format=xml)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/1.xml'); ?>" target="_blank">User #1</a> - get it in XML (users/1.xml)</li>
            <li><a id="ajax" href="<?php echo site_url('ws-api/example/users/format/json'); ?>" target="_blank">Users</a> - get it in JSON (AJAX request)</li>
            <li><a href="<?php echo site_url('ws-api/example/users.html'); ?>" target="_blank">Users</a> - get it in HTML (users.html)</li>
            <li><a href="<?php echo site_url('ws-api/example/users/format/html'); ?>" target="_blank">Users</a> - get it in HTML (users/format/html)</li>
            <li><a href="<?php echo site_url('ws-api/example/users?format=html'); ?>" target="_blank">Users</a> - get it in HTML (users?format=html)</li>
            <li><a href="<?php echo site_url('ws-api/tarificador_seguro_alquiler/alquiler/id/1601/format/xml'); ?>" target="_blank">Alquiler</a> - obtener los datos de la tarificacion con id=49 en formato XML</li>
            <li><a href="<?php echo site_url('ws-api/tarificador_seguro_alquiler/alquiler/id/1601'); ?>" target="_blank">Alquiler</a> - obtener los datos de la tarificacion con id=49 en formato JSON</li>
            <li><a href="<?php echo site_url('ws-api/tarificador_seguro_vida/vida/id/2188'); ?>" target="_blank">Vida</a> - obtener los datos de la tarificacion con id=2189 en formato JSON</li>
        </ol>



<script src="https://code.jquery.com/jquery-1.12.0.js"></script>

<script>
    // Create an 'App' namespace
    var App = App || {};

    // Basic rest module using an IIFE as a way of enclosing private variables
    App.rest = (function restModule(window) {
        // Fields
        var _alert = window.alert;
        var _JSON = window.JSON;

        // Cache the jQuery selector
        var _$ajax = null;

        // Cache the jQuery object
        var $ = null;

        // Methods (private)

        /**
         * Called on Ajax done
         *
         * @return {undefined}
         */
        function _ajaxDone(data) {
            // The 'data' parameter is an array of objects that can be iterated over
            _alert(_JSON.stringify(data, null, 2));
        }

        /**
         * Called on Ajax fail
         *
         * @return {undefined}
         */
        function _ajaxFail() {
            _alert('Oh no! A problem with the Ajax request!');
        }

        /**
         * On Ajax request
         *
         * @param {jQuery} $element Current element selected
         * @return {undefined}
         */
        function _ajaxEvent($element) {
            $.ajax({
                    // URL from the link that was 'clicked' on
                    url: $element.attr('href')
                })
                .done(_ajaxDone)
                .fail(_ajaxFail);
        }

        /**
         * Bind events
         *
         * @return {undefined}
         */
        function _bindEvents() {
            // Namespace the 'click' event
            _$ajax.on('click.app.rest.module', function (event) {
                event.preventDefault();

                // Pass this to the Ajax event function
                _ajaxEvent($(this));
            });
        }

        /**
         * Cache the DOM node(s)
         *
         * @return {undefined}
         */
        function _cacheDom() {
            _$ajax = $('#ajax');
        }

        // Public API
        return {
            /**
             * Initialise the following module
             *
             * @param {object} jQuery Reference to jQuery
             * @return {undefined}
             */
            init: function init(jQuery) {
                $ = jQuery;

                // Cache the DOM and bind event(s)
                _cacheDom();
                _bindEvents();
            }
        };
    }(window));

    // DOM ready event
    $(function domReady($) {
        // Initialise the App module
        App.rest.init($);
    });
</script>


